function getLoanDetail(entityId, customerId) {
    var self = this;
    var questionnaireService = self.call('questionnaireService');

    return questionnaireService.getLoanDetail(entityId, customerId)
    .then(function (response) {
        return { status: 'success', data: response };
    }).catch(function (error) {
         return { status: 'error', error: error };
    });
}