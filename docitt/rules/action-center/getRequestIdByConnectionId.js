function getRequestIdByConnectionId(connectionId) {
    var self = this;
    var syndicationService = self.call('syndicationService');
    return syndicationService.getRequestIdByConnectionId(connectionId)
    .then(function (response) {
        return { status: 'success', data: response };
    }).catch(function (error) {
        return { status: 'error', error: error };
    });
}