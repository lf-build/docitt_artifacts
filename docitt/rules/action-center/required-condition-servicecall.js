function addDefaultCondition(entityType,entityId,payload) {
    var self = this;
    var requiredConditionService = self.call('requiredConditionService');
 
    return requiredConditionService.addDefaultRequiredCondition(entityType, entityId,payload).then(function (conditionResponse) {
        return {
            status: 'success',
            data: conditionResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}