function hasPlaidLinking(entityId, customerId) {
    var self = this;
    var syndicationService = self.call('syndicationService');

    return syndicationService.hasPlaidLinking(entityId, customerId).then(function (response) {
        return {
            status: 'success',
            data: response
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}