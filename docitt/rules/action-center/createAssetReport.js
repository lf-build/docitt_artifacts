function createAssetReport(entityType, entityId, customerId, requestId) {
    var self = this;
    var syndicationService = self.call('syndicationService');

    return syndicationService.createAssetReport(entityId, customerId, requestId).then(function (assetResponse) {
        return {
            status: 'success',
            data: assetResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}