function getLinkingFlag(entityId, customerId) {
    var self = this;
    var syndicationService = self.call('syndicationService');

    return syndicationService.getLinkingFlag(entityId, customerId)
    .then(function (response) {
        return { status: 'success', data: response };
    }).catch(function (error) {
         return { status: 'error', error: error };
    });
}