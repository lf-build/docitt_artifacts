function adddefault_condition(payload) {
    var errorData = [];
    var requestIds = [];
    var removeRequestIds = [];
    try {
      
        var objValidateInput = validateRequiredInput(payload);
        if (objValidateInput.result) {
            if (payload.purposeLoan.toLowerCase() == 'purchase') {
                requestIds.push('agreenment-purchase-sale');
            }
            if ((payload.residencyType.toLowerCase() == 'owned' && payload.purposeLoan.toLowerCase() == 'purchase') || payload.purposeLoan.toLowerCase() == 'refinance') {
                requestIds.push('verification-mortgage');
                requestIds.push('hazard-insurance');
            }           
            if (payload.isInContract.toLowerCase() == 'yes' && payload.purposeLoan.toLowerCase() == 'purchase') {
                requestIds.push('letter-giftor-gifted');
                requestIds.push('verification-emd');
            }
            if (payload.propertyType.toLowerCase() == 'condominium' && (payload.purposeLoan.toLowerCase() == 'purchase' || payload.purposeLoan.toLowerCase() == 'refinance' || payload.purposeLoan.toLowerCase() == 'quote')) {
                requestIds.push('property-h06');
            }
            if (payload.employmentIncome > 0) {
                requestIds.push('qualifying-paystubs');
                requestIds.push('qualifying-w2');
                requestIds.push('qualifying-ptr');
            }
            

            if (payload.businessSelfEmploymentIncome > 0 && !payload.companyStackLT25Per) {
                requestIds.push('qualifying-pbftr');
                requestIds.push('qualifying-k1');
                requestIds.push('qualifying-pl-bl');

                checkDuplicateAndPush('qualifying-paystubs');
                checkDuplicateAndPush('qualifying-w2');

            } else if (payload.businessSelfEmploymentIncome > 0 && payload.companyStackLT25Per) {
                checkDuplicateAndPush('qualifying-paystubs');
                checkDuplicateAndPush('qualifying-w2');
            }

            if (payload.alimonyorChildSupportAmount > 0) {
                requestIds.push('qualifying-por-ac');
                requestIds.push('legal-divorcedecree');
            }
            if (payload.militaryPayAmount > 0 || payload.isBorrowerVAEligible) {
                requestIds.push('legal-coe');
                requestIds.push('legal-statement-service');
            }
            if (payload.socialSecurityIncome > 0) {
                requestIds.push('legal-social-security-letter');
            }            

            /*if (payload.gender.toLowerCase() == 'male' || payload.gender.toLowerCase() == 'female')*/
            if(payload.gender != '') 
            {
                if(IsJsonString(payload.gender))
                {
                    var objGender = JSON.parse(payload.gender);                
                    if(objGender.length > 0)
                    {                    
                        if(objGender.includes("Male") || objGender.includes("male") || objGender.includes("Female") || objGender.includes("FeMale") || objGender.includes("female"))
                        {
                            requestIds.push('personal-dl-pp');
                        }
                    }
                }
                else
                {                   
                    //for old record
                    if (payload.gender.toLowerCase() == 'male' || payload.gender.toLowerCase() == 'female')
                    {
                        requestIds.push('personal-dl-pp');
                    }

                }
            }

            if (payload.isLargeDeposit.toLowerCase() == 'yes') {
                requestIds.push('asset-bank-largedeposits');
            }
            if (payload.IsEmploymentGap) {
                requestIds.push('employment-gap');
            }

            if (payload.IsSelfEmploymentGap) {
                requestIds.push('self-employment-gap');
            }

           
            if (payload.IsOutStandingJudgement.toLowerCase() == 'yes') {
                requestIds.push('borrower-outstanding-judgement');
            }

            if (payload.isBankBankrupt.toLowerCase() == 'yes') {
                requestIds.push('legal-bankruptcy');
            }

            if (payload.transactionClosingStatement.toLowerCase() == 'yes') {
                requestIds.push('transaction-closing-statement');                
            }

            if (payload.IsPartyLawSuit.toLowerCase() == 'yes') {
                requestIds.push('borrower-party-lawsuit');
            }

            if (payload.IsBeenObligated.toLowerCase() == 'yes') {
                requestIds.push('borrower-been-obligated');
            }

            if (payload.IsPresentlyDelinquent.toLowerCase() == 'yes') {
                requestIds.push('borrower-presently-delinquent');
            }

            if (payload.IsPayAlimony.toLowerCase() == 'yes') {
                requestIds.push('borrower-obligated-to-payalimony');
            }

            if (payload.IsEndoserOnNote.toLowerCase() == 'yes') {
                requestIds.push('borrower-endoser-on-note');
            }          

            if (payload.IsPermanentResidentAlien.toLowerCase() == 'yes') {
                requestIds.push('personal-rac-gc');
            }
            
            /* TODO : date : 09-Aug-2017 , Right now we have to add asset related condition bydefault, when we have loan amount 
            and verified asset amount that time this conditon is added based on the logic given by BA/Client */

            requestIds.push('asset-bank-statements');
            requestIds.push('asset-bank-statements-manual');
            requestIds.push('asset-bank-statements-electronic');
           
            // Comment as hide in naf-production
            // Uncomment whenever need to rollback
           requestIds.push('econsent'); 

            if (payload.DischargeFromActiveDuty) {
                requestIds.push('legal-dd214');
            }
            if (payload.SpouseAsNotACoBorrower) {               
                requestIds.push('personal-spouse-full-legal-name');                
            }
          
            var otherIncome = false;
            otherIncome = (payload.alimonyorChildSupportAmount > 0 ||
                payload.businessSelfEmploymentIncome > 0 ||
                payload.militaryPayAmount > 0 ||
                payload.socialSecurityIncome > 0 ||
                payload.IncomeRentRentalIncome > 0 ||
                payload.InterestLastYear > 0 ||
                payload.OtherIncomePerMonth > 0 
                );
               
                if (payload.employmentIncome > 0 && payload.purposeLoan.toLowerCase() == 'refinance' &&
!otherIncome 
)
{
  
    removeRequestIds.push('qualifying-ptr');
    removeRequestIds.push('asset-bank-statements');
    removeRequestIds.push('asset-bank-statements-manual');   
}
if(payload.purposeLoan.toLowerCase() == 'refinance' ){
    requestIds.push('qualifying-mortgage-liabilities');
    
    }
    
    requestIds = requestIds.filter(function (item) {
        return !removeRequestIds.includes(item);
      });
   
            return {
                'result': true,
                'detail': '',
                'data': requestIds
            };
        } else {
            return objValidateInput;
        }
    } catch (e) {
        var msg = 'Error occured :' + e.message;
        errorData.push(msg);
        return {
            'result': false,
            'detail': errorData,
            'data': null
        };
    }

    function checkDuplicateAndPush(valueToCheck) {
        if (requestIds.indexOf(valueToCheck) < 0) {
            requestIds.push(valueToCheck);
        }
    }

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    function validateRequiredInput(input) {
        var apprequiredProperty = ['purposeLoan', 'residencyType', 'isInContract', 'propertyType', 'employmentIncome', 'gender',
            'businessSelfEmploymentIncome', 'alimonyorChildSupportAmount', 'militaryPayAmount', 'socialSecurityIncome', 'usCitizen', 'isBankBankrupt', 'transactionClosingStatement', 'companyStackLT25Per', 'isLargeDeposit', 'IsEmploymentGap', 'IsSelfEmploymentGap',
            'DischargeFromActiveDuty','SpouseAsNotACoBorrower','IsOutStandingJudgement','IsPartyLawSuit','IsBeenObligated','IsPresentlyDelinquent',
            'IsPayAlimony','IsEndoserOnNote','IsPermanentResidentAlien'
        ];
        var errorMessage;
        var errorData = [];
        if (typeof (input) != 'undefined') {
            for (var i = 0; i < apprequiredProperty.length; i++) {
                var el = input[apprequiredProperty[i]];
                if (typeof (el) === 'undefined') {
                    errorMessage = 'Input property : ' + apprequiredProperty[i] + ' not found.';
                    errorData.push(errorMessage);
                }
            };

            if (errorData.length > 0) {
                return {
                    'result': false,
                    'detail': errorData,
                    'data': null
                };
            } else {
                return {
                    'result': true,
                    'detail': errorData,
                    'data': null
                };
            }
        } else {
            errorMessage = 'application data not found';
            errorData.push(errorMessage);
            return {
                'result': false,
                'detail': errorData,
                'data': null
            };
        }
    }
};