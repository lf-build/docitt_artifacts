function transformRule(payload) {
	var self = this;
	var helperService = self.call('helperv1');
	var transformedResultData = helperService.getTransformResponseModel();
	if (payload != null) {
		var original_data = payload.OriginalData;
		transformedResultData.TenantId = payload.TenantId;
		transformedResultData.ApplicationId = payload.ApplicationId;
		transformedResultData.BorrowerId = payload.BorrowerId;
		transformedResultData.Result = 'Passed';
		transformedResultData.OriginalData = payload.OriginalData;
		var masterData = payload.MasterData;
		if (Array.isArray(original_data)) {
			var transformedData = [];
			original_data.forEach(function (singleObject) {
				var transformedDataTemp = helperService.getTransformatedData(singleObject, masterData);
				transformedData.push(transformedDataTemp);
			});
			transformedResultData.TransformedData = transformedData;
			return transformedResultData;
		} else {
			transformedResultData.TransformedData = helperService.getTransformatedData(original_data, masterData);
			return transformedResultData;
		}
	} else {
		transformedResultData.Result = 'Failed';
		return transformedResultData;
	}
}
