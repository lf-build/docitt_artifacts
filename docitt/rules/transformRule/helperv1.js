function helperv1() {
	var self = this;
	return {
		getKeysData: function (input) {
			return Object.keys(input);
		},
		getValuesData: function (input) {
			return Object.values(input);
		},
		getTransformResponseModel: function () {
			var TransformResponseModel = {
				'TenantId': '',
				'ApplicationId': '',
				'BorrowerId': '',
				'OriginalData': [],
				'TransformedData': [],
				'Result': ''
			};
			return TransformResponseModel;
		},
		getFieldModel: function () {
			var FieldModel = {
				'FieldName': '',
				'AliasName': '',
				'Value': ''
			};
			return FieldModel;
		},
		getEnumModel: function () {
			var EnumModel = {
				'FieldName': '',
				'AliasName': '',
				'Value': []
			};
			return EnumModel;
		},
		getEnumValueModel: function () {
			var EnumFieldModel = {
				'EnumFieldName': '',
				'EnumAliasName': ''
			};
			return EnumFieldModel;
		},
		getTransformatedData: function (original_data, master_field) {
			var self = this;
			var transformedData = [];
			var keys = self.getKeysData(original_data);
			var values = self.getValuesData(original_data);
			for (var i = 0; i < keys.length; i++) {
				var fieldModel = self.getFieldModel();
				master_field.TenantFields.forEach(function (fieldResultObj) {
					if (keys[i].toLowerCase() === fieldResultObj.StandardizedField.toLowerCase()) {
						fieldModel.FieldName = keys[i];
						fieldModel.AliasName = fieldResultObj.TenantLabel;
						if (Array.isArray(values[i]) && self.isValidField(values[i])) {
							var resultTrans = [];
							values[i].forEach(function (singleObject) {
								var transformedDataTemp = self.getTransformatedData(singleObject, master_field);
								resultTrans.push(transformedDataTemp);
							});
							fieldModel.Value = [];
							fieldModel.Value = resultTrans;
							transformedData.push(fieldModel);
						} else if (self.isFunction(values[i])) {
							var resultTrans = self.getTransformatedData(values[i], master_field);
							fieldModel.Value = [];
							fieldModel.Value = resultTrans;
							transformedData.push(fieldModel);
						} else {
							if (fieldResultObj.TenantFieldType.toLowerCase() === 'field' || fieldResultObj.TenantFieldType.toLowerCase() === 'date') {
								fieldModel.Value = values[i];
								transformedData.push(fieldModel);
							} else if (fieldResultObj.TenantFieldType.toLowerCase() === 'enum' && fieldModel.FieldName === 'Relationship') {
								var enumModel = self.getEnumModel();
								enumModel.FieldName = '';
								enumModel.AliasName = '';
								enumModel.Value = [];
								master_field.TenantEnums.forEach(function (keyData, valueData) {
									if (keys[i].toLowerCase() == keyData.StandardizedField.toLowerCase()) {
										enumModel.FieldName = keys[i];
										try
										{
											if (values[i].toLowerCase() === keyData.TenantLabel.toLowerCase()) {
												var enumValueModel = self.getEnumValueModel();
												enumValueModel.EnumFieldName = keyData.StandardizedEnum;
												enumValueModel.EnumAliasName = keyData.TenantLabel;
												enumModel.Value.push(enumValueModel);
											}
										}catch(ex) {}
									}
								});
								enumModel.AliasName = fieldResultObj.TenantLabel;
								transformedData.push(enumModel);
							}
							else if (fieldResultObj.TenantFieldType.toLowerCase() === 'enum' && fieldModel.FieldName === 'BorrowerMaleFemale') {
								var enumModel = self.getEnumModel();
								enumModel.FieldName = '';
								enumModel.AliasName = '';
								enumModel.Value = [];
								master_field.TenantEnums.forEach(function (keyData, valueData) {
									if (keys[i].toLowerCase() == keyData.StandardizedField.toLowerCase()) {
										enumModel.FieldName = keys[i];
										try
										{
											if (values[i].includes(keyData.TenantLabel)) {
												var enumValueModel = self.getEnumValueModel();
												enumValueModel.EnumFieldName = keyData.StandardizedEnum;
												enumValueModel.EnumAliasName = keyData.TenantLabel;
												enumModel.Value.push(enumValueModel);
											}	
										}catch(ex) {}
									}
								});
								enumModel.AliasName = fieldResultObj.TenantLabel;
								transformedData.push(enumModel);
							}
							else if (fieldResultObj.TenantFieldType.toLowerCase() === 'enum' && fieldModel.FieldName === 'BorrowerAccountType') {
								var enumModel = self.getEnumModel();
								enumModel.FieldName = '';
								enumModel.AliasName = '';
								enumModel.Value = [];
								master_field.TenantEnums.forEach(function (keyData, valueData) {
									if (keys[i].toLowerCase() == keyData.StandardizedField.toLowerCase()) {
										enumModel.FieldName = keys[i];
										try
										{
											if (keyData.TenantLabel.toLowerCase().includes(values[i].toLowerCase())) {
												var enumValueModel = self.getEnumValueModel();
												enumValueModel.EnumFieldName = keyData.StandardizedEnum;
												enumValueModel.EnumAliasName = values[i];
												enumModel.Value.push(enumValueModel);
											}											
										}catch(ex) {}
									}
								});

								//Now check if vaules is not found then we have to set it as other account type
								if(enumModel.Value.length == 0)
								{
									var enumValueModel = self.getEnumValueModel();
									enumValueModel.EnumFieldName = "EnumAccountTypeOthers";
									enumValueModel.EnumAliasName = values[i];									
									enumModel.Value[0] = enumValueModel;
								}
								enumModel.AliasName = fieldResultObj.TenantLabel;
								transformedData.push(enumModel);
							}
							else if (fieldResultObj.TenantFieldType.toLowerCase() === 'enum' && (
									fieldModel.FieldName === 'BorrowerMaritalStatus' || 
									fieldModel.FieldName === 'BorrowerAddAsNewCoBorrower' || 
									fieldModel.FieldName === 'ConditionStatus')) {
								var enumModel = self.getEnumModel();
								enumModel.FieldName = '';
								enumModel.AliasName = '';
								enumModel.Value = [];
								master_field.TenantEnums.forEach(function (keyData, valueData) {
									if (keys[i].toLowerCase() == keyData.StandardizedField.toLowerCase()) {
										enumModel.FieldName = keys[i];
										try
										{
											if (values[i].toLowerCase() === keyData.TenantLabel.toLowerCase()) {
												var enumValueModel = self.getEnumValueModel();
												enumValueModel.EnumFieldName = keyData.StandardizedEnum;
												enumValueModel.EnumAliasName = keyData.TenantLabel;
												enumModel.Value.push(enumValueModel);
											}
										}catch(ex) {}
									}
								});
								enumModel.AliasName = fieldResultObj.TenantLabel;
								transformedData.push(enumModel);
							}
							 else if (fieldResultObj.TenantFieldType.toLowerCase() === 'enum' || fieldResultObj.TenantFieldType.toLowerCase() === 'multiselectfield' || fieldResultObj.TenantFieldType.toLowerCase() === 'selectfield') {
								var enumModel = self.getEnumModel();
								enumModel.FieldName = '';
								enumModel.AliasName = '';
								enumModel.Value = [];
								master_field.TenantEnums.forEach(function (keyData, valueData) {
									if (keys[i].toLowerCase() == keyData.StandardizedField.toLowerCase()) {
										enumModel.FieldName = keys[i];
										if (self.setToLowerCase(values[i]).includes(keyData.TenantLabel.toLowerCase())) {
											var enumValueModel = self.getEnumValueModel();
											enumValueModel.EnumFieldName = keyData.StandardizedEnum;
											enumValueModel.EnumAliasName = keyData.TenantLabel;
											enumModel.Value.push(enumValueModel);
										}
									}
								});
								enumModel.AliasName = fieldResultObj.TenantLabel;
								transformedData.push(enumModel);
							} 
						}
					}
				});
			}
			return transformedData;
		},
		isFunction: function (value) {
			return value && typeof value === 'object' && value.constructor === Object;
		},
		setToLowerCase: function (data) {
			if (this.isFunction(data)) {
				var sorted = [];
				for (var i = 0; i < data.length; i++) {
					sorted.push(data[i].toLowerCase());
				}
				return sorted;
			} else {
				return JSON.stringify(data).toLowerCase();
			}
		},
		isValidField: function (arrayData) {
			if (arrayData.length > 0) {
				var arrayDataCount = 0;
				for (var i = 0; i < arrayData.length; i++) {
					if (this.isFunction(arrayData[i])) {
						arrayDataCount++;
					} else {
						arrayDataCount--;
					}
				}
				if (arrayDataCount === arrayData.length) {
					return true;
				} else {
					return false;
				}
			}
			return true;
		}
	}
}
