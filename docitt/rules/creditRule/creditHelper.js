function creditHelper() {
	var self = this;
	return {
		getCreditRuleResponseModel: function () {
			var responseModel = {
				'ApplicationNumber': '',
				'UserName': '',
				'ReportId': '',
				'RefreshId': '',
				'EquifaxScore': 0,
                'ExperianScore': 0,
                'TransUnionScore': 0,
                'MortgageCount' : 0,
                'BankRuptcyCount':0,
                'ForeClosureCount':0,
                'RuleResult' : true,
                'Message' : '',
                'ReportType': 'IndividualCredit'
			};
			return responseModel;
		}
	}
}
