function creditValidationRule(payload) {
    var errorData = [];   
    try {
      
            var scoreRuleResult = executeScoreRule(payload.Borrowers);
          
            var mortgageLateRuleResult =  executeMortgageLateRule(payload.Borrowers);

            var bankRuptcyRuleResult =  executeBankRuptcyRuleResult(payload.Borrowers);        

            var foreClosuresRuleResult =  executeForeClosuresRuleResult(payload.Borrowers);

        var finalResult = scoreRuleResult && !mortgageLateRuleResult && !bankRuptcyRuleResult && !foreClosuresRuleResult;

        return {
            'result': true,
            'detail': null, 
            'data':finalResult,
            'extraData': {
                'scoreRuleResult':scoreRuleResult,
                'mortgageLateRuleResult':mortgageLateRuleResult,
                'bankRuptcyRuleResult':bankRuptcyRuleResult,
                'foreClosuresRuleResult':foreClosuresRuleResult
            }
            };

    } catch (e) {
        var msg = 'Error occured :' + e.message;
        errorData.push(msg);
        return {
            'result': false,
            'detail': errorData,
            'data': null
        };
    } 
    function executeMortgageLateRule(borrowers){
        var totalMortgageLateCountIn12Months = 0;
        if(borrowers.length > 0)
        {            
            for (var i = 0; i < borrowers.length;i++) { 
                if(borrowers[i].TotalMortgageLateCountIn12Months!=null)
                {
                    var totalMortgageLateCountIn12MonthsDataPayload=  borrowers[i].TotalMortgageLateCountIn12Months;
                    totalMortgageLateCountIn12Months = totalMortgageLateCountIn12Months +
                    totalMortgageLateCountIn12MonthsDataPayload.Days30 +
                    totalMortgageLateCountIn12MonthsDataPayload.Days60 +
                    totalMortgageLateCountIn12MonthsDataPayload.Days90 +
                    totalMortgageLateCountIn12MonthsDataPayload.Days120 ;
                }
            }            
        }
        return totalMortgageLateCountIn12Months>1;
    }
    function executeForeClosuresRuleResult(borrowers){
        if(borrowers.length > 0)
    {            
        var currentDate = new Date();
        for (var i = 0; i < borrowers.length;i++) { 
            if(borrowers[i].Foreclosures!=null)
            {                
               var toDate = borrowers[i].Foreclosures.map(function (a) {
                return Date.parse(a.LastActivityDate);
              });
                    
               if(toDate.length >0)
               {
                   for (var j = 0; j < toDate.length; ++j) {                        
                       var totalMonth = monthDiff(new Date( toDate[j]),currentDate);                       
                       if(totalMonth < 36){
                           return true;
                       }
                   }
               }
            }
        }            
    }
    return false;

    }
function executeBankRuptcyRuleResult(borrowers){        
    if(borrowers.length > 0)
    {            
        var currentDate = new Date();
        for (var i = 0; i < borrowers.length;i++) { 
            if(borrowers[i].Bankruptcies!=null)
            {                
               var toDate = borrowers[i].Bankruptcies.map(function (a) {
                return Date.parse(a.DispositionDate);
              });
                    
               if(toDate.length >0)
               {
                   for (var j = 0; j < toDate.length; ++j) {                        
                       var totalMonth = monthDiff(new Date( toDate[j]),currentDate) ;                                              
                       if(totalMonth < 24){

                           return true;
                       }
                   }
               }
            }
        }            
    }
    return false;

}
    function executTradeLineRule(payload)
    {
        //need to check for DispositionData as this field is not available right now.
        
        if(payload.Summary.NumberOfBankruptcyTradeLines < 1 && payload.Summary.NumberOfForeclosedTradeLines < 1
             && executeDispositionDateRule(payload.Borrowers) )
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    function executeDispositionDateRule(borrowers)
    {       
        if(borrowers.length > 0)
        {
            var currentDate = new Date();

                for (var i = 0; i < borrowers.length; ++i) {
                
                    if (typeof(borrowers[i].Bankruptcies) !== 'undefined') {
                    
                        var toDate = borrowers[i].Bankruptcies.map(function (a) {
                            return Date.parse(a.DispositionDate);
                          });
                    
                        if(toDate.length >0)
                        {
                            for (var j = 0; j < toDate.length; ++j) {                        
                                var totalMonth = monthDiff(new Date( toDate[j]),currentDate);                       
                                if(totalMonth < 24){
                                    return false;
                                }
                            }
                        }
                    }
                }
            //if no bankruptcies record found that time set to true.
            return true;
        }   
        return false;    
    }

    function executeScoreRule(borrowers)
    {
        var finalScore = 0;
        var borrowerFinalScore = [];
        if(borrowers.length > 0)
        { 
            for (var i = 0; i < borrowers.length; ++i) { 
                if(borrowers[i].Score.length > 0)
                {
                    if(borrowers[i].Score.length == 1)
                    {
                        borrowerFinalScore.push(parseInt(borrowers[i].Score[0].Value));                        
                    }
                    else if(borrowers[i].Score.length > 1 && borrowers[i].Score.length < 3)
                    {
                        var result = borrowers[i].Score.map(function (a) {
                            return parseInt(a.Value);
                          });
                        finalScore = Math.min.apply(null,result);
                        borrowerFinalScore.push(finalScore);
                    }
                    else 
                    {                   
                        var meidanScore = getScoreMedian(borrowers[i].Score);
                        borrowerFinalScore.push(parseInt(meidanScore));
                    }
                }
            }
        }
      
        if(borrowerFinalScore.length > 0)
        {         
          
            finalScore = Math.min.apply(null,borrowerFinalScore);
        }
   
        if(finalScore > 620)
            return true;
        else
            return false;
    }    

    function median(scoreArray) {
        scoreArray.sort(function(a, b) {
          return a - b;
        });
        var mid = scoreArray.length / 2;
        return mid % 1 ? scoreArray[mid - 0.5] : (scoreArray[mid - 1] + scoreArray[mid]) / 2;
      }

      function getScoreMedian(scoreArray)
      {
        var result = scoreArray.map(function (a) {
            return parseInt(a.Value);
          });
        if(result.length > 0)
        {
           
            return median(result);            
        }
        else
        {
            return 0;
        }
      }

      function monthDiff(d1, d2) {
        var d1Y = d1.getFullYear();
        var d2Y = d2.getFullYear();
        var d1M = d1.getMonth();
        var d2M = d2.getMonth();

        return (d2M+12*d2Y)-(d1M+12*d1Y);

        // var months;
        // months = (d2.getFullYear() - d1.getFullYear()) * 12;
        // months -= d1.getMonth() + 1;
        // months += d2.getMonth();
        // return months <= 0 ? 0 : months;
//         var diff =(d2.getTime() - d1.getTime()) / 1000;
//    diff /= (60 * 60 * 24 * 7 * 4);
//   return Math.abs(Math.round(diff));
    }
    
};

