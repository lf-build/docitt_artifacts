function checkPrimaryBorrowerHasSignedup(entityType, entityId){

    var self = this;
    var passed = 1;
    var failed = 2; 
    var questionnaireService = self.call('questionnaireService');
 
    return questionnaireService.checkPrimaryBorrowerHasSignedUp(entityId).then(function (response) {
        if(response == true)
        {
            return {
                'EntityType': entityType,
                'EntityId' : entityId,
                'Result' : passed
            };
        }
        else
        {
            return {
                'EntityType': entityType,
                'EntityId' : entityId,
                'Result' : failed
            };
        }
    }).catch(function (error) {
        return {
            'EntityType': entityType,
            'EntityId' : entityId,
            'Result' : failed
        };
    });
}