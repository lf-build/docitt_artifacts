function dtmResendInvitation(invitationId, inviteEmailId) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.resendInvitation(invitationId, inviteEmailId)
                        .then(function (inviteResponse) {
        return {
            status: 'success',
            data: inviteResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}
