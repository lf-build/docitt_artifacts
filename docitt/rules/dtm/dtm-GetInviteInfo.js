function dtmGetInviteByUser(userName) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.getInviteInfoByUser(userName).then(function (inviteResponse) {
        return {
            status: 'success',
            data: inviteResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}