function mapInviteIdToUsernameForAsset(inviteId, temporaryApplicationNumber, username) {
    var self = this;
    var syndicationService = self.call('syndicationService');
	
    return syndicationService.mapUsername(username, inviteId)
        .then(function(response) {
            return {
                status: 'success',
                data: response
            };
        }).catch(function(error) {
            return {
                status: 'error',
                error: error
            }
        });
}
