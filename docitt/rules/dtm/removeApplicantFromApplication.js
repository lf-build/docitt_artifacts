function removeApplicantFromApplication(applicationNumber,applicantList) {
    var self = this;
    var qfService = self.call('questionnairefilter');
    var assignmentService = self.call('inviteService');
    
    return qfService.removeApplicantFromApplication(applicationNumber,applicantList)
    .then(function (response) {     
            for (var username in applicantList) {
                
                assignmentService.unAssignUser('application', applicationNumber, username)
             }
             return {
                status: 'success',
                data: response
            };
                
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}
