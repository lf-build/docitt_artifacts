function checklargedeposits(customerId,applicationNumber) {
    var self = this;
    var syndicationService = self.call('syndicationService');
 
    return syndicationService.checklargedepositsinfo(customerId,applicationNumber).then(function (syndicationResponse) {
        return {
            status: 'success',
            data: syndicationResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}