function updateLastUpdatedOnAttribute(entityType, entityId, event) {
    var currentDate = new Date();
    return {
        "lastUpdatedOn" : currentDate
    };    
}