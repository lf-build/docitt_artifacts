function linkEConsentCondition(entityId, userName, conditionId) {
    var self = this;
    var consentTracker = self.call('consentTracker');
    var documentManagerService = self.call('documentManagerService');

    return  consentTracker.getConsents(entityId).then(function (responseConsents) {

        var filteredConsents = responseConsents.filter(function (item) {
            return item.signedBy === userName;
        });

        var promises = [];
        var documentIds = [];
        for (var key in filteredConsents) {
            var consentToUpdate = filteredConsents[key];
            documentIds.push(consentToUpdate.documentId);
            var payload = {"requestId": conditionId , "createdBy": userName};
            promises.push(documentManagerService.updateMetaData("application", entityId, consentToUpdate.documentId, payload));
        }
        return Promise.all(promises).then(function (responses) {
            return {
                status: 'success',
                data: documentIds
            };
        }).catch(function (error) {
            return {
                status: 'error1',
                error: error
            };
        });

    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}