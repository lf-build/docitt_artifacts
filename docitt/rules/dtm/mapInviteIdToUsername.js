function mapInviteIdToUsername(inviteId, temporaryApplicationNumber, username) {
    var self = this;
    var qfService = self.call('questionnairefilter');
    var appService = self.call('application');
 
    return qfService.mapInviteIdToUsername(inviteId, temporaryApplicationNumber, username)
                     .then(function (resQR) {
                        return appService.mapInviteIdToUsernameForApplication(inviteId, temporaryApplicationNumber, username)
                            .then(function (resApp) {
                            return {
                                status: 'success',
                                data: resApp
                            };
                        }).catch(function (error) {
                             return {
                                    status: 'error',
                                    error: error
                                }
                        });
                    }).catch(function (error) {
                        return {
                                status: 'error',
                                error: error
                            }
                    });
}
