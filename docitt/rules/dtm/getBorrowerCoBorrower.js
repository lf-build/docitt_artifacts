function getBorrowerCoBorrower(applicationNumber) {
    var self = this;
    var quesService = self.call('questionnaireService');
 
    return quesService.getBorrowerCoBorrowerWithSpouseInfo(applicationNumber).then(function (response) {
        return {
            status: 'success',
            data: response
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            };
    });
}
