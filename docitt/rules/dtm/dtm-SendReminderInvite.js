function dtmSendReminderInvite(inviteId, allowEmail) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.sendReminderInvite(inviteId, allowEmail).then(function (inviteResponse) {
        return {
            status: 'success',
            data: inviteResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}
