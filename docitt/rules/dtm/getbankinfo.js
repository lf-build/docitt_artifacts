function getbankinfo(customerId, entityId) {
    var self = this;
    var syndicationService = self.call('syndicationService');
 
    return syndicationService.getbankinfo(customerId, entityId).then(function (syndicationResponse) {
        return {
            status: 'success',
            data: syndicationResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}