function getInviteInfoById(invitationId) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.getInviteById(invitationId).then(function (inviteResponse) {
        return {
            status: 'success',
            data: inviteResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}