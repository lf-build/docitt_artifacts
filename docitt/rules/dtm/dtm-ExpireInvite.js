function dtmExpireInvite(invitationId) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.expireInvitation(invitationId).then(function (response) {
        return response;
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            };
    });
}
