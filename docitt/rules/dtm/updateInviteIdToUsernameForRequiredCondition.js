function updateInviteIdToUsernameForRequiredCondition(temporaryApplicationNumber, inviteId) {
    var self = this;
    var requiredConditionService = self.call('requiredConditionService');
	var entityType = "application";
    return requiredConditionService.updateInviteIdWithUserName(entityType, temporaryApplicationNumber, inviteId)
        .then(function(response) {
            return {
                status: 'success',
                data: response
            };
        }).catch(function(error) {
            return {
                status: 'error',
                error: error
            }
        });
}
