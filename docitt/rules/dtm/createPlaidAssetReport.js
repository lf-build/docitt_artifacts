function createPlaidAssetReport(
    clientReportId, 
    applicantId, 
    firstName,
    middleName,
    lastName,
    socialSecurityNumber,
    phoneNumber) {
    var self = this;
    var syndicationService = self.call('syndicationService');
 
    var payload = {
        'applicantId': applicantId,
        'clientReportId' : clientReportId,
        'firstName' : firstName,
        'middleName' : middleName,
        'lastName' : lastName,
        'socialSecurityNumber' : socialSecurityNumber,
        'phoneNumber' : phoneNumber
    };

    return syndicationService
    .createPlaidAssetReport(payload)
    .then(function (syndicationResponse) {
        return {
            status: 'success',
            data: syndicationResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}