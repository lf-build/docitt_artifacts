function dtmGetAssignmentInfo(entityType, entityId,role) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.getAssignmentInfo(entityType, entityId,role).then(function (assignmentResponse) {
        if(assignmentResponse != null && assignmentResponse != ""){
            return {
                status: 'success',
                data: true
            };
        }
        else{
            return {
                status: 'no-content',
                data: false
            };
        }
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}