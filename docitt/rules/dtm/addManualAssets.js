function addManualAssets(
    customerId,
    entityId,
    payload) {
    var self = this;
    var syndicationService = self.call('syndicationService');

    return syndicationService.deleteManualAssetList(customerId,'Banking',entityId)
    .then(function (response) {     

        return  syndicationService.addManualAssetList(customerId,entityId,payload)
        .then(function (addResponse) { 
            return {
                status: 'success',
                data: addResponse
            }    
            
        }).catch(function (error) {
             return {
                    status: 'error',
                    error: error
                }
        });
        
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
    
}