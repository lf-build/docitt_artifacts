function getuser(username) {
    var self = this;
    var securityService = self.call('securityService');
 
    return securityService.getUser(username).then(function (Response) {
        return {
            status: 'success',
            data: Response
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}