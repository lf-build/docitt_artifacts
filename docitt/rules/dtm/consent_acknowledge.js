function consentacknowledge(entityType, entityId, userName, payload, submitCondition) {
    var self = this;
    var FormData = require('form-data');
    var documentManagerService = self.call('documentManagerService');
    var conditionId;
    var consentTracker = self.call('consentTracker');
    var requiredConditionService = self.call('requiredConditionService');
    var consents = [];
    consents.push(consentTracker.sign(entityType, entityId, userName, "ConsolidatedDisclosure", payload));
    // consents.push(consentTracker.sign(entityType, entityId, userName, "TermsofService", payload));
    // consents.push(consentTracker.sign(entityType, entityId, userName, "electronicConsentAgreement", payload));
    // consents.push(consentTracker.sign(entityType, entityId, userName, "electronicSignature", payload));

    return Promise.all(consents).then(function (consentResponse) {

        if (submitCondition == true) {
            return requiredConditionService.get(entityType, entityId, "requested").then(function (response) {
              

                var conditions = response.filter(function (condition) {
                    return (condition.requestType.requestId === 'econsent' && condition.recipients.includes(userName));
                });
                if (conditions.length > 0) {
                    conditionId = conditions[0].id;
                }
                if(conditionId != null && conditionId != "")
                {
                    var promises = [];
                    consentResponse.forEach(function (consentItem) {
                        var payload = { "requestId": conditionId, "createdBy": userName };
                        promises.push(documentManagerService.updateMetaData(entityType, entityId, consentItem.documentId, payload));
                    })
                    return Promise.all(promises).then(function (responses) {
                
                        
                        var metadata = JSON.stringify({"createdBy": userName, "requestId": conditionId});
                        var payloadForSubmit = new FormData();
                        payloadForSubmit.append('metadata', metadata);
                        payloadForSubmit.append('files', '');
                        payloadForSubmit.append('explanation',"#noexplanation#");

                        return requiredConditionService.submitCondition(entityType, entityId, conditionId, payloadForSubmit)
                        .then(function (conditionResponse) {
                            return {
                                status: 'success',
                                data: consentResponse
                            };
                        })
                        .catch(function (error) {
                            return {
                                status: 'error',
                                error: error
                            }
                        });
                    })
                    .catch(function (error) {
                        return {
                            status: 'error',
                            error: error.message
                        }
                    })
                }
                else
                {
                    return {
                        status: 'success',
                        data: consentResponse
                    };
                }
               
            })
            .catch(function (error) {
                return {
                    status: 'error',
                    error: error
                }
            })
        }
        else{
            return {
                status: 'success',
                data: consentResponse
            };
        }
    }).catch(function (error) {
        return {
            status: 'error',
            error: error
        }
    });
}
