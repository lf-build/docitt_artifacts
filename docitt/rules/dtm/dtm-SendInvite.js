function dtmSendInvite(entityType, entityId, payload, allowEmail) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.sendInvite(entityType, entityId, payload, allowEmail).then(function (inviteResponse) {
        return {
            status: 'success',
            data: inviteResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}
