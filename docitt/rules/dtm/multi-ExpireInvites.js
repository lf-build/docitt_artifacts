function multiExpireInvites(invitationIds) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.expireInvitations(invitationIds).then(function (response) {
        return response;
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}
