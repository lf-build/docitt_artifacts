function applicationTeamAssignment(entityType,entityId,userName) {
    var self = this;
    var inviteService = self.call('inviteService');
 
    return inviteService.applicationTeamAssignment(entityType, entityId,userName).then(function (inviteResponse) {
        return {
            status: 'success',
            data: inviteResponse
        };
    }).catch(function (error) {
         return {
                status: 'error',
                error: error
            }
    });
}