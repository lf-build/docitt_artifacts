function flaggedQuestionIndexer(entityType, entityId, event) {
    var self = this;
    var finalResponse = {};
    var quesService = self.call('questionnaireService');

    return quesService.VerifyHasFlaggedQuestion(entityId)
        .then(function (flaggedQuestion) {
            finalResponse.isFlaggedQuestion = flaggedQuestion;
            return finalResponse;
        });
}