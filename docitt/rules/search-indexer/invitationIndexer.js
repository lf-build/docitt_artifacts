function invitationIndexer(entityType, entityId, event) {
    var self = this;
    var userProfileService = self.call('userProfileService');

    var getLoanOfficer = function (username) {
        return new Promise(function (resolve, reject) {
            finalResponse.loanOfficerName = "";
            return userProfileService.getProfile(username)
                .then(function(profileDetails){
                    if(profileDetails && profileDetails.title == "Loan Officer")
                    {
                        var fullName = profileDetails.firstName ? profileDetails.firstName : '' + ' ' + profileDetails.lastName ? profileDetails.lastName : '';
                        finalResponse.loanOfficer = { name : fullName , email : profileDetails.username};
                        finalResponse.loanOfficerName = fullName;    
                    }
                    resolve(finalResponse);
                })
                .catch(function (error) {
                    finalResponse.times.push({
                        name: "getLoanOfficer"
                    });
                    finalResponse.errors.push({
                        getLoanOfficer: JSON.stringify(error)
                    });
                    resolve(finalResponse);
                });
        });
    };

    var getAllRelationship = function (username) {
        var usersList = [];
        usersList.push({
            key: username,
            role: ''
        });
        finalResponse.assignees = usersList;

        return new Promise(function (resolve, reject) {
            return userProfileService.getAllRelationship(username)
                .then(function (relationship) {
                    if (relationship != null && relationship.length > 0) {
                        relationship.forEach(function (user) {
                            var key = user.memberName.toLowerCase();
                            var role = user.memberRole;
                            if (usersList[key] === undefined) {
                                usersList.push({
                                    key: key,
                                    role: role
                                });
                            }
                        });
                    }
                    finalResponse.assignees = usersList;
                    resolve(finalResponse);
                })
                .catch(function (error) {
                    finalResponse.assignees = usersList;
                    finalResponse.times.push({
                        name: "getAllRelationship"
                    });
                    finalResponse.errors.push({
                        getAllRelationship: JSON.stringify(error)
                    });
                    resolve(finalResponse);
                });
        });
    };


    var invitationData = event.Data.InviteData;
    if (invitationData.Role != "Borrower") {
        return null;
    }

    if (invitationData.UserId != null) {
        return null;
    }

    var startedAt = new Date();
    var finalResponse = {};
    finalResponse.errors = [];
    finalResponse.times = [];
    finalResponse.indexBuildStartedAt = startedAt.toISOString();
    var applicants = [];

    applicants.push({
        firstName: invitationData.InviteFirstName,
        lastName: invitationData.InviteLastName,
        applicantType: invitationData.Role,
        email: invitationData.InviteEmail,
        phone: invitationData.InviteMobileNumber,
        userName: null
    });
    finalResponse.fullName = ((invitationData.InviteFirstName != "") ? invitationData.InviteFirstName + " " : "") + invitationData.InviteLastName;
    finalResponse.applicants = applicants;
    finalResponse.isLead = true;
    finalResponse.lastReminder = invitationData.LastReminder;
    finalResponse.status = "Open";
    finalResponse.createdDate = {};
    finalResponse.createdDate.time = invitationData.InvitationDate;
    finalResponse.invitationDate = invitationData.InvitationDate;
    finalResponse.invitationToken = invitationData.InvitationToken;
    finalResponse.creditReportJobId = invitationData.CreditReportJobId;
    finalResponse.source = invitationData.Source;
    finalResponse.id = invitationData.Id;
    if (invitationData.PartnerDetails) {
        finalResponse.partnerDetails = {
            fullName: invitationData.PartnerDetails.FullName,
            displayRole: invitationData.PartnerDetails.DisplayRole,
            email: invitationData.PartnerDetails.Email,
            phone: invitationData.PartnerDetails.Phone
        };
    }
    var finishedAt = new Date();
    finalResponse.indexBuildFinishedAt = finishedAt.toISOString();
    finalResponse.timeToBuildIndex = finishedAt - startedAt;

    return Promise.all([getAllRelationship(invitationData.InvitedBy), getLoanOfficer(invitationData.InvitedBy)])
        .then(function () {
            return finalResponse;
        })
        .catch(function (reason) {
            finalResponse.errors.push({
                error: JSON.stringify(reason)
            });
            return finalResponse;
        });
}