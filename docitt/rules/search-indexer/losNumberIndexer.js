function losNumberIndexer(entityType, entityId, event) {
    var self = this;
    var dataAttributes = self.call('dataAttributes');
    var finalResponse = {};

    return dataAttributes.GetAttributeByName(entityId, "losNumber")
        .then(function (losNumbers) {
            finalResponse.losNumber = (losNumbers[0]).toString();
            return finalResponse;
        })
        .catch(function (response) {
            if (response) {
                if (response.toString().indexOf("was not found")) {
                    return null;
                }
            }
            return new Error(response);
        });
}