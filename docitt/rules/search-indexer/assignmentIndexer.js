function assignmentIndexer(entityType, entityId, event) {
    var self = this;
    var assignmentService = self.call('assignmentService');
    var userProfileService = self.call('userProfileService');
    var finalResponse = {};
    var usersList = [];
    var affinityAdmin;
    var loanOfficer = "";

    var getUserDetails = function (username) {
        if (username === "") {
            return Promise.resolve("");
        } else {
            return userProfileService.getProfile(username)
                .then(function (profileDetails) {
                    return profileDetails;
									
                }).catch(function () {
                    return "";
                });
        }
    };

    return assignmentService.fetchAssignedUser(entityId).then(function (assignedUsers) {
            if (assignedUsers.length > 0) {
                assignedUsers.forEach(function (user) {
                    var key = user.assignee.toLowerCase();

                    if (usersList[key] === undefined) {
                        usersList.push({
                            key: key,
                            role: user.role
                        });
                        if (user.role === "Affinity Partner Admin" || user.role === "Affinity Partner")
                            affinityAdmin = key;
                        if (user.role === "Loan Officer")
                            loanOfficer = key;
                    }
                });
            }
        })
        .then(function () {
            finalResponse.assignees = usersList;
            finalResponse.affinityAdmin = affinityAdmin;
            return getUserDetails(loanOfficer).then(function (user) {
                var fullName = user.firstName + ' ' + user.lastName;
                finalResponse.loanOfficer = {
                    name: fullName,
                    email: loanOfficer,
                    branchName : user.branchName, 
                    nmlsNumber : user.nmlsNumber
                };
                finalResponse.loanOfficerName = fullName;
            });
        })
        .then(function () {
            if (finalResponse.affinityAdmin) {
                return userProfileService.getProfile(finalResponse.affinityAdmin)
                    .then(function (affinityAdminDetails) {
                        if (affinityAdminDetails) {
                            finalResponse.partnerDetails = {
                                fullName: affinityAdminDetails.firstName + " " + affinityAdminDetails.lastName,
                                displayRole: affinityAdminDetails.title,
                                email: affinityAdminDetails.email,
                                phone: affinityAdminDetails.phone ? affinityAdminDetails.phone : affinityAdminDetails.mobileNumber
                            };
                        }
                    }).catch(function (reason) {});
            } else {
                return Promise.resolve();
            }
        }).then(function () {
            return finalResponse;
        });
}