function creditValidationFailedRule(entityType, entityId, event) {
    var finalResponse = {};
    finalResponse.validationFailed = true;
    return finalResponse;
}