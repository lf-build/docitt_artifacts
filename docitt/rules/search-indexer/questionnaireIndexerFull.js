function questionnaireIndexerFull(entityType, entityId, event) {
    var self = this;
    var quesService = self.call('questionnaireService');
    var securityService = self.call('securityService');
    var assignmentService = self.call('assignmentService');
    var userProfileService = self.call('userProfileService');
    var activityLogService = self.call('activityLogService');
    var statusManagementService = self.call('statusManagementService');
    var reminderService = self.call('reminderService');
    var dataAttributes = self.call('dataAttributes');
    var startedAt = new Date();
    var finalResponse = {};
    finalResponse.errors = [];
    finalResponse.times = [];
    finalResponse.indexBuildStartedAt = startedAt.toISOString();

    var getUnReadReminderCount = function () {
        return new Promise(function (resolve, reject) {
            if (finalResponse.isApplicationSubmitted === false) {
                resolve(finalResponse);
                return;
            }
            var start = new Date();
            return reminderService.getUnReadReminderCount(entityId)
                .then(function (reminderResponse) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "getUnReadReminderCount"
                    });
                    finalResponse.totalUnReadReminders = reminderResponse;
                    resolve(finalResponse);
                }).catch(function (reason) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "getUnReadReminderCount"
                    });
                    finalResponse.errors.push({
                        getUnReadReminderCount: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };

    var getUnReadChatMessages = function () {
        return new Promise(function (resolve, reject) {
            if (finalResponse.isApplicationSubmitted === false) {
                resolve(finalResponse);
                return;
            }
            var start = new Date();
            return reminderService.getUnReadReminders(entityId, "chat-notification").then(function (reminders) {
                if (reminders != null) {
                    for (var i in reminders) {
                        if (finalResponse.chatReminders[reminders[i].userName] != null) {
                            finalResponse.chatReminders[reminders[i].userName] = finalResponse.chatReminders[reminders[i].userName] + 1;
                        } else {
                            finalResponse.chatReminders[reminders[i].userName] = 0;
                        }
                    }
                    resolve(finalResponse);
                }
            });
        });
    }

    var getUsersLastLoggedIn = function () {
        return new Promise(function (resolve, reject) {
            var start = new Date();
            return securityService.getUser(finalResponse.borrowerUserName).then(function (user) {
                finalResponse.times.push({
                    start: start,
                    end: new Date(),
                    name: "getUsersLastLoggedIn"
                });
                finalResponse.lastLoggedIn = user.lastLoggedIn;
                resolve(finalResponse);
            }).catch(function (reason) {
                finalResponse.times.push({
                    start: start,
                    end: new Date(),
                    name: "getUsersLastLoggedIn"
                });
                finalResponse.errors.push({
                    getUsersLastLoggedIn: JSON.stringify(reason)
                });
                resolve(finalResponse);
            });
        });
    };

    var getUserDetails = function (username) {
        return new Promise(function (resolve, reject) {
            if (username === "")
                resolve("");

            return userProfileService.getProfile(username)
                .then(function (profileDetails) {

                    resolve(profileDetails);
                }).catch(function () {
                    resolve({});
                });
        });
    };

    var getAssignment = function () {
        return new Promise(function (resolve, reject) {
            var start = new Date();
            return assignmentService.fetchAssignedUser(entityId).then(function (assignedUsers) {
                var usersList = [];
                var affinityAdmin;
                var loanOfficer = "";
                if (assignedUsers.length > 0) {
                    assignedUsers.forEach(function (user) {
                        var key = user.assignee.toLowerCase();
                        if (usersList[key] === undefined) {
                            usersList.push({
                                key: key,
                                role: user.role
                            });

                            if (user.role === "Affinity Partner Admin" || user.role === "Affinity Partner")
                                affinityAdmin = key;

                            if (user.role === "Loan Officer")
                                loanOfficer = key;
                        }
                    });
                }
                finalResponse.assignees = usersList;
                finalResponse.affinityAdmin = affinityAdmin;
                finalResponse.times.push({
                    start: start,
                    end: new Date(),
                    name: "getAssignment"
                });

                getUserDetails(loanOfficer).then(function (user) {
                    var fullName = user.firstName + ' ' + user.lastName;
                    finalResponse.loanOfficer = {
                        name: fullName,
                        email: loanOfficer,
                        branchName: user.branchName,
                        nmlsNumber: user.nmlsNumber
                    };
                    finalResponse.loanOfficerName = fullName;
                    resolve(finalResponse);
                }).catch(function (reason) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "getUserDetails"
                    });
                    finalResponse.errors.push({
                        getUserDetails: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });


            }).catch(function (reason) {
                finalResponse.times.push({
                    start: start,
                    end: new Date(),
                    name: "getAssignment"
                });
                finalResponse.errors.push({
                    getAssignment: JSON.stringify(reason)
                });
                resolve(finalResponse);
            });
        });
    };

    var getActivity = function () {
        return new Promise(function (resolve, reject) {
            var start = new Date();
            return activityLogService.GetActivitiesByTag(entityId).then(function (activityHistory) {
                var filteredReminderHistory = activityHistory.filter(function (activity) {
                        return activity.tags.includes("Reminder");
                    })
                    .sort(function (a, b) {
                        return b.activityDate - a.activityDate;
                    });
                if (filteredReminderHistory.length > 0)
                    finalResponse.lastReminder = filteredReminderHistory[0].activityDate;
                finalResponse.times.push({
                    start: start,
                    end: new Date(),
                    name: "getActivity"
                });
                resolve(finalResponse);
            }).catch(function (reason) {
                finalResponse.times.push({
                    start: start,
                    end: new Date(),
                    name: "getActivity"
                });
                finalResponse.errors.push({
                    getActivity: JSON.stringify(reason)
                });
                resolve(finalResponse);
            });
        });
    };
    var getStatusByEntity = function () {
        return new Promise(function (resolve, reject) {
            if (finalResponse.isApplicationSubmitted === false) {
                resolve(finalResponse);
                return;
            }
            var start = new Date();
            return statusManagementService.getStatusByEntity(entityId)
                .then(function (applicationStatus) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "getStatusByEntity"
                    });
                    if (Object.keys(applicationStatus).length > 0) {
                        finalResponse.applicationStatus = {
                            'code': applicationStatus.code,
                            'name': applicationStatus.name,
                            'label': applicationStatus.label
                        };
                        finalResponse.activeOn = applicationStatus.activeOn;
                        resolve(finalResponse);
                    }
                }).catch(function (reason) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "getStatusByEntity"
                    });
                    finalResponse.errors.push({
                        getStatusByEntity: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };
    var flaggedQuestion = function () {
        return new Promise(function (resolve, reject) {
            if (finalResponse.isApplicationSubmitted === false) {
                resolve(finalResponse);
                return;
            }
            var start = new Date();
            return quesService.VerifyHasFlaggedQuestion(entityId)
                .then(function (flaggedquestion) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "isFlaggedQuestion"
                    });
                    finalResponse.isFlaggedQuestion = flaggedquestion;
                    resolve(finalResponse);
                })
                .catch(function (reason) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "isFlaggedQuestion"
                    });
                    finalResponse.errors.push({
                        isFlaggedQuestion: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };
    var getLosNumber = function () {
        return new Promise(function (resolve, reject) {
            if (finalResponse.isApplicationSubmitted === false) {
                resolve(finalResponse);
                return;
            }
            var start = new Date();
            return dataAttributes.GetAttributeByName(entityId, "losNumber")
                .then(function (losNumbers) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "LosNumber"
                    });
                    finalResponse.losNumber = (losNumbers[0]).toString();
                    resolve(finalResponse);
                })
                .catch(function (reason) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "LosNumber"
                    });
                    finalResponse.errors.push({
                        LosNumber: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    }
    var getLastUpdatedOn = function () {
        return new Promise(function (resolve, reject) {
            var start = new Date();
            return dataAttributes.GetAttributeByName(entityId, "lastUpdatedOn")
                .then(function (response) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "lastUpdatedOn"
                    });
                    finalResponse.lastUpdatedOn = response[0];
                    resolve(finalResponse);
                })
                .catch(function (reason) {
                    finalResponse.times.push({
                        start: start,
                        end: new Date(),
                        name: "lastUpdatedOn"
                    });
                    finalResponse.errors.push({
                        lastUpdatedOn: JSON.stringify(reason)
                    });
                    finalResponse.lastUpdatedOn = finalResponse.submittedDate;
                    resolve(finalResponse);
                });
        });
    }
    var start = new Date();
    return quesService.getApplication(entityId).then(function (application) {
            finalResponse.times.push({
                start: start,
                end: new Date(),
                name: "getApplication"
            });
            finalResponse.temporaryApplicationNumber = application.temporaryApplicationNumber;
            finalResponse.loanType = application.loanPurpose;
            finalResponse.loanAmount = application.loanAmount === null ? 0 : parseInt(application.loanAmount.toString().replace(/,/g, ""));
            finalResponse.createdDate = application.createdDate;
            finalResponse.lastUpdatedOn = application.createdDate;
            finalResponse.status = application.status;
            finalResponse.creditReportJobId = application.creditReportJobId;
            finalResponse.pageReached = application.highestSubSectionName;
            finalResponse.sourceType = application.sourceType;
            finalResponse.propertyAddress = application.propertyAddress;
            finalResponse.downPayment = application.downPayment === null ? 0 : parseFloat(application.downPayment.toString().replace(/,/g, ""));
            finalResponse.propertyValue = application.propertyValue;
            finalResponse.isSubmittedByBackOfficeUser = application.isSubmittedByBackOfficeUser;
            if (application.expirationDate != null)
                finalResponse.expirationDate = application.expirationDate.time;
            if (application.submittedDate != null)
                finalResponse.submittedDate = application.submittedDate.time;
            finalResponse.sectionDetails = {};
            finalResponse.isApplicationSubmitted = false;
            if (application.status != null && application.status.toLowerCase() != "open") {
                finalResponse.isApplicationSubmitted = true;
            }
            var applicants = [];
            var sections = [];
            var totalCompletedSection = 0;
            var lastSectionReached = "";
            if (application.borrowerCoBorrowerList != null) {
                application.borrowerCoBorrowerList.forEach(function (borrower) {
                    applicants.push({
                        firstName: borrower.firstName,
                        lastName: borrower.lastName,
                        applicantType: borrower.applicantType,
                        email: borrower.email,
                        phone: borrower.phone,
                        userName: borrower.userName.toLowerCase(),
                        isPrimary: borrower.applicantType.toLowerCase() == "borrower" ? true : false,
                        name: borrower.firstName + ' ' + borrower.lastName,
                        creditReportJobId: borrower.creditReportJobId
                    });
                    if (borrower.applicantType.toLowerCase() == "borrower") {
                        finalResponse.borrowerUserName = borrower.userName.toLowerCase();
                        finalResponse.fullName = ((borrower.firstName != "") ? borrower.firstName + " " : "") + borrower.lastName;
                        finalResponse.borrower = {
                            firstName: borrower.firstName,
                            lastName: borrower.lastName,
                            emailAddress: borrower.email,
                            phoneNumber: borrower.phone,
                            appplicationSubmitted: borrower.isApplicationSubmitted
                        };
                    } else if (borrower.applicantType.toLowerCase() == "spouse" && !finalResponse.spouse) {
                        finalResponse.spouse = {
                            firstName: borrower.firstName,
                            lastName: borrower.lastName,
                            emailAddress: borrower.email,
                            phoneNumber: borrower.phone,
                            appplicationSubmitted: borrower.isApplicationSubmitted
                        };
                    } else if (borrower.applicantType.toLowerCase() == "coborrower" && !finalResponse.coBorrower) {
                        finalResponse.coBorrower = {
                            firstName: borrower.firstName,
                            lastName: borrower.lastName,
                            emailAddress: borrower.email,
                            phoneNumber: borrower.phone,
                            appplicationSubmitted: borrower.isApplicationSubmitted
                        };
                    } else if (borrower.applicantType.toLowerCase() == "coborrowerspouse" && !finalResponse.coBorrowerSpouse) {
                        finalResponse.coBorrowerSpouse = {
                            firstName: borrower.firstName,
                            lastName: borrower.lastName,
                            emailAddress: borrower.email,
                            phoneNumber: borrower.phone,
                            appplicationSubmitted: borrower.isApplicationSubmitted
                        };
                    }
                });
            }
            if (application.sections != null) {
                var applicationSections = application.sections.sort(function (a, b) {
                    return a.seqNo - b.seqNo;
                });
                applicationSections.forEach(function (section) {
                    finalResponse.sectionDetails[section.sectionName + "IsCompleted"] = section.isCompleted;
                    if (section.isCompleted) {
                        totalCompletedSection += 1;
                    } else if (lastSectionReached == "") {
                        lastSectionReached = section.sectionName;
                    }
                    sections.push({
                        seqNo: section.seqNo,
                        sectionName: section.sectionName,
                        isCompleted: section.isCompleted
                    });
                });
            }
            finalResponse.applicants = applicants;
            finalResponse.sections = sections;
            finalResponse.totalCompletedSection = totalCompletedSection;
            finalResponse.lastSectionReached = lastSectionReached;
            return finalResponse;
        })
        .then(function () {
            return Promise.all([
                getUsersLastLoggedIn(),
                getAssignment(),
                getActivity(),
                getStatusByEntity(),
                flaggedQuestion(),
                getUnReadReminderCount(),
                getLosNumber(),
                getLastUpdatedOn(),
                getUnReadChatMessages()
            ]);
        })
        .then(function () {
            if (finalResponse.affinityAdmin) {
                return userProfileService.getProfile(finalResponse.affinityAdmin)
                    .then(function (affinityAdminDetails) {
                        if (affinityAdminDetails) {
                            finalResponse.partnerDetails = {
                                fullName: affinityAdminDetails.firstName + " " + affinityAdminDetails.lastName,
                                displayRole: affinityAdminDetails.title,
                                email: affinityAdminDetails.email,
                                phone: affinityAdminDetails.phone ? affinityAdminDetails.phone : affinityAdminDetails.mobileNumber
                            };
                        }
                        return finalResponse;
                    }).catch(function (reason) {
                        finalResponse.errors.push({
                            getProfile: JSON.stringify(reason)
                        });
                        return finalResponse;
                    });
            } else
                return finalResponse;
        }).then(function () {
            var finishedAt = new Date();
            finalResponse.indexBuildFinishedAt = finishedAt.toISOString();
            finalResponse.timeToBuildIndex = finishedAt - startedAt;
            return finalResponse;
        })
        .catch(function (reason) {
            finalResponse.errors.push({
                error: JSON.stringify(reason)
            });
            return finalResponse;
        });


}