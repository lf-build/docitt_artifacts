function missingQuestionsFields(entitytype, entityId, event) {
    var data = {};
    var questionList = [];
    if (event.Data.RequiredQuestions.length > 0) {
        event.Data.RequiredQuestions.forEach(function (question) {
            questionList.push({
                applicantType: event.Data.ApplicantType.toLowerCase(),
                questionFieldName: question.QuestionFieldName
            });
        });
    }
    data["applyTags"] = questionList;
    return data;
}