function creditReportRequestFailedRule(entityType, entityId, event) {
    var finalResponse = {};
    finalResponse.requestFailed = true;
    return finalResponse;
}