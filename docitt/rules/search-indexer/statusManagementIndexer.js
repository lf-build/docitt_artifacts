function statusManagementIndexer(entityType, entityId, event) {
    var self = this;
    var statusManagementService = self.call('statusManagementService');
    var finalResponse = {};
    return statusManagementService.getStatusByEntity(entityId)
        .then(function (applicationStatus) {
            if (Object.keys(applicationStatus).length > 0) {
                finalResponse.applicationStatus = {
                    'code': applicationStatus.code,
                    'name': applicationStatus.name,
                    'label': applicationStatus.label
                };
                finalResponse.activeOn = applicationStatus.activeOn;
                return finalResponse;
            }
        }).catch(function (response) {
            if (response) {
                if (response.toString().indexOf("was not found")) {
                    return null;
                }
            }
            throw new Error(response);
        });
}