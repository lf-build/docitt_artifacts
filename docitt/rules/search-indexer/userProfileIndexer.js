function userProfileIndexer(entityType, entityId, event) {
    var self = this;
    var securityService = self.call('securityService');
    var userProfileService = self.call('userProfileService');

    var startedAt = new Date();
    var finalResponse = {};
    finalResponse.errors = [];
    finalResponse.times = [];
    finalResponse.indexBuildStartedAt = startedAt.toISOString();
    entityId = entityId.toLowerCase();

    var start = new Date();
    return securityService.getUser(entityId).then(function (user) {
        finalResponse.times.push({start:start,end:new Date(),name:"getUser"});
        finalResponse.userName = user.username;
        finalResponse.isActive = user.isActive;
        finalResponse.source = user.source;
        finalResponse.roles = user.roles;
        finalResponse.permissions = user.permissions;
        finalResponse.createdOn = user.createdOn;
        finalResponse.lastActiveDeactiveOn = user.lastActiveDeactiveOn;
        finalResponse.lastLoggedIn = user.lastLoggedIn;
        return finalResponse;
    })
    .then(function(finalResponse){
        var start = new Date();
        return userProfileService.getProfile(entityId)
                    .then(function (userProfileDetails) {
                        finalResponse.times.push({start:start,end:new Date(),name:"getUserProfile"});
                        var fullName = [];
                        if (userProfileDetails.firstName) {
                            fullName.push(userProfileDetails.firstName)
                        }
                        if (userProfileDetails.lastName) {
                            fullName.push(userProfileDetails.lastName)
                        }
                        finalResponse.fullName = fullName.join(" ");
                        finalResponse.firstName = userProfileDetails.firstName;
                        finalResponse.lastName = userProfileDetails.lastName;
                        finalResponse.companyName = userProfileDetails.companyName;
                        finalResponse.title = userProfileDetails.title;
                        finalResponse.license = userProfileDetails.license;
                        finalResponse.mobileNumber = userProfileDetails.mobileNumber;
                        finalResponse.officePhone = userProfileDetails.officePhone;
                        finalResponse.phone = userProfileDetails.phone;
                        finalResponse.fax = userProfileDetails.fax;
                        finalResponse.email = userProfileDetails.email;
                        finalResponse.address = userProfileDetails.address;
                        finalResponse.city = userProfileDetails.city;
                        finalResponse.state = userProfileDetails.state;
                        finalResponse.zipCode = userProfileDetails.zipCode;
                        finalResponse.linkedInUrl = userProfileDetails.linkedInUrl;
                        finalResponse.facebookUrl = userProfileDetails.facebookUrl;
                        finalResponse.twitterUrl = userProfileDetails.twitterUrl;
                        finalResponse.communication = userProfileDetails.communication;
                        finalResponse.branchName = userProfileDetails.branchName;
                        finalResponse.note = userProfileDetails.note;
                        finalResponse.companyId = userProfileDetails.companyId;
                        finalResponse.relationships = userProfileDetails.relationships;
                        finalResponse.settings = userProfileDetails.settings;
                        finalResponse.nmlsNumber = userProfileDetails.nmlsNumber;

                        return finalResponse;
                    }).catch(function (reason) {
                        finalResponse.errors.push({
                            getProfile: JSON.stringify(reason)
                        });
                        return finalResponse;
                    });
    })
    .then(function(finalResponse){
        var finishedAt = new Date();
        finalResponse.indexBuildFinishedAt = finishedAt.toISOString();
        finalResponse.timeToBuildIndex = finishedAt - startedAt;
        return finalResponse;
    })
    .catch(function (reason) {
        finalResponse.errors.push({
            error: reason
        });
        return finalResponse;
    });
}