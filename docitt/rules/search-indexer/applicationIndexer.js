function applicationIndexer(entityType, entityId, event) {
    var self = this;
    var assignmentService = self.call('assignmentService');
    var applicationService = self.call('application');
    var reminderService = self.call('reminderService');
    var statusManagementService = self.call('statusManagementService');
    var questionnaireService = self.call('questionnaireService');
    var dataAttributes = self.call('dataAttributes');
    var userProfileService = self.call('userProfileService');
    var startedAt=new Date();

    var finalResponse = {};
    finalResponse.errors = [];
    finalResponse.times = [];
    finalResponse.indexBuildStartedAt=startedAt.toISOString();

    var getUnReadReminderCount = function () {
        return new Promise(function (resolve, reject) {
            var start=new Date();

            return reminderService.getUnReadReminderCount(entityId)
                .then(function (reminderResponse) {
                    finalResponse.times.push({start:start,end:new Date(),name:"getUnReadReminderCount"});
                    finalResponse.totalUnReadReminders = reminderResponse;
                    resolve(finalResponse);
                }).catch(function (reason) {
                    finalResponse.times.push({start:start,end:new Date(),name:"getUnReadReminderCount"});
                    finalResponse.errors.push({
                        getUnReadReminderCount: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };

    var getFullName = function(username){
        return new Promise(function(resolve, reject){
            if(username === "")
                resolve("");
            
            return userProfileService.getProfile(username)
                            .then(function (profileDetails) {
                                var fullName = profileDetails.firstName ? profileDetails.firstName : '' + ' ' + profileDetails.lastName ? profileDetails.lastName : '';
                                resolve(fullName);
            }).catch(function(){
                resolve("");
            });
        });
    };

    var getApplicationAssignees = function () {
        return new Promise(function (resolve, reject) {
            var start=new Date();
            return assignmentService.getApplicationAssignees(entityId)
                .then(function (assignedUsers) {
                    var usersList = [];
                    var loanOfficer = "";
                    if (assignedUsers.length > 0) {
                        assignedUsers.forEach(function (user) {
                            var key = user.assignee.toLowerCase();
                          
                            if (usersList[key] === undefined) {
                              usersList.push({
                                key: key,
                                role: user.role
                              });
                              if (user.role === "Loan Officer") loanOfficer = key;
                            }
                          });
                    }
                    finalResponse.times.push({start:start,end:new Date(),name:"getApplicationAssignees"});
                    finalResponse.assignees = usersList;
                    
                    getFullName(loanOfficer).then(function(val) {
                        finalResponse.loanOfficer = { name : val , email : loanOfficer};
                        finalResponse.loanOfficerName = val; 
                        resolve(finalResponse);
                    }).catch(function(reason){
                        finalResponse.times.push({start:start, end:new Date(), name:"getFullName"});
                        finalResponse.errors.push({
                            getFullName: JSON.stringify(reason)
                        });
                        resolve(finalResponse);
                    });

                }).catch(function (reason) {
                    finalResponse.times.push({start:start,end:new Date(),name:"getApplicationAssignees"});
                    finalResponse.errors.push({
                        getApplicationAssignees: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };

    var getStatusByEntity = function () {
        return new Promise(function (resolve, reject) {
            var start=new Date();

            return statusManagementService.getStatusByEntity(entityId)
                .then(function (applicationStatus) {
                    finalResponse.times.push({start:start,end:new Date(),name:"getStatusByEntity"});
                    if (Object.keys(applicationStatus).length > 0) {
                        finalResponse.status = {
                            'code': applicationStatus.code,
                            'name': applicationStatus.name,
                            'label': applicationStatus.label
                        };
                        finalResponse.activeOn = applicationStatus.activeOn;
                        resolve(finalResponse);
                    }
                }).catch(function (reason) {
                    finalResponse.times.push({start:start,end:new Date(),name:"getStatusByEntity"});
                    finalResponse.errors.push({
                        getStatusByEntity: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };

    var getStatusHistory = function () {
        return new Promise(function (resolve, reject) {
            var start=new Date();
            return statusManagementService.getStatusHistory(entityId)
                .then(function (applicationStatus) {
                    if (finalResponse.status && Object.keys(finalResponse.status).length > 0) {
                        finalResponse.times.push({start:start,end:new Date(),name:"getStatusHistory"});
                        var statusList = [];
                        if (applicationStatus.length > 0) {
                            applicationStatus.forEach(function (item) {
                                if (item.status != null && item.status !== finalResponse.status.code) {
                                  statusList.push({
                                    "status": item.status
                                  });
                                }
                              });
                        }
                        finalResponse.completedStatuses = statusList;
                        resolve(finalResponse);
                    } else
                        resolve(finalResponse);
                })
                .catch(function (reason) {
                    finalResponse.times.push({start:start,end:new Date(),name:"getStatusHistory"});
                    finalResponse.errors.push({
                        getStatusHistory: JSON.stringify(reason)
                    });
                    resolve(finalResponse);
                });
        });
    };

    var flaggedQuestion = function () {
        return new Promise(function (resolve, reject) {
            var start=new Date();
            return questionnaireService.VerifyHasFlaggedQuestion(entityId)
            .then(function (flaggedquestion) {
                finalResponse.times.push({start:start,end:new Date(),name:"isFlaggedQuestion"});
                finalResponse.isFlaggedQuestion = flaggedquestion;
                resolve(finalResponse);
            })
            .catch(function (reason) {
                finalResponse.times.push({start:start,end:new Date(),name:"isFlaggedQuestion"});
                finalResponse.errors.push({
                    isFlaggedQuestion: JSON.stringify(reason)
                });
                resolve(finalResponse);
            });
        });
    };

    var getLoanAmount = function () {
        return new Promise(function (resolve, reject) {
            var start=new Date();
            return questionnaireService.getApplication(entityId)
            .then(function (response) {
                finalResponse.times.push({start:start, end:new Date(), name:"getLoanAmount"});
                finalResponse.amount = isNaN(Number(response.loanAmount)) ? 0 : Number(response.loanAmount);
                resolve(finalResponse);
            })
            .catch(function (reason) {
                finalResponse.times.push({start:start, end:new Date(), name:"getLoanAmount"});
                finalResponse.errors.push({
                    LoanAmount : JSON.stringify(reason)
                });
                resolve(finalResponse);
            });
        });
    };

    var getLosNumber = function () {
        return new Promise(function (resolve, reject) {
             var start=new Date();
             return dataAttributes.GetAttributeByNames(entityId)
             .then(function (losNumbers) {
                 finalResponse.times.push({start:start,end:new Date(),name:"LosNumber"});
                 finalResponse.losNumber = (losNumbers[0]).toString();
                 resolve(finalResponse);
             })
             .catch(function (reason) {
                 finalResponse.times.push({start:start,end:new Date(),name:"LosNumber"});
                 finalResponse.errors.push({
                     LosNumber: JSON.stringify(reason)
                });
                 resolve(finalResponse);
             });
        });
    }

    var start=new Date();

    return applicationService.getByApplicationNumber(entityId)
        .then(function (response) {
            finalResponse.times.push({start:start,end:new Date(),name:"getApplication"});

            if (response.applicants) {
                var applicants = [];
                response.applicants.forEach(function (applicant) {

                    var applicantFilterView = {
                        'applicantId': applicant.id,
                        'firstName': applicant.firstName,
                        'lastName': applicant.lastName,
                        'email': applicant.email,
                        'name': applicant.firstName + " " + applicant.lastName,
                        'ssn': applicant.ssn,
                        'isPrimary': applicant.isPrimary,
                        'userName': (applicant.userIdentity != null) ? applicant.userIdentity.userId : "",
                        'applicantType': applicant.applicantType
                    };
                    if (applicant.phoneNumbers.length > 0) {
                        var primaryPhoneNumber = applicant.phoneNumbers.find(function (p) {
                            return p.isPrimary;
                          });
                        if (primaryPhoneNumber.length > 0) {
                            applicantFilterView.phoneNumber = primaryPhoneNumber[0].number ? primaryPhoneNumber[0].number : "";
                            applicantFilterView.extension = primaryPhoneNumber[0].extension ? primaryPhoneNumber[0].extension : "";
                        }
                    }

                    if (applicant.isPrimary) {
                        finalResponse.borrowerUserName = (applicant.userIdentity != null) ? applicant.userIdentity.userId : "";
                        finalResponse.fullName = ((applicant.firstName != "") ? applicant.firstName + " " : "" ) +  applicant.lastName;
                    }

                    var address = "";
                    var city = "";
                    var state = "";
                    var zipCode = "";
                    var country = "";
                    if (applicant.addresses && applicant.addresses.length > 0) {
                        var primaryAddress = applicant.addresses.find(function (p) {
                            return p.isPrimary;
                          });

                        if (primaryAddress && primaryAddress.length > 0) {
                            address = primaryAddress[0].line1 ? primaryAddress[0].line1 : "";
                            address += primaryAddress[0].line2 ? primaryAddress[0].line2 : "";
                            address += primaryAddress[0].line3 ? primaryAddress[0].line3 : "";

                            city = primaryAddress[0].city ? primaryAddress[0].city : "";
                            state = primaryAddress[0].state ? primaryAddress[0].state : "";

                            country = primaryAddress[0].country ? primaryAddress[0].country : "";
                            zipCode = primaryAddress[0].zipCode ? primaryAddress[0].zipCode : "";
                        }
                    }
                    applicantFilterView.address = address;
                    applicantFilterView.city = city;
                    applicantFilterView.state = state;

                    applicantFilterView.country = country;
                    applicantFilterView.zipCode = zipCode;
                    applicants.push(applicantFilterView);
                });
                finalResponse.applicants = applicants;
                finalResponse.applicationId = entityId;
                finalResponse.submittedDate = response.submittedDate.time;
                finalResponse.sourceId = response.source.id;
                finalResponse.sourceType = response.source.type.toLowerCase();
                finalResponse.amount = response.amount;
                finalResponse.propertyValue = response.propertyValue;
                finalResponse.propertyAddress = response.propertyAddress;
                finalResponse.creditReportJobId = response.creditReportJobId;

                 if (response.expirationDate != null)
                 finalResponse.ExpirationDate = response.expirationDate.time;

                 var expDate = new Date(finalResponse.ExpirationDate); 
                 var subDate = new Date(finalResponse.submittedDate); 

                 var differenceInTime = expDate.getTime() - subDate.getTime(); 
                finalResponse.daysToClose = differenceInTime/(1000 * 3600 * 24);

                finalResponse.loanType = response.purpose;

            }
            return finalResponse;
        })
        .then(function () {
            return Promise.all([getLoanAmount(), getUnReadReminderCount(), getApplicationAssignees(), getStatusByEntity(), getStatusHistory(), flaggedQuestion(), getLosNumber()
            ]);
        })
        .then(function () {
            var finishedAt=new Date();
            finalResponse.indexBuildFinishedAt=finishedAt.toISOString();
            finalResponse.timeToBuildIndex= finishedAt - startedAt;
            return finalResponse;
        })
        .catch(function (reason) {
            finalResponse.errors.push({
                error: JSON.stringify(reason)
            });
            return finalResponse;
        });
}