function userLoggedInIndexer(entityType, entityId, event) {
    var self = this;
    var searchService = self.call('searchService');
    return searchService.searchQuestionnaireByUserName(event.Data.Username).then(function (response) {
        var data = {};
        if (response != null && response._doc != null) {
            response._doc.forEach(function (doc) {
                data[doc.id] = {
                    "lastLoggedIn": event.Time.Time
                };
            });
        }
        return data;
    });
}