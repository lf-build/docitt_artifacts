function reminderIndexer(entityType, entityId, event) {
    var self = this;
    var reminderService = self.call('reminderService');
    var finalResponse = {};
    finalResponse.chatReminders=[];

    return reminderService.getUnReadReminderCount(entityId)
        .then(function (reminderResponse) {
            finalResponse.totalUnReadReminders = reminderResponse;
        }).catch(function(){

        }).then(function(){
            return reminderService.getUnReadReminders(entityId,"chat-notification").then(function(reminders){
                if(reminders!=null){
                    var tmp={};
                    for(var i in reminders){
                        if(tmp[reminders[i].userName]!=null){
                            tmp[reminders[i].userName]=tmp[reminders[i].userName]+1;
                        }
                        else{
                            tmp[reminders[i].userName]=0;
                        }
                    }
                    for(var key in tmp){
                        finalResponse.chatReminders.push({name:key,count:tmp[key]});
                    }
                }
                return finalResponse;
            }).catch(function(){
                return finalResponse;
            });
        });
}