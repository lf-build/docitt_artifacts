function invitationReminderIndexer(entityType, entityId, event) {
    var invitationData = event.Data.InviteData;
    if (invitationData.Role != "Borrower") {
        return null;
    }
    if (invitationData.UserId != null) {
        return null;
    }
    var SourceEnum = "StaticApplication";
    if (invitationData.Source == SourceEnum) {
        return null;
    }
    var startedAt = new Date();
    var finalResponse = {};
    finalResponse.errors = [];
    finalResponse.times = [];
    finalResponse.indexBuildStartedAt = startedAt.toISOString();
    var usersList = [];
    usersList.push({
        key: invitationData.InvitedBy,
        role: ''
    });
    var applicants = [];

    applicants.push({
        firstName: invitationData.InviteFirstName,
        lastName: invitationData.InviteLastName,
        applicantType: invitationData.Role,
        email: invitationData.InviteEmail,
        phone: invitationData.InviteMobileNumber,
        userName: null
    });
    finalResponse.fullName = ((invitationData.InviteFirstName != "") ? invitationData.InviteFirstName + " " : "") + invitationData.InviteLastName;
    finalResponse.applicants = applicants;
    finalResponse.assignees = usersList;
    finalResponse.isLead = true;
    finalResponse.lastReminder = invitationData.LastReminder;
    finalResponse.status = "Open";
    finalResponse.createdDate = {};
    finalResponse.createdDate.time = invitationData.InvitationDate;
    finalResponse.invitationDate = invitationData.InvitationDate;
    finalResponse.invitationToken = invitationData.InvitationToken;
    finalResponse.creditReportJobId = invitationData.CreditReportJobId;
    finalResponse.source = invitationData.Source;
    finalResponse.id = invitationData.Id;
    if (invitationData.PartnerDetails) {
        finalResponse.partnerDetails = {
            fullName: invitationData.PartnerDetails.FullName,
            displayRole: invitationData.PartnerDetails.DisplayRole,
            email: invitationData.PartnerDetails.Email,
            phone: invitationData.PartnerDetails.Phone
        };
    }

    var finishedAt = new Date();
    finalResponse.indexBuildFinishedAt = finishedAt.toISOString();
    finalResponse.timeToBuildIndex = finishedAt - startedAt;
    return finalResponse;
}