function questionnaireIndexer(entityType, entityId, event) {
    var self = this;
    var quesService = self.call('questionnaireService');
    var startedAt = new Date();
    var finalResponse = {};
    finalResponse.errors = [];
    finalResponse.times = [];
    finalResponse.indexBuildStartedAt = startedAt.toISOString();
    if(event.Name=="QuestionnaireApplicationCreated"){
        finalResponse.lastLoggedIn = event.Time.Time;
    }
    var start = new Date();
    return quesService.getApplication(entityId).then(function (application) {
            finalResponse.times.push({
                start: start,
                end: new Date(),
                name: "getApplication"
            });
            finalResponse.temporaryApplicationNumber = application.temporaryApplicationNumber;
            finalResponse.loanType = application.loanPurpose;
            finalResponse.loanAmount = application.loanAmount === null ? 0 : parseInt(application.loanAmount.toString().replace(/,/g, ""));
            finalResponse.createdDate = application.createdDate;
            finalResponse.status = application.status;
            finalResponse.creditReportJobId = application.creditReportJobId;
            finalResponse.pageReached = application.highestSubSectionName;
            finalResponse.sourceType = application.sourceType;
            finalResponse.propertyAddress = application.propertyAddress;
            finalResponse.downPayment = application.downPayment === null ? 0 : parseFloat(application.downPayment.toString().replace(/,/g, ""));
            finalResponse.propertyValue = application.propertyValue;
            finalResponse.isSubmittedByBackOfficeUser = application.isSubmittedByBackOfficeUser;
            if (application.expirationDate != null)
                finalResponse.expirationDate = application.expirationDate.time;
            if (application.submittedDate != null)
                finalResponse.submittedDate = application.submittedDate.time;
            finalResponse.sectionDetails = {};
            finalResponse.isApplicationSubmitted = false;
            if (application.status != null && application.status.toLowerCase() != "open") {
                finalResponse.isApplicationSubmitted = true;
            }
            var applicants = [];
            var sections = [];
            var totalCompletedSection = 0;
            var lastSectionReached = "";
            if (application.borrowerCoBorrowerList != null) {
                application.borrowerCoBorrowerList.forEach(function (borrower) {
                    applicants.push({
                        firstName: borrower.firstName,
                        lastName: borrower.lastName,
                        applicantType: borrower.applicantType,
                        email: borrower.email,
                        phone: borrower.phone,
                        userName: borrower.userName.toLowerCase(),
                        isPrimary: borrower.applicantType.toLowerCase() == "borrower" ? true : false,
                        name: borrower.firstName + ' ' + borrower.lastName,
                        creditReportJobId: borrower.creditReportJobId
                    });
                    if (borrower.applicantType.toLowerCase() == "borrower") {
                        finalResponse.borrowerUserName = borrower.userName.toLowerCase();
                        finalResponse.fullName = ((borrower.firstName != "") ? borrower.firstName + " " : "") + borrower.lastName;
                        finalResponse.borrower = { firstName : borrower.firstName, lastName : borrower.lastName,
                            emailAddress : borrower.email, phoneNumber : borrower.phone, appplicationSubmitted : borrower.isApplicationSubmitted };
                    }
                    else if (borrower.applicantType.toLowerCase() == "spouse" && !finalResponse.spouse) {
                        finalResponse.spouse = { firstName : borrower.firstName, lastName : borrower.lastName,
                            emailAddress : borrower.email, phoneNumber : borrower.phone, appplicationSubmitted : borrower.isApplicationSubmitted };
                    }
                    else if (borrower.applicantType.toLowerCase() == "coborrower" && !finalResponse.coBorrower) {
                        finalResponse.coBorrower = { firstName : borrower.firstName, lastName : borrower.lastName,
                            emailAddress : borrower.email, phoneNumber : borrower.phone, appplicationSubmitted : borrower.isApplicationSubmitted };
                    }
                    else if (borrower.applicantType.toLowerCase() == "coborrowerspouse" && !finalResponse.coBorrowerSpouse) {
                        finalResponse.coBorrowerSpouse = { firstName : borrower.firstName, lastName : borrower.lastName,
                            emailAddress : borrower.email, phoneNumber : borrower.phone, appplicationSubmitted : borrower.isApplicationSubmitted };
                    }
                });
            }
            if (application.sections != null) {
                var applicationSections = application.sections.sort(function (a, b) {
                    return a.seqNo - b.seqNo;
                });
                applicationSections.forEach(function (section) {
                    finalResponse.sectionDetails[section.sectionName + "IsCompleted"] = section.isCompleted;
                    if (section.isCompleted) {
                        totalCompletedSection += 1;
                    } else if (lastSectionReached == "") {
                        lastSectionReached = section.sectionName;
                    }
                    sections.push({
                        seqNo: section.seqNo,
                        sectionName: section.sectionName,
                        isCompleted: section.isCompleted
                    });
                });
            }
            finalResponse.applicants = applicants;
            finalResponse.sections = sections;
            finalResponse.totalCompletedSection = totalCompletedSection;
            finalResponse.lastSectionReached = lastSectionReached;
            return finalResponse;
        }).then(function () {
            var finishedAt = new Date();
            finalResponse.indexBuildFinishedAt = finishedAt.toISOString();
            finalResponse.timeToBuildIndex = finishedAt - startedAt;
            return finalResponse;
        })
        .catch(function (reason) {
            finalResponse.errors.push({
                error: JSON.stringify(reason)
            });
            return finalResponse;
        });
}