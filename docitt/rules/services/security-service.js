function securityService() {
    var self = this;
    var baseUrl = '{{rule_identity_baseurl}}';

    return {
        isUsernameAvailable: function (username) {
            var url = [baseUrl, 'check'].join('/');
            
            return new Promise(function (resolve, reject) {
                self.http.post(url,{ username: username })
                    .then(function (response) {
                        resolve(response.available);
                    }).catch(function (error) {
                        reject(error)
                    });
            });
        },
        create:function(user){
            var url = baseUrl;
            
            return new Promise(function (resolve, reject) {
                self.http.put(url, user)
                    .then(function (response) {
                        resolve(response);
                    }).catch(function (error) {
                        reject(error);
                    });
            });
        },
        getUser:function(username){    
            var url = [baseUrl, username].join('/');
            return new Promise(function (resolve, reject) {
                self.http.get(url)
                    .then(function (response) {
                        resolve(response);
                    }).catch(function (error) {
                        reject(error);
                    });
            });
        }
    };
}