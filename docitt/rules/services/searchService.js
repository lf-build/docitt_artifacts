function searchService(){
    var self = this;
    var baseUrl = '{{rule_search_baseurl}}';
    return {
        searchQuestionnaireByUserName: function(userName){
            var url = [baseUrl,"questionnaire","search"].join('/');
            return self.http.post(url,
                {
                    query: {
                        bool: {
                            must: [
                                {
                                    match: {
                                        "questionnaire.applicants.userName.keyword": userName
                                    }
                                }
                            ]
                        }
                    },
                    from: 0,
                    size: 1000
                }
            );
        }
    };
}
