function userProfileService() {
    var self = this;
    var baseUrl = '{{rule_profile_baseurl}}';

    return {
        getProfileByUserId:function(userIds){    
            var url = [baseUrl, 'user', userIds].join('/');
            return new Promise(function (resolve, reject) {
                self.http.get(url)
                    .then(function (response) {
                        resolve(response);
                    }).catch(function (error) {
                        reject(error);
                    });
            });
        },
        getProfile: function(userName){
            var url = [baseUrl, userName].join('/');
            return new Promise(function (resolve,reject) {
                self.http.get(url)
                .then(function (response) {
                    resolve(response);
                }).catch(function (error) {
                    reject(error);
                });
            });
        },
        getAllRelationship: function(userName){
            var url = [baseUrl, userName, 'relationships','all'].join('/');
            return new Promise(function(resolve, reject){
                self.http.get(url)
                .then(function(response){
                    resolve(response);
                })
                .catch(function(error){
                    reject(error);
                });
            });
        }
    };
}