function documentManagerService() {
	var self = this;
	var baseUrl = '{{rule_document-manager_baseurl}}';

	return {
		updateMetaData: function (entityType, entityId, documentId, metadata) {
			var url = [baseUrl, entityType, entityId, documentId, 'metadata'].join('/');
			return self.http.post(url, metadata)
				.then(function (response) {
					return response;
				})
				 .catch(function (error) {
				 	return Promise.reject('document-manager failed for ' + documentId + ' ' + error);
				 });
		}
	};
}