function application(){
    var self = this;
    var baseUrl = '{{rule_application_baseurl}}';
    return {
        mapInviteIdToUsernameForApplication : function(inviteId, temporaryApplicationNumber, username){
            var url = '';            
            url = [ baseUrl, 'map', 'inviteid', inviteId, username, 'temporaryapplicationnumber',temporaryApplicationNumber].join('/');
            return self.http.put(url, {});
        },
        getByApplicationNumber: function(applicationNumber){
            var url = '';
            url = [ baseUrl,applicationNumber].join('/');
            return self.http.get(url,{});
        }
    };
}
