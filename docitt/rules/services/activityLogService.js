function activityLogService(){
    var self = this;
    var baseUrl = '{{rule_activitylog_baseurl}}';
    return {        
        GetActivitiesByTag: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber].join('/');
            return self.http.get(url,{});
        }

    };
}