function consentTracker() {
	var self = this;
	var baseUrl = '{{rule_consent-tracker_baseurl}}';

	var consentIsValid = function (response) {
		try {
			if (response == null) {
				return false;
			};

			if (response.consentName == null) {
				return false;
			};

			return true;
		} catch (error) {
			return false;
		}
	};

	return {
		sign: function (entityType, entityId, userName, consentName, payload) {
			var url = [baseUrl, entityType, entityId, userName, consentName, 'sign'].join('/');

			return self.http.post(url, payload)
				.then(function (response) {
					 if (!consentIsValid(response)) {
					 	return Promise.reject('consent-tracker failed for ' + consentName);
					 }

					return response;
				})
				 .catch(function (error) {
				 	return Promise.reject('consent-tracker failed for ' + consentName + ' '+ error.message);
				 });
		},
		getConsents: function(entityId){
            var url = '';
            url = [ baseUrl,'application',entityId].join('/');
            return self.http.get(url,{});
        }
	};
}