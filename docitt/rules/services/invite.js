function inviteService(){
    var self = this;
    var baseUrl = '{{rule_assignmentservice_baseurl}}';
    return {
        sendInvite : function(entityType, entityId, payload, allowEmail){
            var url = '';            
            url = [ baseUrl, entityType, entityId,'invite','send', allowEmail].join('/');
            return self.http.post(url, payload);
        },
        applicationTeamAssignment:function(entityType, entityId,userName){    
            var url = '';     
            url = [ baseUrl, entityType, entityId,userName,'assignment'].join('/');
            return self.http.put(url,{});
        },
        getInviteInfoByUser:function(userName){    
            var url = '';
            url = [ baseUrl,'invite',userName,'info'].join('/');
            return self.http.get(url,{});
        },
        getAssignmentInfo:function(entityType, entityId,role){    
            var url = '';
            url = [ baseUrl, entityType, entityId, 'assignment',role].join('/');
            return self.http.get(url,{});
        } ,
        expireInvitation:function(invitationId){    
            var url = '';
            url = [ baseUrl, 'expireinvitation', invitationId].join('/');
            return self.http.post(url,{});
        },
        sendReminderInvite:function(invitationId, allowEmail){
            var url = '';
            url = [baseUrl, 'invite-lead', invitationId, 'send-reminder', allowEmail].join('/');
            return self.http.put(url,{});
        },
        expireInvitations:function(invitationIds){    
            var url = '';
            url = [ baseUrl, 'expire-invitations'].join('/');
            return self.http.put(url,invitationIds);
        },
        unAssignUser:function(entityType,entityId,userName)
        {
            var url = '';     
            url = [ baseUrl, entityType, entityId,'release',userName].join('/');
            return self.http.post(url,{});
        },
        resendInvitation: function(invitationId, inviteEmailId)
        {
            var url = '';     
            url = [ baseUrl, 'regenerateinvitation', invitationId, inviteEmailId].join('/');
            return self.http.put(url,{});
        },
        getInviteById: function(invitationId)
        { 
            var url = '';
            url = [ baseUrl,'invite-by-id',invitationId].join('/');
            return self.http.get(url,{});
        }
    };
}
