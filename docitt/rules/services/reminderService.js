function reminderService(){
    var self = this;
    var baseUrl = '{{rule_reminder_baseurl}}';
    return {        
        getUnReadReminderCount: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber,'unread','count'].join('/');
            return self.http.get(url,{});
        },
        getUnReadReminders: function(applicationNumber, source){
            var url = '';
            url = [ baseUrl,'application',applicationNumber,'unread','all',source].join('/');
            return self.http.get(url,{});
        },
        add: function(entityType, entityId, userName, payload){
            var url = '';
            url = [baseUrl,entityType, entityId, userName].join('/');
            self.http.post(url, payload);
            return true;
        }
    };
}