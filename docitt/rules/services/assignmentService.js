function assignmentService(){
    var self = this;
    var baseUrl = '{{rule_assignmentservice_baseurl}}';  
    return {        
        fetchAssignedUser: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber, 'assignments'].join('/');
            return self.http.get(url,{});
        },
        getApplicationAssignees: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber,'assignments'].join('/');
            return self.http.get(url,{});
        }
    };
}
