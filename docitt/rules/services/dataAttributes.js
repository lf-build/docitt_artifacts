function dataAttributes(){
    var self = this;
    var baseUrl = '{{rule_data-attributes_baseurl}}';
    return {        
        GetAttributeByName:function(applicationNumber,name){    
            var url = '';
            url = [ baseUrl,'application',applicationNumber,name].join('/');
            return self.http.get(url,{});
        }
    };
};