function emailService(){
    var self = this;
    var baseUrl = '{{rule_email_baseurl}}';

    return {
        sendEmail : function(entityType, entityId, templateName, payload){
            var url = '';
            url = [baseUrl, entityType, entityId, templateName].join('/');
            return self.http.post(url, payload);
        }
    };
}