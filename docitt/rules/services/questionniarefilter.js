function questionnairefilter() {
    var self = this;
    var baseUrl = '{{rule_docitt-questionnaire-filters_baseurl}}';
    return {
        mapInviteIdToUsername: function (inviteId, temporaryApplicationNumber, username) {
            var url = '';
            url = [baseUrl, 'map', 'inviteid', inviteId, username, 'temporaryapplicationnumber', temporaryApplicationNumber].join('/');
            return self.http.put(url, {});
        },
        removeApplicantFromApplication: function (applicationNumber, applicantList) {
            var url = '';
            url = [baseUrl, 'remove-applicant', applicationNumber].join('/');
            return self.http.put(url, applicantList);
        }
    };
}
