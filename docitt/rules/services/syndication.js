function syndicationService() {
    var self = this;
    var baseUrl = '{{rule_docittcapacityconnect_baseurl}}';
    var plaidUrl = '{{rule_docitt-syndication-plaid_plaidurl}}';
    var filethisUrl = '{{rule_docitt-syndication-filethis}}';
    return {
        getbankinfo: function (customerId, entityId) {
            var url = '';
            if (entityId) {
                url = [baseUrl, 'customer', customerId, 'banking', 'accountswithasset', entityId].join('/');
            }
            else {
                url = [baseUrl, 'customer', customerId, 'banking', 'accountswithasset'].join('/');
            }
            return self.http.get(url, {});
        },
        checklargedepositsinfo: function (customerId, applicationNumber) {
            var url = '';
            url = [baseUrl, 'customer', customerId, applicationNumber, 'has-large-deposits'].join('/');
            return self.http.post(url, {});
        },
        createPlaidAssetReport: function (payload) {
            var url = '';
            url = [plaidUrl, 'assetreport', 'create'].join('/');
            return self.http.post(url, payload);
        },
        mapUsername: function (customerId, inviteId) {
            var url = '';
            url = [baseUrl, 'mapusername', 'customer', customerId, 'invite', inviteId].join('/');
            return self.http.post(url, {});
        },
        addManualAssetList : function(customerId,entityId,payload){
            var url = '';
            url = [baseUrl, 'customer', customerId,'institution-list','bank-detail',entityId].join('/');
            return self.http.post(url, payload);         
        },
        deleteManualAssetList : function(customerId,type,entityId){
            var url = '';
            url = [baseUrl, 'customer', customerId,type,'manual-bank',entityId].join('/');
            return self.http.delete(url,{});    
        },
        createAssetReport: function(entityId, customerId, requestId){
            var url = '';
            var payload = {'customerId' : customerId, 'requestId' : requestId};
            url = [baseUrl, 'create', 'assetreport', entityId].join("/");
            return self.http.post(url, payload);
        },
        hasPlaidLinking: function(entityId, customerId){
            var url = '';
            url = [plaidUrl, 'item', 'has-plaid-linking', 'entityId', entityId, 'applicant', customerId].join("/");
            return self.http.get(url, {});
        },
        getLinkingFlag: function(entityId, customerId){
            var url = '';
            url = [baseUrl, 'linking-flag', 'entity', entityId, 'customer', customerId].join("/");
            return self.http.get(url, {});
        },
        getRequestIdByConnectionId: function(connectionId){
            var url = '';
            url = [filethisUrl, 'requestId', 'connection', connectionId].join("/");
            return self.http.get(url, {});
        }
    };
}
