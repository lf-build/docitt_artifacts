/*
	Description : This rule is a Proxy for the send required condition service e.g. POST required-condition:5023/{entityType}/{entityId}/invite/send/ 
	Author : @Suresh
	Consume by: Decision Engine/Rules
	
*/
function requiredConditionService(){
    var self = this;
    var baseUrl = '{{rule_required-condition_baseurl}}';
    return {
        addDefaultRequiredCondition : function(entityType, entityId, payload){
            var url = '';            
            url = [ baseUrl, entityType, entityId ].join('/');
            return self.http.put(url, payload);
        },
        updateInviteIdWithUserName : function(entityType, entityId, inviteId){
            var url = '';            
            url = [ baseUrl, entityType, entityId, 'update-recipients-from', inviteId,'to-username' ].join('/');
            return self.http.put(url, {});
        },
        get : function(entityType, entityId, status) {
            var url = '';
            url = [baseUrl, entityType, entityId, 'status', status].join('/');
            return self.http.get(url);
        },
        submitCondition: function(entityType, entityId, conditionId, payload){
            var url = [ baseUrl, entityType, entityId, conditionId, 'submit', true].join('/');
            return self.http.post(url, payload)
                .then(function (response) {
                    return response;
                })
                .catch(function (error) {
                    return Promise.reject('submitCondition failed for ' + conditionId + ' ' + error.message);
                });
        }
    };
}