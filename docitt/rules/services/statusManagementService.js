function statusManagementService(){
    var self = this;
    var baseUrl = '{{rule_status-management_baseurl}}';
    return {        
        getStatusByEntity: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber].join('/');
            return self.http.get(url,{});
        },
        getStatusHistory: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber,'history'].join('/');
            return self.http.get(url,{});
        }


    };
}