function twilioService(){
    var self = this;
    var baseUrl = '{{rule_twilio-otp_baseurl}}'; 
    return {
        sendTextMessage : function(payload){
            var url = '';            
            url = [baseUrl,"send"].join('/');
            return self.http.post(url, payload);
        }     
    };
}