function questionnaireService(){
    var self = this;
    var baseUrl = '{{rule_docitt-questionnaire_baseurl}}';
    return {        
        getBorrowerCoBorrowerInfo:function(applicationNumber){    
            var url = '';
            url = [ baseUrl,applicationNumber,'borrowers-and-co-borrowers'].join('/');
            return self.http.get(url,{});
        },
        getBorrowerCoBorrowerWithSpouseInfo:function(applicationNumber){    
            var url = '';
            url = [ baseUrl,applicationNumber,'borrowers-and-co-borrowers-spousename'].join('/');
            return self.http.get(url,{});
        },
        getApplication: function(applicationNumber){
            var url = '';
            url = [ baseUrl,'application',applicationNumber].join('/');
            return self.http.get(url,{});
        },
        VerifyHasFlaggedQuestion: function(applicationNumber){
            var url = '';
            url = [ baseUrl,applicationNumber, "has-flagged-question"].join('/');
            return self.http.get(url,{});
        },
        checkPrimaryBorrowerHasSignedUp: function(applicationNumber){
            var url = '';
            url = [ baseUrl, applicationNumber, "has-borrower-logged"].join('/');
            return self.http.get(url,{});
        },
        getLoanDetail: function(applicationNumber, borrowerUserName){
            var url = '';
            url = [ baseUrl, applicationNumber, "user", borrowerUserName, "loan-detail"].join('/');
            return self.http.get(url,{});
        }

    };
}
