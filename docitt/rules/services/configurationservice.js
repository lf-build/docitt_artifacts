function configurationService(){
    var self = this;
    var baseUrl = '{{rule_configuration_baseurl}}'; 
    return {
        getTwilioOtpDetails : function(){
            var url = '';            
            url = [baseUrl,"twilio-otp"].join('/');
            return new Promise(function (resolve, reject) {
                self.http.get(url)
                    .then(function (response) {
                        resolve(response);
                    }).catch(function (error) {
                        reject(error)
                    });
            });
        }     
    };
}