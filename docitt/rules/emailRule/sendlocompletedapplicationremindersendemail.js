function sendlocompletedapplicationremindersendemail(payload) {
    var self = this;
    var eService = self.call('emailService'); 
    var pData = payload.Data || payload.data;
    if(pData.sendEmailNotification)
    {
        return eService.sendEmail("application", pData.TemporaryApplicationNumber, pData.borrowerTemplateName, pData).then(function (response) {
        return {
            status: 'success',
            error: ""
        };
    }).catch(function (error) {
         return {
                status: 'error2',
                error: error
            };
    });
}
}