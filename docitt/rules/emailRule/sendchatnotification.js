function sendchatnotification(payload) {
    var self = this;
    var emailService = self.call('emailService'); 
    var twilioService = self.call('twilioService'); 
    var configurationService = self.call('configurationService'); 
    var userProfileService = self.call('userProfileService');
    var reminderService = self.call('reminderService');
    var pData = payload.Data;
    if (pData.name !== "chatsmsemailnotification") return;
    else {
        var responseBody = pData.Response.body;
        var userIdArray = pData.Response.body[0].from.split('-');
        var entityId = userIdArray[0];
        var fromUserId = userIdArray[1];
        if (pData.Response.body[0].to) { // For individual Chat
            var toUserId = pData.Response.body[0].to.split('-')[1];
            var initialPromises = [];
            initialPromises.push(configurationService.getTwilioOtpDetails());
            initialPromises.push(userProfileService.getProfileByUserId(fromUserId + "/" + toUserId));
            return Promise.all(initialPromises)
            .then(function (responses) {
                var countryCode = responses[0].DefaultCountryCode;
                var fromUserProfile = responses[1].filter(function (item) {
                    return item.userId === fromUserId;
                })[0];

                var toUserProfile = responses[1].filter(function (item) {
                    return item.userId === toUserId;
                })[0];
                var phoneNumber;
                if(toUserProfile.mobileNumber !== null) {
                    phoneNumber = toUserProfile.mobileNumber;
                } 
                else phoneNumber = toUserProfile.phone;
                var email = toUserProfile.email;
                //var message = "<br/>";
                var obj = {}
                responseBody.slice(0).reverse().map(function (data) {
                    var time = new Date(data.timeStamp);
                    var hours = time.getHours();
                    var minutes = time.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    var userId = data.from.split('-')[1];
                    var message = data.message +" ["+ strTime+ "]";
                    if (obj.hasOwnProperty(userId)) {
                        obj[userId].push(message)
                    }
                    else {
                        obj[userId] = [message]
                    }
                });
                var userDetails = [];
                var allUsers = [];
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        allUsers.push(key)
                    }
                }

                userDetails.push(userProfileService.getProfileByUserId(allUsers.join('/')));

                return Promise.all(userDetails)
                    .then(function (usersMessageFrom) {
                        var usersMessage = usersMessageFrom[0]
                        var body = "";
                        var emailBody = "";

                        for (var key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                var message = obj[key].join('<br/>')
                                var userDetail = usersMessage.find(function (user) {
                                    return user.userId == key;
                                  });
                                body += "<br/>You have a new chat message from "+ userDetail.firstName + " " + userDetail.lastName + ": " + message + "<br/>";
                                emailBody += "<br/>You have a new chat message(s) waiting from " + userDetail.firstName + " " + userDetail.lastName + ".<br/><br/>Message(s):<br/>" + message + "<br/>";
                            }
                        }
                        var smsPayload = {
                            CountryCode : countryCode,
                            PhoneNumber : phoneNumber,
                            Body : body
                        };
                        var emailPayload = {
                            "Email": email, 
                            "toUserName" : toUserProfile.firstName + " " + toUserProfile.lastName, 
                            "fromUserName": fromUserProfile.firstName + " " + fromUserProfile.lastName, 
                            "message": emailBody 
                        };
                        var metadata = JSON.stringify({ "groupId": responseBody[0].to });
                        var reminderPayload = {"Source": "chat-notification", "Description" : "You have a new message from '" + fromUserProfile.firstName + " " + fromUserProfile.lastName + "'", "Metadata" : metadata };
         
                        var promises = [];
                        promises.push(emailService.sendEmail("application", entityId, "chatEmail", emailPayload));
                        promises.push(reminderService.add("application", entityId, toUserProfile.email, reminderPayload));
                        if(phoneNumber) promises.push(twilioService.sendTextMessage(smsPayload));
                
                        return Promise.all(promises)
                        .then(function (responses) {
                            return {
                                status: 'success',
                                data: JSON.stringify(responses[0]) + ">>>>>>>/n" +  JSON.stringify(responses[1]) + ">>>>>>>/n" +  JSON.stringify(responses[2])
                            };
                            }).catch(function (error) {
                                    return {
                                            status: 'error1',
                                            error: error
                                    };
                            });
                    })
                    .catch(function (error) {
                        return {
                            status: 'error3',
                            error: error
                        };
                    });
            }).catch(function (error) {
                        return {
                                status: 'error2',
                                error: error
                        };
            });
        }
        else if(pData.Response.body[0].clientGroupId && pData.Response.body[0].userIds){ // For Group Chat
            var allUserIds = pData.Response.body[0].userIds;
            var initialPromises = [];
            var groupUserIds = [];
            groupUserIds.push(fromUserId);

            allUserIds.forEach(function (toUserId) {
                var userIdArray = toUserId.split('-');
                groupUserIds.push(userIdArray[1]);
            });
            initialPromises.push(userProfileService.getProfileByUserId(groupUserIds.join('/')));
            initialPromises.push(configurationService.getTwilioOtpDetails());

            return Promise.all(initialPromises)
            .then(function (responses) {
                var countryCode = responses[1].DefaultCountryCode;
                var fromUserProfile = responses[0].filter(function (item) {
                    return item.userId === fromUserId;
                })[0];
                var metadata = JSON.stringify({ "groupId": pData.Response.body[0].clientGroupId });
                var reminderPayload = {"Source": "chat-notification", "Description" : "You have a new message from '" + fromUserProfile.firstName + " " + fromUserProfile.lastName + "'", "Metadata" : metadata };
                var time = new Date( pData.Response.body[0].timeStamp);
                var hours = time.getHours();
                var minutes = time.getMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                var message =  pData.Response.body[0].message +" ["+ strTime+ "]";
                var body = "";
                var emailBody = "";
                body += "<br/>You have a new chat message from "+ fromUserProfile.firstName + " " + fromUserProfile.lastName + ": " + message + "<br/>";
                emailBody += "<br/>You have a new chat message(s) waiting from " + fromUserProfile.firstName + " " + fromUserProfile.lastName + ".<br/><br/>Message(s):<br/>" + message + "<br/>";
                var promises = [];
                groupUserIds.forEach(function (userId) {
                    if(userId !== fromUserId){
                        var toUserProfile = responses[0].filter(function (item) {
                            return item.userId === userId;
                        })[0];
                        var phoneNumber = "";
                        if(toUserProfile.mobileNumber !== null) {
                            phoneNumber = toUserProfile.mobileNumber;
                        } 
                        else phoneNumber = toUserProfile.phone;
                        var email = toUserProfile.email;
                        var smsPayload = {
                            CountryCode : countryCode,
                            PhoneNumber : phoneNumber,
                            Body : body
                        };
                        var emailPayload = {
                            "Email": email, 
                            "toUserName" : toUserProfile.firstName + " " + toUserProfile.lastName, 
                            "fromUserName": fromUserProfile.firstName + " " + fromUserProfile.lastName, 
                            "message": emailBody 
                        };
                        promises.push(emailService.sendEmail("application", entityId, "chatEmail", emailPayload));
                        promises.push(reminderService.add("application", entityId, toUserProfile.email, reminderPayload));
                        if(phoneNumber) promises.push(twilioService.sendTextMessage(smsPayload));
                    }
                });
                return Promise.all(promises)
                .then(function (newResponses) {
                    return {
                        status: 'success',
                        data: JSON.stringify(newResponses[0]) + ">>>>>>>/n" +  JSON.stringify(newResponses[1]) + ">>>>>>>/n" +  JSON.stringify(newResponses[2])
                    };
                    }).catch(function (error) {
                            return {
                                    status: 'error1',
                                    error: error
                            };
                    });

            }).catch(function (error) {
                return {
                    status: 'error2',
                    error: error
                };
            });
        }
    }
}