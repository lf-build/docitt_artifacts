function sendBorrowerInvitationAccepted(payload) {
    var self = this;
    var eService = self.call('emailService'); 
    var pData = payload.Data;
   
   if(pData.LoanOfficerEmail){
    pData.Email = pData.LoanOfficerEmail;
   eService.sendEmail("application", pData.ApplicationNumber, pData.LoanOfficerTemplateName, pData).then(function (response) {
}).catch(function (error) {
    return {
           status: 'error2',
           error: error
       }
});
   }
   if(pData.AffinityPartnerEmail){
    pData.Email = pData.AffinityPartnerEmail;
   eService.sendEmail("application", pData.ApplicationNumber, pData.AffinityPartnerTemplateName, pData).then(function (response) {
}).catch(function (error) {
    return {
           status: 'error2',
           error: error
       }
});
   }
    
}