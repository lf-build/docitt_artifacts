function sendapplicationsubmittedemail(payload) {
    var self = this;
    var eService = self.call('emailService'); 
    var pData = payload.Data;
    if(pData.IsManuallySubmitted){
        return;
    }
    eService.sendEmail("application", pData.TemporaryApplicationNumber, pData.borrowerTemplateName, pData).then(function (response) {
    }).catch(function (error) {
        return {
               status: 'error2',
               error: error
           }
   });
   if(pData.loanOfficerEmail){
    pData.Email = pData.loanOfficerEmail;
   eService.sendEmail("application", pData.TemporaryApplicationNumber, pData.loanOfficerTemplateName, pData).then(function (response) {
}).catch(function (error) {
    return {
           status: 'error2',
           error: error
       }
});
   }
   if(pData.AffinityPartnerEmail){
    pData.Email = pData.AffinityPartnerEmail;
   eService.sendEmail("application", pData.TemporaryApplicationNumber, pData.AffinityPartnerTemplateName, pData).then(function (response) {
}).catch(function (error) {
    return {
           status: 'error2',
           error: error
       }
});
   }
    
}