'use strict';
var path = require('path');
var fs = require('fs');
var Utility = require('./utility.js')();
var Diff = require('diff');

//default_fontfamily.json
function applyStyles(themeServiceUrl, rootFolderPath, token, config, style, styleKey) {
    var rawJSON = null;
    var tenantApplied = false;
    var promise = null;
    if (config.tenant != null && fs.existsSync(rootFolderPath + "/" + config.tenant + "/" + style)) {
        promise = fs.promises.readFile(rootFolderPath + "/" + config.tenant + "/" + style, {
            encoding: 'utf8'
        }).then(function (response) {
            rawJSON = response;
        });
        tenantApplied = true;
    } else {
        promise = fs.promises.readFile(rootFolderPath + "/" + style, {
            encoding: 'utf8'
        }).then(function (response) {
            rawJSON = response;
        });
    }

    return promise.then(function () {
        var api = require('./api_module.js')(token);
        return api.post(themeServiceUrl + '/' + styleKey + '/', JSON.parse(rawJSON)).then(function (result) {
            if (result.statusCode == 200) {
                if (tenantApplied == true) {
                    Utility.logSuccess("[Done] Applied tenant " + styleKey);
                } else {
                    Utility.logSuccess("[Done] Applied default " + styleKey);
                }
            } else {
                if (tenantApplied == true) {
                    Utility.logError("[Error] Failed to apply tenant " + styleKey);
                } else {
                    Utility.logError("[Error] Failed to apply default " + styleKey);
                }
                Utility.logError(rawJSON);
                Utility.logError(JSON.stringify(result.raw_body));
            }
        });
    });
}

function applyDefaultFontFamily(themeServiceUrl, rootFolderPath, token, config) {
    return applyStyles(themeServiceUrl, rootFolderPath, token, config, "default_fontfamily.json","masterfontfamily").then(function(res){
        return applyStyles(themeServiceUrl, rootFolderPath, token, config, "default_fontfamily.json", "tenantfontfamily");
    });
    
}

function applyDefaultLogo(themeServiceUrl, rootFolderPath, token, config) {
    return applyStyles(themeServiceUrl, rootFolderPath, token, config, "default_logo.json", "logo");
}

function applyDefaultAsset(themeServiceUrl, rootFolderPath, token, config) {
    return applyStyles(themeServiceUrl, rootFolderPath, token, config, "default_asset.json", "asset");
}

function applyDefaultColorPallet(themeServiceUrl, rootFolderPath, token, config) {
    var tenantApplied = false;
    var defaultName = "Frost";
    var defaultColorPallet;
    var promise = fs.promises.readFile(rootFolderPath + "/default_color_pallet.json", {
        encoding: 'utf8'
    }).then(function (defaultColorPalletRaw) {
        defaultColorPallet = JSON.parse(defaultColorPalletRaw);
        if (config.tenant != null) {
            return fs.promises.readFile(rootFolderPath + "/" + config.tenant + "/default_color_pallet.json", {
                encoding: 'utf8'
            }).then(function (data) {
                var defaultColorPalletForTenant = JSON.parse(data);
                defaultColorPallet.push(defaultColorPalletForTenant[0]);
                defaultName = defaultColorPalletForTenant[0].name;
                tenantApplied = true;
            }).catch(function (error) {
                console.dir(error);
            });
        }
    });

    var api = require('./api_module.js')(token);

    return promise.then(function () {
        return Utility.processSync(defaultColorPallet, function (r) {
            return api.post(themeServiceUrl + '/colorpallet/', r).then(function (result) {
                if (result.statusCode == 200) {
                    if (tenantApplied == true) {
                        Utility.logSuccess("[Done] Added tenant color pallet " + r.name);
                    } else {
                        Utility.logSuccess("[Done] Added default color pallet" + r.name);
                    }
                } else {
                    if (tenantApplied == true) {
                        Utility.logError("[Error] Failed to add tenant color pallet" + r.name);
                    } else {
                        Utility.logError("[Error] Failed to add default color pallet" + r.name);
                    }
                    Utility.logError(JSON.stringify(r));
                    Utility.logError(JSON.stringify(result.raw_body));
                }
            });
        });
    }).then(function () {
        return api.put(themeServiceUrl + '/colorpallet/apply/' + defaultName).then(function (result) {
            if (result.statusCode == 200) {
                if (tenantApplied == true) {
                    Utility.logSuccess("[Done] Applied tenant color pallet");
                } else {
                    Utility.logSuccess("[Done] Applied default color pallet");
                }
            } else {
                if (tenantApplied == true) {
                    Utility.logError("[Error] Failed to apply tenant color pallet");
                } else {
                    Utility.logError("[Error] Failed to apply default color pallet");
                }

                Utility.logError(defaultName);
                Utility.logError(JSON.stringify(result.raw_body));
            }
        });
    });
}

function applyDefaultIconStyle(themeServiceUrl, rootFolderPath, token, config) {
    var defaultColorPalletRaw = null;
    var tenantApplied = false;
    var promise = null;
    if (fs.existsSync(rootFolderPath + "/" + config.tenant + "/default_iconstyle.json")) {
        tenantApplied = true;
        promise = fs.promises.readFile(rootFolderPath + "/" + config.tenant + "/default_iconstyle.json", {
            encoding: 'utf8'
        }).then(function (data) {
            defaultColorPalletRaw = data;
        });
    } else {
        promise = fs.promises.readFile(rootFolderPath + "/default_iconstyle.json", {
            encoding: 'utf8'
        }).then(function (data) {
            defaultColorPalletRaw = data;
        });
    }

    return promise.then(function () {
        var defaultColorPallet = JSON.parse(defaultColorPalletRaw);
        var api = require('./api_module.js')(token);
        return api.post(themeServiceUrl + '/iconstyle/', defaultColorPallet).then(function (result) {
            if (result.statusCode == 200) {
                if (tenantApplied == true) {
                    Utility.logSuccess("[Done] Applied tenant icon style");
                } else {
                    Utility.logSuccess("[Done] Applied default icon style");
                }
            } else {
                if (tenantApplied == true) {
                    Utility.logError("[Error] Failed to apply tenant icon style");
                } else {
                    Utility.logError("[Error] Failed to apply default icon style");
                }
                Utility.logError(JSON.stringify(defaultColorPallet));
                Utility.logError(JSON.stringify(result.raw_body));
            }
        });
    });
}

function generateCSS(themeServiceUrl, token) {
    var api = require('./api_module.js')(token);
    return api.post(themeServiceUrl + '/theme/generate/css/').then(function (result) {
        if (result.statusCode == 200) {
            Utility.logSuccess("[Done] Generated CSS");
            return true;
        } else {
            Utility.logError("[Error] Failed to Generate CSS");
            Utility.logError(result.statusCode + " : " + result.raw_body + " : URL:" + themeServiceUrl + '/theme/generate/css/' + token);
            Utility.logError(JSON.stringify(result.raw_body));
            return false;
        }
    });
}

function initializeTheme(assetServiceUrl, themeServiceUrl, currentFolder, config, token) {
    var filesToUpload = [{
            defaultFolder: 'default_less',
            serverFolder: 'less',
            entity: "theme"
        },
        {
            defaultFolder: 'default_images',
            serverFolder: 'css',
            entity: "theme"
        },
        {
            defaultFolder: 'default_js',
            serverFolder: 'js',
            entity: "theme"
        },
        {
            defaultFolder: 'default_icons',
            serverFolder: 'icons',
            entity: "icons"
        }
    ];
    return Utility.processSync(filesToUpload, (r) => {
        return uploadFiles(assetServiceUrl, currentFolder, token, config, r.defaultFolder, r.serverFolder, r.entity);
    }).then(function () {
        return syncFontsWithGoogle(themeServiceUrl, token).then(function () {
            return applyDefaultFontFamily(themeServiceUrl, currentFolder, token, config).then(function () {
                return applyDefaultLogo(themeServiceUrl, currentFolder, token, config).then(function () {
                    return applyDefaultAsset(themeServiceUrl, currentFolder, token, config).then(function () {
                        return applyDefaultColorPallet(themeServiceUrl, currentFolder, token, config).then(function () {
                            return applyDefaultIconStyle(themeServiceUrl, currentFolder, token, config).then(function () {
                                return generateCSS(themeServiceUrl, token);
                            });
                        });
                    });
                });
            });
        });
    });
}

function uploadFileToAsset(serviceUrl, imageFile, token, entityType, entityId) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(imageFile);
    return api.uploadFile("put", serviceUrl + '/' + entityType + '/' + entityId + '/' + pathObject.base, imageFile + "/").then(function (result) {
        if (result.statusCode != 200) {
            return api.uploadFile("post", serviceUrl + '/' + entityType + '/' + entityId, imageFile).then(function (result) {
                if (result.statusCode == 200) {
                    Utility.logSuccess("[Done] Publishing file: " + imageFile);
                    return true;
                } else {
                    Utility.logError("[Error] Failed to publish file: " + imageFile + " Service URL: " + serviceUrl + '/' + entityType + '/' + entityId + '/' + pathObject.base);
                    Utility.logError(result.statusCode + " : " + result.raw_body);
                    return JSON.stringify(result.raw_body);
                }
            });
        } else {
            Utility.logSuccess("[Done] Publishing file: " + imageFile);
            return true;
        }
    });
}

function getLocalFiles(currentFolder, config, defaultFolderPath, tenantFolderName) {
    var files = Utility.walkSync(currentFolder + "/" + defaultFolderPath);
    if (config.tenant != null && fs.existsSync(currentFolder + "/" + config.tenant + "/theme/" + tenantFolderName)) {
        var tenantFiles = Utility.walkSync(currentFolder + "/" + config.tenant + "/theme/" + tenantFolderName);
        if (tenantFiles && tenantFiles.length > 0) {
            files = files.concat(tenantFiles);
        }
    }
    return files;
}

function uploadFiles(assetServiceUrl, currentFolder, token, config, defaultFolder, serverFolder, entity) {
    var files = getLocalFiles(currentFolder, config, defaultFolder, serverFolder);
    return Utility.processSync(files, (r) => {
        if (entity == "icons") {
            serverFolder = r.indexOf("/" + config.tenant + "/") != -1 ? config.tenant + "-icons" : "default";
        }
        return uploadFileToAsset(assetServiceUrl, r, token, entity, serverFolder);
    });
}

function syncFontsWithGoogle(themeServiceUrl, token) {
    var api = require('./api_module.js')(token);
    return api.post(themeServiceUrl + '/masterfontfamily/import/googlefonts/').then(function (result) {
        if (result.statusCode == 200) {
            Utility.logSuccess("[Done] Imported fonts");
        } else {
            Utility.logError("[Error] Failed to Import fonts");
            Utility.logError(result.statusCode + " : " + result.raw_body);
        }
    });
}

function diffFiles(assetServiceUrl, currentFolder, defaultFolder, tenantFolderName, serverFolder, entity, config, token) {
    var api = require('./api_module.js')(token);
    var files = getLocalFiles(currentFolder, config, defaultFolder, tenantFolderName);
    var localFiles = {};
    var promises = [];
    var fileObject = files.map(a => {
        var pathObject = path.parse(a);
        return {
            name: pathObject.base,
            file: a
        };
    });

    fileObject.forEach(file => {
        promises.push(fs.promises.readFile(file.file, {
            encoding: 'utf8'
        }).then(function (data) {
            localFiles[file.name] = {
                data: data,
                file: file
            };
        }));
    });
    var configurationServiceUrl = config.baseUrl + config.configurationConfiguration.servicePort;
    if (process.env.CONFIGURATION_HOST) {
        configurationServiceUrl = process.env.CONFIGURATION_HOST;
    }
    return api.get(configurationServiceUrl + "/theme").then(function (themeConfig) {
        themeConfig=themeConfig.body;
        Utility.logSuccess("Asset Base URL:"+themeConfig.AssetBaseURL);
        Utility.logSuccess("Asset Base Internal URL:"+themeConfig.AssetInternalBaseURL);

        return Promise.all(promises).then(function () {
            return api.get(assetServiceUrl + "/" + entity + "/" + serverFolder + "/").then(function (result) {
                var promises = [];
                var serverFiles = {};
                result.body.forEach(file => {
                    var url=file.url;
                    if(process.env.UseInternalUrl=="true"){
                        Utility.logSuccess("Before URL:"+file.url);
                        url = url.replace(themeConfig.AssetBaseURL,themeConfig.AssetInternalBaseURL);
                        Utility.logSuccess("After URL:"+file.url);
                    }
                    promises.push(api.getWithoutToken(url).then(function (result) {
                        serverFiles[file.fileName] = result.body;
                    }));
                });
                return Promise.all(promises).then(function () {
                    var output = {};

                    output.newFiles = [];
                    output.changes = [];
                    output.extraFiles = [];
                    output.errors = {};
                    output.isSuccess = true;

                    for (var key in localFiles) {
                        if (serverFiles[key] != null) {
                            if (serverFiles[key] != localFiles[key].data) {
                                var diff = Diff.diffLines(serverFiles[key], localFiles[key].data);
                                output.changes.push({
                                    key: key,
                                    value: diff,
                                    isJSONDiff: false,
                                    file: localFiles[key].file
                                });
                            }
                        } else {
                            output.newFiles.push({
                                key: key,
                                value: localFiles[key].data,
                                file: localFiles[key].file
                            });
                        }
                    }

                    for (key in serverFiles) {
                        if (localFiles[key] == null) {
                            output.extraFiles.push({
                                key: key,
                                value: serverFiles[key]
                            });
                        }
                    }

                    return output;
                });
            });
        });
    });


}

module.exports = function (currentFolder, config) {
    return {
        assetServiceUrl: function () {
            var assetServiceUrl = config.baseUrl + config.assetManagerConfiguration.servicePort;
            if (process.env.ASSET_HOST) {
                assetServiceUrl = process.env.ASSET_HOST;
            }
            return assetServiceUrl;
        },
        themeServiceUrl: function () {
            var themeServiceUrl = config.baseUrl + config.themeConfiguration.servicePort;
            if (process.env.THEME_HOST) {
                themeServiceUrl = process.env.THEME_HOST;
            }
            return themeServiceUrl;
        },
        initializeTheme: function () {
            return initializeTheme(this.assetServiceUrl(), this.themeServiceUrl(), currentFolder, config, config.authorizationToken);
        },
        syncFontsWithGoogle: function () {
            return syncFontsWithGoogle(this.themeServiceUrl(), config.authorizationToken);
        },
        diffLessFiles: function () {
            return diffFiles(this.assetServiceUrl(), currentFolder, "default_less", "less", "less", "theme", config, config.authorizationToken);
        },
        publishLessFileChange: function () {
            var self = this;
            return this.diffLessFiles(this.assetServiceUrl(), this.themeServiceUrl(), currentFolder, config).then(function (response) {
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                var anyFailed = false;
                return Utility.processSync(files, (r) => {
                    return uploadFileToAsset(self.assetServiceUrl(), r.file, config.authorizationToken, "theme", "less").then(function (result) {
                        if (result == false) {
                            anyFailed = true;
                        }
                    });
                }).then(function () {
                    Utility.logSuccess("Publish Less file Changes completed");
                    Utility.logSuccess("Generating CSS");
                    if (anyFailed == true) {
                        throw new Error("Publish theme failed");
                    }
                    return generateCSS(self.themeServiceUrl(), config.authorizationToken).then(function (result) {
                        if (result == false) {
                            throw new Error("Unable to generate CSS");
                        }
                        Utility.logSuccess("CSS Generated");
                    });

                });
            });
        }
    };
};