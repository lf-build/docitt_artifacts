'use strict';

var path = require('path');
var Diff = require('diff');
var Utility = require('./utility.js')();
var fs = fs || require('fs');

function publishConfiguration(tenants, tenant, serviceUrl, configFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(configFile);
    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, ".json");
        if (result.isSuccess) {
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
            return processConfiguration(configFile, configurationKey, tenant, config).then(function (result) {
                if (result.isSuccess) {
                    return api.post(serviceUrl + '/' + configurationKey, result.data).then(function (result) {
                        if (result.statusCode == 200) {
                            Utility.logSuccess("[Done] Publishing configuration: " + configurationKey);
                            return true;
                        } else {
                            Utility.logError(JSON.stringify(result));
                            Utility.logError("[Error] Failed to publish configuration: " + configurationKey);
                            Utility.logError(result.raw_body);
                            return JSON.stringify(result.raw_body);
                        }
                    });
                }
            });

        }
    } else {
        Utility.logError('[Warning] Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
    return Promise.resolve(false);
}

function publishAllConfiguration(tenants, tenant, serviceUrl, folderPath, config, token) {
    var api = require('./api_module.js')(token);
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".json", processConfiguration).then(function (response) {
        var configurations = response.configurations;
        var configToProcess = [];
        for (var key in configurations) {
            configToProcess.push({
                key: key,
                value: configurations[key]
            });
        }

        return Utility.processSync(configToProcess, (r) => {
            return api.post(serviceUrl + '/' + r.key, r.value.data).then(function (result) {
                var pathObject = path.parse(r.value.file);
                if (result.statusCode == 200) {
                    Utility.logSuccess("[Done] Publishing configuration: " + pathObject.name + " as " + r.key);
                } else {
                    Utility.logError(JSON.stringify(result));
                    Utility.logError("[Error] Failed to publish configuration: " + pathObject.name);
                    Utility.logError(result.raw_body);
                }
            });
        }).then(function () {
            Utility.logSuccess("Publish All Configuration completed");
        });
    });


}

function getConfigurationsDiff(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getArtifactsDiff(tenants, tenant, serviceUrl, folderPath, config, token, ".json", processOldResponse, processConfiguration, compareConfiguration);
}

function processOldResponse(result) {
    var oldConfigurations = {};
    for (var key in result) {
        var configValue = JSON.stringify(result[key], null, 1);
        oldConfigurations[key] = configValue;
    }
    return Promise.resolve(oldConfigurations);
}

function processConfiguration(configFile, configurationKey, tenant, config) {
    return fs.promises.readFile(configFile, {
        encoding: "utf8"
    }).then(function (data) {
        data = Utility.replaceStringForJSON(config, data,configurationKey);
        try {
            data = JSON.stringify(JSON.parse(data), null, 1);
            return {
                configurationKey: configurationKey,
                data: data,
                isSuccess: true,
                file: configFile
            };
        } catch (error) {
            var message = '[Error] Skipping file ' + configurationKey + ' as JSON parse failed ' + tenant;
            Utility.logError(message);
            return {
                configurationKey: configurationKey,
                message: message,
                error: error,
                isSuccess: false
            };
        }
    });

}

function compareConfiguration(key, oldConfigurations, newConfigurations, output) {
    if (oldConfigurations[key] != newConfigurations[key].data) {
        //var diff=jsonDiff.diffString(JSON.parse(oldConfigurations[key]),JSON.parse(newConfigurations[key]));
        var diff = Diff.diffLines(oldConfigurations[key] || "", newConfigurations[key].data || "");
        // diff.forEach(function(part){
        //     // green for additions, red for deletions
        //     // grey for common parts
        //     var color = part.added ? 'green' :
        //       part.removed ? 'red' : 'grey';
        //     process.stderr.write(part.value[color]);
        //   });

        output.changes.push({
            key: key,
            value: diff,
            isJSONDiff: false,
            file: newConfigurations[key].file
        });
    }
}

module.exports = function (folderPath, config) {
    return {
        serviceUrl: function(){
            var configurationServiceUrl = config.baseUrl + config.configurationConfiguration.servicePort;
            if (process.env.CONFIGURATION_HOST) {
                configurationServiceUrl = process.env.CONFIGURATION_HOST;
            }
            return configurationServiceUrl;
        },
        publish: function (configFile) {
            return publishConfiguration(config.tenants, config.tenant, this.serviceUrl(), configFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllConfiguration(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self=this;
            return getConfigurationsDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                var anyFailed = false;
                var messages="";
                return Utility.processSync(files, (r) => {
                    return publishConfiguration(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function (result) {
                        if (result !== true) {
                            anyFailed = true;
                            messages+=result;
                        }
                    });
                }).then(function () {
                    if (anyFailed == true) {
                        throw new Error("Publish configuration failed " + messages);
                    }
                    Utility.logSuccess("Publish Configuration Changes completed");
                });
            });
        },
        GetAllDiff: function () {
            return getConfigurationsDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        }
    };
};