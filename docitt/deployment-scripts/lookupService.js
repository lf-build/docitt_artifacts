'use strict';
var path = require('path');
var fs = require('fs');
var Utility = require('./utility.js')();
var fs = fs || require('fs');
var Diff = require('diff');

function publishLookup(tenants, tenant, serviceUrl, configFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(configFile);
    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, ".json");
        if (result.isSuccess) {
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
            return processConfiguration(configFile, configurationKey, tenant, config).then(function (result) {
                if (result.isSuccess) {
                    return api.delete(serviceUrl + '/' + configurationKey).then(function () {
                        return api.post(serviceUrl + '/' + configurationKey, result.data).then(function (result) {
                            if (result.statusCode == 200 || result.statusCode == 204) {
                                Utility.logSuccess("[Done] Publishing lookup: " + configurationKey);
                                return true;
                            } else {
                                Utility.logError("[Error] Failed to publish lookup: " + configurationKey);
                                Utility.logError(JSON.stringify(result));
                                Utility.logError(result.raw_body);
                                return false;
                            }
                        });
                    });
                }
            });
        }
    } else {
        Utility.logError('[Warning] Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
    return Promise.resolve(true);
}

function publishAllLookup(tenants, tenant, serviceUrl, folderPath, config, token) {
    var api = require('./api_module.js')(token);
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".json", processConfiguration).then(function (response) {
        var configurations = response.configurations;
        var configToProcess = [];
        for (var key in configurations) {
            configToProcess.push({
                key: key,
                value: configurations[key]
            });
        }

        return Utility.processSync(configToProcess, (r) => {
            return api.delete(serviceUrl + '/' + r.key).then(function () {
                return api.post(serviceUrl + '/' + r.key, r.value.data).then(function (result) {
                    var pathObject = path.parse(r.value.file);
                    if (result.statusCode == 200 || result.statusCode == 204) {
                        Utility.logSuccess("[Done] Publishing lookup: " + pathObject.name);
                    } else {
                        Utility.logError("[Error] Failed to publish lookup: " + pathObject.name);
                        Utility.logError(JSON.stringify(result));
                        Utility.logError(result.raw_body);
                    }
                });
            });
        }).then(function () {
            Utility.logSuccess("Publish All Configuration completed");
        });
    });
}

function getLookupDiff(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getArtifactsDiff(tenants, tenant, serviceUrl + "/entities", folderPath, config, token, ".json", processOldResponse, processConfiguration, compareConfiguration);
}

function processOldResponse(result, token, serviceUrl) {
    var oldConfigurations = {};
    var promises = [];
    var ulr = serviceUrl.replace("/entities", "");
    result.forEach(function (entityType) {
        var api = require('./api_module.js')(token);
        promises.push(api.get(ulr + "/" + entityType).then(function (response) {
            if (response.statusCode == 200) {
                return {
                    key: entityType,
                    data: JSON.stringify(response.body, null, 1)
                };
            } else {
                Utility.logError("[Error] Failed to publish lookup: " + entityType);
                Utility.logError(JSON.stringify(result));
                Utility.logError(response.raw_body);
            }
        }));
    });

    return Promise.all(promises).then(function (data) {
        for (var key in data) {
            oldConfigurations[data[key].key] = data[key].data;
        }
        return oldConfigurations;
    });
}

function processConfiguration(configFile, configurationKey, tenant, config) {
    return fs.promises.readFile(configFile, {
        encoding: "utf8"
    }).then(function (data) {
        data = Utility.replaceStringForJSON(config, data,configurationKey);
        try {
            var o = JSON.parse(data);
            o = Utility.toCamel(o);
            data = JSON.stringify(o, null, 1);
            return {
                configurationKey: configurationKey.toLowerCase(),
                data: data,
                isSuccess: true,
                file: configFile
            };
        } catch (error) {
            var message = '[Error] Skipping file ' + configurationKey + ' as JSON parse failed ' + tenant;
            Utility.logError(message);
            return {
                configurationKey: configurationKey.toLowerCase(),
                message: message,
                error: error,
                isSuccess: false
            };
        }
    });
}

function compareConfiguration(key, oldConfigurations, newConfigurations, output) {
    if (oldConfigurations[key] != newConfigurations[key].data) {
        //var diff=jsonDiff.diffString(JSON.parse(oldConfigurations[key]),JSON.parse(newConfigurations[key]));
        var diff = Diff.diffLines(oldConfigurations[key], newConfigurations[key].data);
        output.changes.push({
            key: key,
            value: diff,
            isJSONDiff: false,
            file: newConfigurations[key].file
        });
    }
}

module.exports = function (folderPath, config) {
    return {
        serviceUrl: function () {
            var lookupServiceUrl = config.baseUrl + config.lookupServiceConfiguration.servicePort;
            if (process.env.LOOKUP_HOST) {
                lookupServiceUrl = process.env.LOOKUP_HOST;
            }
            return lookupServiceUrl;
        },
        publish: function (configFile) {
            return publishLookup(config.tenants, config.tenant, this.serviceUrl(), configFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllLookup(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self=this;
            return getLookupDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var anyFailed = false;
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                return Utility.processSync(files, (r) => {
                    return publishLookup(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function (result) {
                        if (result == false) {
                            anyFailed = true;
                        }
                    });
                }).then(function () {
                    if (anyFailed == true) {
                        throw new Error("Publish Lookup failed");
                    }
                    Utility.logSuccess("Publish Lookup Changes completed");
                });
            });
        },
        GetAllDiff: function () {
            return getLookupDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        }
    };
};