'use strict';
var express = require('express');
var exphbs = require('express-handlebars');
var serviceFactory = require('./serviceFactory.js')();
var port = 3000;
var app = express();
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
app.set('etag', false);
app.use((req, res, next) => {
    res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private')
    next();
});
var APIBaseUrl = process.env.SERVICE_URL;
var SERVER = process.env.SERVER;

console.log('ENVIRONMENT VARIABLES');
console.log('SERVER:' + SERVER);
console.log('SERVICE_URL:' + APIBaseUrl);
console.log('TENANT_HOST:' + process.env.TENANT_HOST);
console.log('CONFIGURATION_HOST:' + process.env.CONFIGURATION_HOST);
console.log('DECISION_ENGINE_HOST:' + process.env.DECISION_ENGINE_HOST);
console.log('WEBHOOK_HOST:' + process.env.WEBHOOK_HOST);
console.log('LOOKUP_HOST:' + process.env.LOOKUP_HOST);
console.log('THEME_HOST:' + process.env.THEME_HOST);
console.log('ASSET_HOST:' + process.env.ASSET_HOST);
console.log('QUESTIONNAIRE_HOST:' + process.env.QUESTIONNAIRE_HOST);
console.log('TEMPLATE_HOST:' + process.env.TEMPLATE_HOST);

var initializeEnv = function () {
    return serviceFactory.getEnvironment(SERVER + ".env.json");
};

var initialize = function (tenant) {
    var configFile = process.env.SERVER + '-' + tenant + '.json';
    return serviceFactory.getConfiguration(configFile);
};

var getHelper = function () {
    return {
        ifNotEmpty: function (v1, options) {
            var t = v1;
            if (t != null) {
                t = t.trim();
            }
            if (t.length != 0) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    };
};

function getService(tenant, name) {
    return initialize(tenant).then(function (config) {
        return serviceFactory.getService(name, config, serviceFactory.folders[name]);
    });
}

app.get('/ping', (req, res) => {
    res.status(200).send("true");
});

app.get('/diff/themeCSS/:tenant/raw', (req, res) => {
    return getService(req.params.tenant, "theme").then(function (service) {
        return service.diffLessFiles().then(function (response) {
            res.send(response);
        });
    }).catch(function (error) {
        res.status(500).send({
            isSuccess: false,
            errors: {
                status: error.status || 500,
                message: error.message || 'Internal Server Error for ' + req.params.tenant + ' ' + "theme",
                error: error
            }
        });
    });
});

app.post('/sync/theme/:tenant/:google-fonts', (req, res) => {
    return getService(req.params.tenant, "theme").then(function (service) {
        return service.syncFontsWithGoogle().then(function () {
            res.send('true');
        });
    }).catch(function (error) {
        res.status(500).send({
            isSuccess: false,
            errors: {
                status: error.status || 500,
                message: error.message || 'Internal Server Error for ' + req.params.tenant + ' ' + "theme",
                error: error
            }
        });
    });
});

app.post('/sync/themeCSS/:tenant/publish-changes', (req, res) => {
    return getService(req.params.tenant, "theme").then(function (service) {
        return service.publishLessFileChange().then(function (response) {
            res.send('true');
        });
    }).catch(function (error) {
        res.status(500).send({
            isSuccess: false,
            errors: {
                status: error.status || 500,
                message: error.message || 'Internal Server Error for ' + req.params.tenant + ' ' + "theme",
                error: error
            }
        });
    });
});

app.get('/diff/:name/:tenant/raw', (req, res) => {
    try {
        return getService(req.params.tenant, req.params.name).then(function (service) {
            return service.GetAllDiff().then(function (response) {
                res.send(response);
            });
        }).catch(function (error) {
            console.dir(error);
            res.status(500).send({
                isSuccess: false,
                errors: {
                    status: error.status || 500,
                    message: error.message || 'Internal Server Error for ' + req.params.tenant + ' ' + req.params.name,
                    error: error
                }
            });
        });
    } catch (error) {
        console.dir(error);
        res.status(500).send({
            isSuccess: false,
            errors: {
                message: "Unable to get difference for " + req.params.name,
                error: error
            }
        });
    }
});

app.post('/sync/:name/:tenant/publish-changes', (req, res) => {
    try {
        return getService(req.params.tenant, req.params.name).then(function (service) {
            return service.publishChanges().then(function () {
                res.send('true');
            });
        }).catch(function (error) {
            res.status(500).send({
                isSuccess: false,
                errors: {
                    status: error.status || 500,
                    message: error.message || 'Internal Server Error for ' + req.params.tenant + ' ' + req.params.name,
                    error: error
                }
            });
        });
    } catch (error) {
        console.dir(error);
        res.status(500).send({
            isSuccess: false,
            errors: {
                message: error.message || 'Internal Server Error for ' + req.params.tenant + ' ' + req.params.name,
                error: error
            }
        });
    }
});



app.get('/', (req, res) => {
    return initializeEnv().then(function (env) {
        res.render('home', {
            APIBaseUrl: APIBaseUrl,
            data: env,
            helpers: getHelper()
        });
    });
});

app.get('/tenants', (req, res) => {
    return initializeEnv().then(function (env) {
        res.send(env.tenants);
    });
});

app.use((error, req, res, next) => {
    res.status(error.status || 500).send({
        isSuccess: false,
        errors: {
            status: error.status || 500,
            message: error.message || 'Internal Server Error',
            error: error
        }
    });
});

app.listen(port, () => console.log(`Artifacts app listening on port ${port}!`));
