'use strict';
var matchStrings = ['this.call(\'', 'this.service(\'', 'self.call(\'', 'self.service(\'', 'this.call("', 'this.service("', 'self.call("', 'self.service("'];
var path = require('path');
var fs = require('fs');
var UglifyJS = require("uglify-js");
var beautify = require('js-beautify').js;
var Diff = require('diff');
var Utility = require('./utility.js')();
var babel = require("@babel/core");

function getDependencies(str, matchString) {
    var all = [];
    var firstMatch = str.split(matchString);
    if (firstMatch[1] != undefined) {
        var lastIndex = firstMatch[1].indexOf('\'');
        if (matchString.indexOf('"') >= 0) {
            lastIndex = firstMatch[1].indexOf('"');
        }
        var pushIt = str.split(matchString)[1].substring(0, lastIndex).replace(/'/g, '');
        all.push(pushIt);
        var newStr = str.substring(firstMatch[0].length + lastIndex, str.length);
        if (firstMatch.length > 2 && newStr.indexOf(matchString) >= 0) {
            var newMatch = getDependencies(newStr, matchString);
            if (newMatch != null) {
                for (var p = 0; p < newMatch.length; p++) {
                    all.push(newMatch[p]);
                }
            }
        }
        return all;
    }
    return null;
}

function getRuleObject(file) {
    return fs.promises.readFile(file, {
        encoding: 'utf8'
    }).then(function (data) {
        var minimizationFailed = false;
        var minimizationFailedMessage = "";

        var result = UglifyJS.minify(data, {
            compress: false,
            mangle: false
        });

        if (!result.error) {
            data = result.code;
        } else {
            minimizationFailed = true;
            Utility.logError("Minimization failed with error " + file);
            Utility.logError(JSON.stringify(result.error));
            minimizationFailedMessage = result.error.message;
        }

        var dependencies = [];
        for (var j = 0; j < matchStrings.length; j++) {
            var childDependency = getDependencies(data, matchStrings[j]);
            if (childDependency != null) {
                for (var k = 0; k < childDependency.length; k++) {
                    if (dependencies.indexOf(childDependency[k]) == -1) {
                        dependencies.push(childDependency[k]);
                    }
                }
            }
        }
        return {
            source: data,
            dependencies: dependencies,
            minimizationFailed: minimizationFailed,
            minimizationFailedMessage: minimizationFailedMessage
        };
    });
}

function publishRule(tenants, tenant, serviceUrl, ruleFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(ruleFile);
    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == ".js") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, ".js");
        if (result.isSuccess) {
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
            return processConfiguration(ruleFile, configurationKey, tenant, config).then(function (ruleResult) {
                if (ruleResult.isSuccess) {
                    return api.put(serviceUrl, ruleResult.rule.data).then(function (result) {
                        if (result.statusCode == 200) {
                            Utility.logSuccess("[Done] Published rule: " + ruleFile);
                            return Promise.resolve(true);
                        } else {
                            Utility.logError("[Error] Failed to publish rule: " + configurationKey);
                            Utility.logError(JSON.stringify(ruleResult.rule.data));
                            Utility.logError(result.raw_body);
                            return Promise.resolve(false);
                        }
                    });
                }
            });
        }
    } else {
        if(pathObject.name!==".jshintrc"){
            console.warn('[Warning] Skipping file ' + ruleFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
        }
    }
    return Promise.resolve(false);
}

function publishAllRule(tenants, tenant, serviceUrl, folderPath, config, token) {
    var api = require('./api_module.js')(token);
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".js", processConfiguration).then(function (response) {
        var rules = response.configurations;
        var rulesToProcess = [];
        for (var key in rules) {
            rulesToProcess.push({
                key: key,
                value: rules[key]
            });
        }

        rulesToProcess = rulesToProcess.sort(function (a, b) {
            return a.value.rule.data.dependencies.length - b.value.rule.data.dependencies.length;
        });

        return Utility.processSync(rulesToProcess, (r) => {
            return api.put(serviceUrl, r.value.rule.data).then(function (result) {
                var pathObject = path.parse(r.value.file);
                if (result.statusCode == 200) {
                    Utility.logSuccess("[Done] Published rule: " + pathObject.name);
                } else {
                    Utility.logError("[Error] Failed to publish rule: " + pathObject.name);
                    Utility.logError(JSON.stringify(r.value.rule.data));
                    Utility.logError(result.raw_body);
                }
            });
        }).then(function () {
            Utility.logSuccess("Publish All Rules completed");
        });
    });
}

function getRuleDiff(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getArtifactsDiff(tenants, tenant, serviceUrl, folderPath, config, token, ".js", processOldResponse, processConfiguration, compareConfiguration);
}

function processOldResponse(result) {
    var oldRules = {};
    result.forEach(item => {
        if (item != null && item.source != null) {
            var source = item.source.replace(/\\"/g, '"');
            source = source.replace(/\\n/g, "\n");
            source = source.substr(1, source.length - 2);
            oldRules[item.name] = beautify(source, {
                indent_size: 2,
                space_in_empty_paren: true
            });
        }
    });
    return Promise.resolve(oldRules);
}

function processConfiguration(configFile, configurationKey, tenant, config) {
    return getRuleObject(configFile).then(function (ruleResult) {
        var minimizationFailed = ruleResult.minimizationFailed;
        var minimizationFailedMessage = ruleResult.minimizationFailedMessage;
        delete ruleResult.minimizationFailed;
        delete ruleResult.minimizationFailedMessage;
        var rule = {
            element: configurationKey,
            data: ruleResult
        };

        var fnName = rule.data.source.substr(0, rule.data.source.indexOf("("));
        fnName = fnName.replace('function', '').trim();
        rule.data.source = Utility.replaceStringForJSON(config, rule.data.source,configurationKey);

        return {
            configurationKey: fnName,
            data: beautify(rule.data.source, {
                indent_size: 2,
                space_in_empty_paren: true
            }),
            rule: rule,
            isSuccess: true,
            minimizationFailed: minimizationFailed,
            minimizationFailedMessage: minimizationFailedMessage,
            file: configFile
        };
    });
}

function compareConfiguration(key, oldRules, newRules, output) {
    if (oldRules[key] != newRules[key].data) {
        var diff = Diff.diffLines(oldRules[key], newRules[key].data);
        // diff.forEach(function (part) {
        //     // green for additions, red for deletions
        //     // grey for common parts
        //     var color = part.added ? 'green' :
        //         part.removed ? 'red' : 'grey';
        //     process.stderr.write(part.value[color]);
        // });
        output.changes.push({
            key: key,
            value: diff,
            isJSONDiff: false,
            file: newRules[key].file
        });
    }
}

module.exports = function (folderPath, config) {
    return {
        serviceUrl: function () {
            var ruleServiceUrl = config.baseUrl + config.ruleConfiguration.servicePort;
            if (process.env.DECISION_ENGINE_HOST) {
                ruleServiceUrl = process.env.DECISION_ENGINE_HOST;
            }
            return ruleServiceUrl;
        },
        publish: function (ruleFile) {
            return publishRule(config.tenants, config.tenant, this.serviceUrl(), ruleFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllRule(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self=this;
            return getRuleDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var anyFailed = false;
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                return Utility.processSync(files, (r) => {
                    return publishRule(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function (result) {
                        if (result == false) {
                            anyFailed = true;
                        }
                    });
                }).then(function () {
                    if (anyFailed == true) {
                        throw new Error("Publish rule failed");
                    }
                    Utility.logSuccess("Publish Rule Changes completed");
                });
            });
        },
        GetAllDiff: function () {
            return getRuleDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        convertToES5: function () {
            return Utility.getAllLocalFiles(folderPath, config.tenants, config.tenant, config, ".js", processConfiguration).then(function (rules) {
                var files = [];
                for (var key in rules) {
                    files.push(rules[key].file);
                }
                return Utility.processSync(files, (r) => {
                    babel.transformFileAsync(r).then(result => {
                        fs.writeFileSync(r, result.code);
                    });
                });

            });
        }
    };
};