'use strict';
var Utility = require('./utility.js')();

function getEnvironment(envFile) {
    var envConfig = require("../" + envFile);
    var tenantServiceUrl = envConfig.baseUrl + envConfig.tenantServiceConfiguration.servicePort;
    if (process.env.TENANT_HOST) {
        tenantServiceUrl = process.env.TENANT_HOST;
    }
    var tenantService = require('../deployment-scripts/tenantService.js')(tenantServiceUrl);
    return tenantService.getTenants().then(function (tenants) {
        return {
            name: envConfig.name,
            tenants: tenants
        };
    });
}

function getConfiguration(tenantConfigFile) {
    var config = require("../" + tenantConfigFile);
    if (config.env) {
        var envConfig = require("../" + config.env + ".json");
        for (var k in envConfig) {
            if (k == "data_replacement") {
                envConfig[k].forEach(data => {
                    config[k].push(data);
                });
            } else if (k == "importDataReplacements") {
                envConfig[k].forEach(element => {
                    config[k].push(element);
                });
            } else {
                config[k] = envConfig[k];
            }
        }
    }
    if (config.importDataReplacements != null) {
        config.importDataReplacements.forEach(configFileName => {
            var tmp = require("../" + configFileName);
            tmp.forEach(item => {
                config.data_replacement.push(item);
            });
        });
    }
    Utility.logInfo("Getting configuration for " + tenantConfigFile);
    var tenantServiceUrl = config.baseUrl + config.tenantServiceConfiguration.servicePort;
    if (process.env.TENANT_HOST) {
        tenantServiceUrl = process.env.TENANT_HOST;
    }
    var tenantService = require('../deployment-scripts/tenantService.js')(tenantServiceUrl);
    return tenantService.getTenants().then(function (tenants) {
        config.tenants = tenants.map(t => t.id);
        Utility.logInfo(JSON.stringify(config.tenants));
        return config;
    });
}

function getService(name, config, rootFolder){
    return require('../deployment-scripts/'+name+'Service.js')(rootFolder, config);
}

module.exports = function () {
    return {
        getEnvironment: function (envFile) {
            return getEnvironment(envFile);
        },
        getConfiguration: function (tenantConfigFile) {
            return getConfiguration(tenantConfigFile);
        },
        getService: function(name, config, rootFolder){
            return getService(name,config,rootFolder);
        },
        folders: {
                "configuration": "configuration",
                "rule": "rules",
                "template": "templates",
                "inboundWebhook": "webhook/inbound",
                "outboundWebhook": "webhook/outbound",
                "lookup": "lookup",
                "theme": "theme",
                "questionnaire": "questionnaire"
        } 
    };
};