'use strict';
var path = require('path');
var Diff = require('diff');
var Utility = require('./utility.js')();
var fs = fs || require('fs');

function publishInboundWebhook(tenants, tenant, serviceUrl, configFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(configFile);
    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, ".json");
        if (result.isSuccess) {
            if(result.configurationKey){
                configurationKey=result.configurationKey;
            }
            return processConfiguration(configFile, configurationKey, tenant, config).then(function(result){
                if (result.isSuccess) {
                    return api.post(serviceUrl + '/inbound', result.data).then(function (result) {
                        if (result.statusCode == 200) {
                            Utility.logSuccess("[Done] Publishing inbound webhook: " + configurationKey);
                            return true;
                        } else {
                            Utility.logError("[Error] Failed to publish inbound webhook: " + configurationKey);
                            Utility.logError(JSON.stringify(result));
                            Utility.logError(result.raw_body);
                            return JSON.stringify(result.raw_body);
                        }
                    });
                }
            });
        }
    } else {
        console.warn('[Warning] Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
    return Promise.resolve(false);
}

function publishAllInboundWebhook(tenants, tenant, serviceUrl, folderPath, config, token) {
    var api = require('./api_module.js')(token);
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".json", processConfiguration).then(function(response){
        var configurations = response.configurations;
        var configToProcess = [];
        for (var key in configurations) {
            configToProcess.push({
                key: key,
                value: configurations[key]
            });
        }
    
        return Utility.processSync(configToProcess, (r) => {
            return api.post(serviceUrl + '/inbound', r.value.data).then(function (result) {
                if (result.statusCode == 200) {
                    Utility.logSuccess("[Done] Publishing inbound webhook: " + r.key);
                } else {
                    Utility.logError("[Error] Failed to publish inbound webhook: " + r.key);
                    Utility.logError(JSON.stringify(result));
                    Utility.logError(result.raw_body);
                }
            });
        }).then(function(){
            Utility.logSuccess("Publish All inbound webhook completed");
        });
    });
}

function getInboundDiff(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getArtifactsDiff(tenants, tenant, serviceUrl + "/inbound", folderPath, config, token, ".json", processOldResponse, processConfiguration, compareConfiguration);
}

function processOldResponse(result) {
    var oldWebHooks = {};
    for (var key in result) {
        var item = result[key];
        delete item._id;
        delete item.tenantId;
        var template = JSON.stringify(item, null, 1);
        oldWebHooks[item.name] = template;
    }
    return Promise.resolve(oldWebHooks);
}

function processConfiguration(configFile, configurationKey, tenant, config) {
    return fs.promises.readFile(configFile, {
        encoding: "utf8"
    }).then(function (data) {
        data = Utility.replaceStringForJSON(config, data,configurationKey);
        try {
            var obj = JSON.parse(data);
            data = JSON.stringify(obj, null, 1);
            return {
                configurationKey: obj.name,
                data: data,
                isSuccess: true,
                file:configFile
            };
        } catch (error) {
            var message = '[Error] Skipping file ' + configurationKey + ' as JSON parse failed ' + tenant;
            Utility.logError(message);
            return {
                configurationKey: configurationKey,
                message: "Error Parsing Inbound",
                error: error,
                isSuccess: false
            };
        }
    });
}

function compareConfiguration(key, oldConfigurations, newConfigurations, output) {
    if (oldConfigurations[key] != newConfigurations[key].data) {
        //var diff=jsonDiff.diffString(JSON.parse(oldConfigurations[key]),JSON.parse(newConfigurations[key]));
        var diff = Diff.diffLines(oldConfigurations[key], newConfigurations[key].data);
        output.changes.push({
            key: key,
            value: diff,
            isJSONDiff: false,
            file:newConfigurations[key].file
        });
    }
}

module.exports = function (folderPath, config) {
    return {
        serviceUrl: function(){
            var webhookServiceUrl = config.baseUrl + config.webhookConfiguration.servicePort;
            if (process.env.WEBHOOK_HOST) {
                webhookServiceUrl = process.env.WEBHOOK_HOST;
            }
            return webhookServiceUrl;
        },
        publish: function (configFile) {
            return publishInboundWebhook(config.tenants, config.tenant, this.serviceUrl(), configFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllInboundWebhook(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self=this;
            return getInboundDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                var anyFailed=false;
                var messages="";
                return Utility.processSync(files, (r) => {
                    return publishInboundWebhook(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function(result){
                        if (result !== true) {
                            anyFailed=true;
                            messages+=result;
                        }
                    });
                }).then(function(){
                    if(anyFailed==true){
                        throw new Error("Publish inbound webhook failed " + messages );
                    }
                    Utility.logSuccess("Publish inbound webhook changes completed");
                });
            });
        },
        GetAllDiff: function () {
            return getInboundDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        }
    };
};