'use strict';

var api = require('unirest');

function request(type, url, payload, token) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

    return new Promise(function (resolve, reject) {
        api[type](url)
            .headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
                'cache-control': 'no-cache'
            })
            .send(payload)
            .end(function (response) {
                resolve(response);
            });
    });
}

function requestWithoutToken(type, url) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

    return new Promise(function (resolve, reject) {
        api[type](url)
            .headers({
                'cache-control': 'no-cache'
            })
            .send()
            .end(function (response) {
                resolve(response);
            });
    });
}


function uploadFile(type, url, filePath, token) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

    return new Promise(function (resolve, reject) {
        api[type](url)
            .headers({
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token,
                'cache-control': 'no-cache'
            })
            .attach('file', filePath)
            .end(function (response) {
                resolve(response);
            });
    });
}

module.exports = function (token) {

    return {
        get: function (url) {
            return request('get', url, undefined, token);
        },
        put: function (url, payload) {
            return request('put', url, payload, token);
        },
        post: function (url, payload) {
            return request('post', url, payload, token);
        },
        delete: function (url, payload) {
            return request('delete', url, payload, token);
        },
        uploadFile: function (type, url, filePath) {
            return uploadFile(type, url, filePath, token);
        },
        getWithoutToken: function(url){
            return requestWithoutToken('get', url);
        }
    };
};