'use strict';
var Utility = require('./utility.js')();

function getTenants(serviceUrl) {
    var api = require('./api_module.js')("");
    return api.get(serviceUrl).then(function (result) {
        if (result.statusCode == 200) {
            return result.body;
        } else {
            Utility.logSuccess(serviceUrl);
            console.dir(result);
            throw new Error(result.statusCode);
        }
    });
}

module.exports = function (serviceUrl) {
    return {
        getTenants: function () {
            return getTenants(serviceUrl);
        }
    };
};