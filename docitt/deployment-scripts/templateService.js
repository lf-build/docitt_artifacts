'use strict';
var path = require('path');
var fs = require('fs');
var Diff = require('diff');
var Utility = require('./utility.js')();

function publishTemplate(tenants, tenant, serviceUrl, configFile, config, token) {
    var pathObject = path.parse(configFile);
    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == ".html") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, ".html");
        if (result.isSuccess) {
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
            return processTemplate(configFile, configurationKey, tenant, config).then(function (result) {
                if (result.isSuccess) {
                    return publishTemplateApi(serviceUrl, configurationKey, result.template, token);
                }
            });
        }
    } else {
        console.warn('[Warning] Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
    return Promise.resolve(false);
}

function publishAllTemplate(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".html", processTemplate).then(function (response) {
        var configurations = response.configurations;
        var configToProcess = [];
        for (var key in configurations) {
            configToProcess.push({
                key: key,
                value: configurations[key]
            });
        }

        return Utility.processSync(configToProcess, (r) => {
            return publishTemplateApi(serviceUrl, r.key, r.value.template, token);
        }).then(function () {
            Utility.logSuccess("Publish All templates completed");
        });
    });
}

function publishTemplateApi(serviceUrl, templateFile, template, token) {
    var api = require('./api_module.js')(token);
    return api.post(serviceUrl + '/' + template.name + '/' + template.version + '/' + template.format + '/deactivate', null).then(function (resultDeactivate) {
        return api.delete(serviceUrl + '/' + template.name + '/' + template.version + '/' + template.format, null).then(function (resultDelete) {
            return api.post(serviceUrl, template).then(function (result) {
                if (result.statusCode == 200) {
                    Utility.logSuccess("[Done] Publishing template: " + templateFile);
                    return api.post(serviceUrl + '/' + template.name + '/' + template.version + '/' + template.format + '/activate', null).then(function (resultActivate) {
                        if (result.statusCode == 200) {
                            Utility.logSuccess("[Done] Activated template: " + templateFile);
                            return true;
                        } else {
                            Utility.logError("[Failed] Activation failed for template: " + templateFile);
                            Utility.logError(result.raw_body);
                            return false;
                        }
                    });
                } else {
                    Utility.logError("[Error] Failed to publish template: " + templateFile);
                    Utility.logError(JSON.stringify(template));
                    Utility.logError(result.raw_body);
                    return false;
                }
            });
        });
    });
}

function getTemplateDiff(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getArtifactsDiff(tenants, tenant, serviceUrl, folderPath, config, token, ".html", processOldResponse, processTemplate, compareConfiguration);
}

function processOldResponse(result) {
    var oldConfigurations = {};
    for (var key in result) {
        var item = result[key];
        delete item.isActive;
        delete item.modifiedBy;
        delete item.createdBy;
        delete item.createdDate;
        delete item.modifiedDate;
        delete item.id;
        var template = JSON.stringify(item, null, 1);
        oldConfigurations[item.name + "." + item.version] = template;
    }
    return Promise.resolve(oldConfigurations);
}

function compareConfiguration(key, oldConfigurations, newConfigurations, output) {
    if (oldConfigurations[key] != newConfigurations[key].data) {
        //var diff=jsonDiff.diffString(JSON.parse(oldConfigurations[key]),JSON.parse(newConfigurations[key]));
        var templateSchemaOld=JSON.parse(oldConfigurations[key]);
        var templateSchemaNew=JSON.parse(newConfigurations[key].data);
        var oldHtml=templateSchemaOld.body.replace(/\r|\n|\t/g, '');
        var newHtml=templateSchemaNew.body.replace(/\r|\n|\t/g, '');
        delete templateSchemaOld.body;
        delete templateSchemaNew.body;

        var diff = Diff.diffLines(JSON.stringify(templateSchemaOld, null, 1), JSON.stringify(templateSchemaNew, null, 1));
        diff=diff.concat(Diff.diffLines(oldHtml, newHtml));
        output.changes.push({
            key: key,
            value: diff,
            isJSONDiff: false,
            file: newConfigurations[key].file
        });
    }
}

function processTemplate(configFile, configurationKey, tenant, config) {
    var promises = [];
    var htmlData = "";
    var templateSchema = "";
    promises.push(fs.promises.readFile(configFile, {
        encoding: 'utf8'
    }).then(function (response) {
        htmlData = response;
    }));
    promises.push(fs.promises.readFile(configFile.replace('.html', '.json'), {
        encoding: 'utf8'
    }).then(function (response) {
        templateSchema = response;
    }));

    return Promise.all(promises).then(function () {
        templateSchema = templateSchema.replace("{{body}}", '');
        templateSchema = Utility.replaceStringForJSON(config, templateSchema,configurationKey);
        var template = JSON.parse(templateSchema);
        htmlData = Utility.replaceStringForText(config, htmlData,configurationKey);
        template.body = htmlData.replace(/\r|\n|\t/g, '');
        delete template.id;

        return {
            configurationKey: template.name + "." + template.version,
            data: JSON.stringify(template, null, 1),
            template: template,
            isSuccess: true,
            file: configFile
        };
    });


}

module.exports = function (folderPath, config) {
    return {
        serviceUrl: function () {
            var templateServiceUrl = config.baseUrl + config.templateConfiguration.servicePort;
            if (process.env.TEMPLATE_HOST) {
                templateServiceUrl = process.env.TEMPLATE_HOST;
            }
            return templateServiceUrl;
        },
        publish: function (templateFile) {
            return publishTemplate(config.tenants, config.tenant, this.serviceUrl(), templateFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllTemplate(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self= this;
            return getTemplateDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var files = response.changes.map((a) => a.file);
                var anyFailed = false;
                files = files.concat(response.newFiles.map((a) => a.file));
                return Utility.processSync(files, (r) => {
                    return publishTemplate(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function (result) {
                        if (result == false) {
                            anyFailed = true;
                        }
                    });
                }).then(function () {
                    if (anyFailed == true) {
                        throw new Error("Publish template failed");
                    }
                    Utility.logSuccess("Publish template Changes completed");
                });
            });
        },
        GetAllDiff: function () {
            return getTemplateDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        }
    };
};