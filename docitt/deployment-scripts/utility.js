'use strict';
var path = path || require('path');
var fs = fs || require('fs');

var walkSync = function (dir, fileList) {
    var files = fs.readdirSync(dir);
    fileList = fileList || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            fileList = walkSync(path.join(dir, file), fileList);
        } else {
            fileList.push(path.join(dir, file));
        }
    });
    return fileList;
};

var replaceStringForJSON = function (config, data, key, onlyIf) {

    if (config != null && config.data_replacement != null && config.data_replacement.length > 0) {
        for (var i in config.data_replacement) {
            var regex = new RegExp("{{" + config.data_replacement[i].name + "}}", "g");
            if (config.data_replacement[i].type == "object") {
                data = data.replace(regex, JSON.stringify(config.data_replacement[i].value));
            } else {
                data = data.replace(regex, config.data_replacement[i].value);
            }
        }
    }
    var result= data.match(new RegExp("\{{([^}]+)\}}","g"));
    if(result && result.length>0){
        var any=false;
        var message="Missing replacement value for " + key + " :";
        var ignoreList=["{{TemporaryApplicationNumber}}","{{borrowername}}"].map(function(value) {
            return value.toLowerCase();
          });
        result.forEach(element => {
            if(ignoreList.indexOf(element.toLowerCase())==-1){
                if(onlyIf==null || onlyIf.indexOf(element)!==-1){
                    any=true;
                    message+=" " + element;
                }
            }
        });
        if(any==true){
            logError(message);
            throw Error(message);
        }
    }
    return data;
};

var replaceStringForText = function (config, data,key) {

    if (config != null && config.data_replacement != null && config.data_replacement.length > 0) {
        for (var i in config.data_replacement) {
            var regex = new RegExp("<<" + config.data_replacement[i].name + ">>", "g");
            data = data.replace(regex, config.data_replacement[i].value);
        }
    }
    var result= data.match(new RegExp("\\<<(.*?)\>>","g"));
    if(result && result.length>0){
        var message="Missing replacement value for " + key + " :";
        result.forEach(element => {
            message+=" " + element;
        });
        logError(message);
        throw Error(message);
    }
    return data;
};

var getAllLocalFiles = function (folderPath, tenants, tenant, config, extension, processObject) {
    var files = walkSync(folderPath);
    var configurations = {};
    var failedConfigurations = [];
    var promises = [];
    files.forEach((element) => {
        promises.push(processLocalFiles(element, tenants, tenant, config, extension, processObject).then(function (configResult) {
            var pathObject = path.parse(element);
            if (configResult != null) {
                if (configResult.isSuccess == true) {
                    configurations[configResult.configurationKey] = configResult;
                    if (configResult.minimizationFailed == true) {
                        failedConfigurations.push({
                            key: configResult.configurationKey,
                            message: "JavScript minification Failed with error " + configResult.minimizationFailedMessage
                        });
                    }
                } else {
                    failedConfigurations.push({
                        key: configResult.configurationKey,
                        message: configResult.message
                    });
                }
            } else {
                if (!(extension == ".html" && pathObject.ext.toLocaleLowerCase() == ".json") && pathObject.name!==".jshintrc") {
                    logError("Processing failed for " + element);
                }
            }
        }));
    });
    for (var i = 0; i < files.length; i++) {
        var element = files[i];

    }
    return Promise.all(promises).then(function () {
        return {
            configurations: configurations,
            failedConfigurations: failedConfigurations
        };
    });
};

function isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, extension) {
    var isEnvSpecificFile = false;
    var fileEnv = null;
    var message;

    for (var i = 0; i < tenants.length; i++) {
        if (pathObject.name.endsWith("." + tenants[i])) {
            fileEnv = tenants[i];
            isEnvSpecificFile = true;
        }
    }
    if (isEnvSpecificFile == false) {
        if (fs.existsSync(pathObject.dir + "/" + pathObject.name + "." + tenant + extension)) {
            isEnvSpecificFile = true;
        }
    }
    if (isEnvSpecificFile == true) {
        if (fileEnv != tenant) {
            message = '[Error] Skipping file ' + pathObject.name + ' as not specific to ' + tenant;
            logInfo(message);
            var result = {
                configurationKey: configurationKey,
                isSuccess: false,
                isNotSpecificToTenant: true,
                message: message
            };
            return result;
        } else {
            configurationKey = configurationKey.replace("." + tenant, '');
            logInfo('[Info] Using file ' + pathObject.name + ' as specific to ' + tenant + ' With key ' + configurationKey);
            return {
                configurationKey: configurationKey,
                isSuccess: true
            };
        }
    } else {
        var allowedAfterDot=[".1.0",".1.1"];
        var fileName=pathObject.name;
        allowedAfterDot.forEach(item=>fileName=fileName.replace(item,""));

        if(fileName.indexOf(".")!=-1){
            message='[Error] Skipping file as looks like tenant specific ' + pathObject.name + '  With key ' + configurationKey;
            logError(message);
            return {
                configurationKey: configurationKey,
                isSuccess: false,
                isNotSpecificToTenant: true,
                message: message
            };
        }
        return {
            configurationKey: configurationKey,
            isSuccess: true
        };
    }
}

function processLocalFiles(configFile, tenants, tenant, config, extension, processObject) {

    var pathObject = path.parse(configFile);
    var message = "";

    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == extension) {
        var result = isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, extension);
        if (result == null || result.isSuccess) {
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
        } else {
            return Promise.resolve({
                configurationKey: configurationKey,
                isSuccess: false,
                isValid: false,
                message: result.message
            });
        }
        return processObject(configFile, configurationKey, tenant, config);

    } else {
        if (extension == ".html" && pathObject.ext.toLocaleLowerCase() == ".json") {
            //ignore template's JSON file
            return Promise.resolve(null);
        }
        if(configurationKey==".jshintrc"){
            //ignore template's JSON file
            return Promise.resolve(null);
        }
        message = '[Error] Skipping file ' + configurationKey + ' as not ' + extension + ' file for ' + tenant;
        logInfo(message);
        return Promise.resolve({
            configurationKey: configurationKey,
            isSuccess: false,
            isValid: false,
            message: message
        });

    }
}

function toCamel(o) {
    var newO, origKey, newKey, value;
    if (o instanceof Array) {
        return o.map(function (value) {
            if (typeof value === "object") {
                value = toCamel(value);
            }
            return value;
        });
    } else {
        newO = {};
        for (origKey in o) {
            if (o.hasOwnProperty(origKey)) {
                newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString();
                value = o[origKey];
                if (value instanceof Array || (value !== null && value.constructor === Object)) {
                    value = toCamel(value);
                }
                newO[newKey] = value;
            }
        }
    }
    return newO;
}

var getArtifactsDiff = function (tenants, tenant, serviceUrl, folderPath, config, token, extension, processOldFile, processObject, compareConfiguration) {
    var api = require('./api_module.js')(token);
    var output = {};

    output.newFiles = [];
    output.changes = [];
    output.extraFiles = [];
    output.errors = {};
    output.isSuccess = true;

    return api.get(serviceUrl).then(function (result) {
        if (result.statusCode == 200) {
            var key = "";
            return processOldFile(result.body, token, serviceUrl).then(function (oldConfigurations) {

                return getAllLocalFiles(folderPath, tenants, tenant, config, extension, processObject).then(function (localConfigurationResult) {
                    var newConfigurations = localConfigurationResult.configurations;
                    output.errors.failedConfigurations = localConfigurationResult.failedConfigurations;

                    for (key in newConfigurations) {
                        if (oldConfigurations[key] != null) {
                            try {
                                compareConfiguration(key, oldConfigurations, newConfigurations, output);
                            } catch (error) {
                                logError(key + " failed in compare");
                                console.dir(error);
                                throw error;
                            }
                        } else {
                            output.newFiles.push({
                                key: key,
                                value: newConfigurations[key].data,
                                file: newConfigurations[key].file
                            });
                        }
                    }

                    for (key in oldConfigurations) {
                        if (newConfigurations[key] == null) {
                            output.extraFiles.push({
                                key: key,
                                value: oldConfigurations[key]
                            });
                        }
                    }

                    return output;
                });
            });
        } else {
            output.isSuccess = false;
            output.errors.message = "Unable to get data from server " + serviceUrl + " Status " + result.statusCode;
        }
        return Promise.resolve(output);
    });
};

var processSync = function (list, callBack) {
    list = list.map(r => () => {
        return callBack(r);
    });

    return list.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('[Error] failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
};

var logError = function (message) {
    console.error('\x1b[31m', message.toString().trimRight(), '\x1b[0m');
};

var logSuccess = function (message) {
    console.log('\x1b[32m', message.toString().trimRight(), '\x1b[0m');
};

var logInfo = function (message) {
    console.log('\x1b[36m%s\x1b[0m', message);
};


module.exports = function () {
    return {
        walkSync: function (dir, fileList) {
            return walkSync(dir, fileList);
        },
        replaceStringForJSON: function (config, data, key,onlyIf) {
            return replaceStringForJSON(config, data, key,onlyIf);
        },
        replaceStringForText: function (config, data, key) {
            return replaceStringForText(config, data, key);
        },
        getAllLocalFiles: function (folderPath, tenants, tenant, config, extension, processObject) {
            return getAllLocalFiles(folderPath, tenants, tenant, config, extension, processObject);
        },
        getArtifactsDiff: function (tenants, tenant, serviceUrl, folderPath, config, token, extension, processOldFile, processObject, compareConfiguration) {
            return getArtifactsDiff(tenants, tenant, serviceUrl, folderPath, config, token, extension, processOldFile, processObject, compareConfiguration);
        },
        toCamel: function (o) {
            return toCamel(o);
        },
        isTenantSpecificFile: function (tenants, tenant, configurationKey, pathObject, extension) {
            return isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, extension);
        },
        processSync: function (list, callBack) {
            return processSync(list, callBack);
        },
        logError: function (message) {
            return logError(message);
        },
        logSuccess: function (message) {
            return logSuccess(message);
        },
        logInfo: function (message) {
            return logInfo(message);
        }
    };
};