'use strict';
var path = require('path');
var fs = require('fs');
var Diff = require('diff');
var Utility = require('./utility.js')();
var fs = fs || require('fs');


function publishOutboundWebhook(tenants, tenant, serviceUrl, configFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(configFile);
    var configurationKey = pathObject.name;

    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, configurationKey, pathObject, ".json");
        if (result.isSuccess) {
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
            return processConfiguration(configFile, configurationKey, tenant, config).then(function (result) {
                if (result.isSuccess) {
                    return api.post(serviceUrl + '/outbound', result.data).then(function (result) {
                        if (result.statusCode == 200) {
                            Utility.logSuccess("[Done] Publishing outbound webhook: " + configurationKey);
                            return true;
                        } else {
                            Utility.logError("[Error] Failed to publish outbound webhook: " + configurationKey);
                            Utility.logError(JSON.stringify(result));
                            Utility.logError(result.raw_body);
                        }
                        return false;
                    });
                }
            });
        }
    } else {
        Utility.logError('[Warning] Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
    return Promise.resolve(false);
}

function publishAllOutboundWebhook(tenants, tenant, serviceUrl, folderPath, config, token) {
    var api = require('./api_module.js')(token);
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".json", processConfiguration).then(function (response) {
        var configurations = response.configurations;
        var configToProcess = [];
        for (var key in configurations) {
            configToProcess.push({
                key: key,
                value: configurations[key]
            });
        }

        return Utility.processSync(configToProcess, (r) => {
            return api.post(serviceUrl + '/outbound', r.value.data).then(function (result) {
                if (result.statusCode == 200) {
                    Utility.logSuccess("[Done] Publishing outbound webhook: " + r.key);
                } else {
                    Utility.logError("[Error] Failed to publish outbound webhook: " + r.key);
                    Utility.logError(JSON.stringify(result));
                    Utility.logError(result.raw_body);
                }
            });
        }).then(function () {
            Utility.logSuccess("Publish All outbound webhook completed");
        });
    });
}

function getOutboundDiff(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getArtifactsDiff(tenants, tenant, serviceUrl + "/outbound", folderPath, config, token, ".json", processOldResponse, processConfiguration, compareConfiguration);
}

function processOldResponse(result) {
    var oldWebHooks = {};
    for (var key in result) {
        var item = result[key];
        delete item._id;
        delete item.tenantId;
        var template = JSON.stringify(item, null, 1);
        oldWebHooks[item.name] = template;
    }
    return Promise.resolve(oldWebHooks);
}

function processConfiguration(configFile, configurationKey, tenant, config) {
    return fs.promises.readFile(configFile, {
        encoding: 'utf8'
    }).then(function (data) {
        data = Utility.replaceStringForJSON(config, data,configurationKey,["webhook_callback_url"]);
        try {
            var obj = JSON.parse(data);
            data = JSON.stringify(obj, null, 1);
            return {
                configurationKey: obj.name,
                data: data,
                isSuccess: true,
                file: configFile
            };
        } catch (error) {
            var message = '[Error] Skipping file ' + configurationKey + ' as JSON parse failed ' + tenant;
            Utility.logError(message);
            return {
                configurationKey: configurationKey,
                message: message,
                error: error,
                isSuccess: false,
                file: configFile
            };
        }
    });
}

function compareConfiguration(key, oldConfigurations, newConfigurations, output) {
    if (oldConfigurations[key] != newConfigurations[key].data) {
        //var diff=jsonDiff.diffString(JSON.parse(oldConfigurations[key]),JSON.parse(newConfigurations[key]));
        var diff = Diff.diffLines(oldConfigurations[key], newConfigurations[key].data);
        output.changes.push({
            key: key,
            value: diff,
            isJSONDiff: false,
            file: newConfigurations[key].file
        });
    }
}


module.exports = function (folderPath, config) {
    return {
        serviceUrl: function(){
            var webhookServiceUrl = config.baseUrl + config.webhookConfiguration.servicePort;
            if (process.env.WEBHOOK_HOST) {
                webhookServiceUrl = process.env.WEBHOOK_HOST;
            }
            return webhookServiceUrl;
        },
        publish: function (configFile) {
            return publishOutboundWebhook(config.tenants, config.tenant, this.serviceUrl(), configFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllOutboundWebhook(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self=this;
            return getOutboundDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                var anyFailed = false;
                return Utility.processSync(files, (r) => {
                    return publishOutboundWebhook(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function (result) {
                        if (result == false) {
                            anyFailed = true;
                        }
                    });
                }).then(function () {
                    if (anyFailed == true) {
                        throw new Error("Publish outbound webhook failed");
                    }
                    Utility.logSuccess("Publish outbound webhook changes completed");
                });
            });
        },
        GetAllDiff: function () {
            return getOutboundDiff(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        }
    };
};