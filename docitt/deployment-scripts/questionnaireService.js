'use strict';
var path = require('path');
var fs = require('fs');
var Utility = require('./utility.js')();
var Diff = require('diff');

function publishQuestionnaire(tenants, tenant, serviceUrl, questionnaireFile, config, token) {
    var pathObject = path.parse(questionnaireFile);
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        var result = Utility.isTenantSpecificFile(tenants, tenant, pathObject.name, pathObject, ".json");
        if (result.isSuccess) {
            var configurationKey = pathObject.name;
            if (result.configurationKey) {
                configurationKey = result.configurationKey;
            }
            return processQuestionnaire(questionnaireFile, configurationKey, tenant, config).then(function (response) {
                return publishQuestionnaireToServer(serviceUrl, response.questionnaire, token);
            });
        }
    } else {
        Utility.logError('[Warning] Skipping file ' + questionnaireFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
    return Promise.resolve(false);
}

function publishQuestionnaireToServer(serviceUrl, questionnaire, token) {
    var api = require('./api_module.js')(token);
    Utility.logSuccess("[info] Posting new questionnaire " + questionnaire.formName + " with version: " + questionnaire.majorVersion + "." + questionnaire.minorVersion);
    return api.post(serviceUrl + '/forms', questionnaire).then(function (submittedFormsResponse) {
        if (submittedFormsResponse.statusCode == 200 || submittedFormsResponse.statusCode == 204 || submittedFormsResponse.statusCode == 404) {
            var formVersion = submittedFormsResponse.body.version;
            Utility.logSuccess("[Done] Posting new questionnaire with version: " + formVersion);
            return api.post(serviceUrl + '/forms/' + questionnaire.formName + '/' + formVersion + '/activate').then(function (result) {
                if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                    Utility.logSuccess("[Done] Activating questionnaire with version: " + formVersion);
                    Utility.logSuccess("[Done] Published questionnaire");
                    return true;
                } else {
                    Utility.logError("[Error] Failed to activate new questionnaire: ");
                    Utility.logError(JSON.stringify(questionnaire));
                    Utility.logError(result.raw_body);
                    return JSON.stringify(result.raw_body);
                }
            });
        } else {
            Utility.logError("[Error] Failed to post new questionnaire: " + questionnaire.formName + " " + questionnaire.majorVersion + "." + questionnaire.minorVersion);
            Utility.logError(submittedFormsResponse.statusCode);
            Utility.logError(serviceUrl + '/forms' + " ");
            Utility.logError(JSON.stringify(submittedFormsResponse.raw_body));
            return JSON.stringify(submittedFormsResponse.raw_body);
        }
    });
}

function publishAllQuestionnaire(tenants, tenant, serviceUrl, folderPath, config, token) {
    return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".json", processQuestionnaire).then(function (response) {
        var configurations = response.configurations;
        var configToProcess = [];
        for (var key in configurations) {
            configToProcess.push({
                key: key,
                value: configurations[key]
            });
        }
        return Utility.processSync(configToProcess, (r) => {
            return publishQuestionnaireToServer(serviceUrl, r.value.questionnaire, token);
        });
    });
}

function processQuestionnaire(file, key, tenant, config) {
    Utility.logSuccess("Processing file " + file + " " + key);
    return fs.promises.readFile(file, {
        encoding: "utf8"
    }).then(function (data) {
        data = Utility.replaceStringForJSON(config, data,key);
        try {
            var obj = JSON.parse(data);
            data = JSON.stringify(obj, null, 1);
            return {
                configurationKey: key,
                data: data,
                isSuccess: true,
                file: file,
                questionnaire: obj,
                version: obj.majorVersion + "." + (obj.minorVersion.toString().length==1? "0"+obj.minorVersion.toString() : obj.minorVersion)
            };
        } catch (error) {
            var message = '[Error] Skipping file ' + key + ' as JSON parse failed ' + tenant;
            Utility.logError(message);
            return {
                key: key,
                message: message,
                error: error,
                isSuccess: false
            };
        }
    }).catch(function (error) {
        Utility.logError("Error");
        Utility.logError(error);
    });
}

function diffVersions(tenants, tenant, serviceUrl, folderPath, config, token) {
    var api = require('./api_module.js')(token);
    return api.get(serviceUrl + '/questionnaires/active').then(function (response) {
        var oldQuestionnaire = {};
        response.body.forEach(function (item) {
            oldQuestionnaire[item.name] = item.version;
        });
        return Utility.getAllLocalFiles(folderPath, tenants, tenant, config, ".json", processQuestionnaire).then(function (response) {
            var localFiles = response.configurations;
            var output = {};

            output.newFiles = [];
            output.changes = [];
            output.extraFiles = [];
            output.errors = {};
            output.isSuccess = true;

            for (var key in localFiles) {
                if (oldQuestionnaire[key] != null) {
                    if (oldQuestionnaire[key] != localFiles[key].version) {
                        var diff = Diff.diffLines(oldQuestionnaire[key].toString(), localFiles[key].version.toString());

                        output.changes.push({
                            key: key,
                            value: diff,
                            isJSONDiff: false,
                            file: localFiles[key].file
                        });
                    }
                } else {
                    output.newFiles.push({
                        key: key,
                        value: localFiles[key].version,
                        file: localFiles[key].file
                    });
                }
            }

            for (key in oldQuestionnaire) {
                if (localFiles[key] == null) {
                    output.extraFiles.push({
                        key: key,
                        value: oldQuestionnaire[key]
                    });
                }
            }

            return output;
        });
    });

}

module.exports = function (folderPath, config) {
    return {
        serviceUrl: function(){
            var questionnaireServiceUrl = config.baseUrl + config.questionnaireConfiguration.servicePort;
            if (process.env.QUESTIONNAIRE_HOST) {
                questionnaireServiceUrl = process.env.QUESTIONNAIRE_HOST;
            }
            return questionnaireServiceUrl;
        },
        publish: function (questionnaireFile) {
            return publishQuestionnaire(config.tenants, config.tenant, this.serviceUrl(), questionnaireFile, config, config.authorizationToken);
        },
        publishAll: function () {
            return publishAllQuestionnaire(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        GetAllDiff: function () {
            return diffVersions(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken);
        },
        publishChanges: function () {
            var self=this;
            return diffVersions(config.tenants, config.tenant, this.serviceUrl(), folderPath, config, config.authorizationToken).then(function (response) {
                var files = response.changes.map((a) => a.file);
                files = files.concat(response.newFiles.map((a) => a.file));
                var anyFailed = false;
                var messages="";
                return Utility.processSync(files, (r) => {
                    return publishQuestionnaire(config.tenants, config.tenant, self.serviceUrl(), r, config, config.authorizationToken).then(function (result) {
                        if (result !== true) {
                            anyFailed = true;
                            messages+=result;
                        }
                    });
                }).then(function () {
                    if (anyFailed == true) {
                        throw new Error("Publish questionnaire failed " + messages);
                    }
                    Utility.logSuccess("Publish questionnaire Changes completed");
                });
            });
        }
    };
};