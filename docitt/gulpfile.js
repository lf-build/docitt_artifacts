'use strict';
var gulp = require('gulp');
var argv = require('yargs').argv;
var serviceFactory = require('./deployment-scripts/serviceFactory.js')();

var currentFile = argv.file;
var configuration = argv.configuration;

function validateArgs() {
    if (!currentFile) {
        throw {
            name: "InvalidFileError",
            message: "A file must be specified with the flag --file",
            stack: new Error().stack
        };
    }
    if (!configuration) {
        throw {
            name: "InvalidConfigurationError",
            message: "A file must be specified with the flag --configuration",
            stack: new Error().stack
        };
    }
}

console.log('File ' + currentFile);
console.log('Tenant ' + configuration);

/**
 * Publish a portal configuration to the deployment env of your choice
 * @example gulp --configuration=dev --file=./configurations/back-office-portal.dev.json publishConfiguration
 */



var initialize = function () {
    return serviceFactory.getConfiguration("./" + configuration + '.json');
};

function getService(name) {
    return initialize().then(function (config) {
        return serviceFactory.getService(name, config, currentFile + "/" + serviceFactory.folders[name]);
    });
}

gulp.task('publishConfiguration', function () {
    validateArgs();
    return getService("configuration").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllConfigurations', function () {
    validateArgs();
    return getService("configuration").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishConfigurationChanges', function () {
    validateArgs();
    return getService("configuration").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});

gulp.task('ShowConfigurationChanges', function () {
    validateArgs();
    return getService("configuration").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});

gulp.task('publishWebhookOutbound', function () {
    validateArgs();
    return getService("outboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllWebhookOutbound', function () {
    validateArgs();
    return getService("outboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishWebhookOutboundChanges', function () {
    validateArgs();
    return getService("outboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});

gulp.task('ShowWebhookOutboundChanges', function () {
    validateArgs();
    return getService("outboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});

gulp.task('publishWebhookInbound', function () {
    validateArgs();
    return getService("inboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllWebhookInbound', function () {
    validateArgs();
    return getService("inboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishWebhookInboundChanges', function () {
    validateArgs();
    return getService("inboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});

gulp.task('ShowWebhookInboundChanges', function () {
    validateArgs();
    return getService("inboundWebhook").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});


gulp.task('publishQuestionnaire', function () {
    validateArgs();
    return getService("questionnaire").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllQuestionnaire', function () {
    validateArgs();
    return getService("questionnaire").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishQuestionnaireChanges', function () {
    validateArgs();
    return getService("questionnaire").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});

gulp.task('ShowQuestionnaireChanges', function () {
    validateArgs();
    return getService("questionnaire").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});

gulp.task('publishTemplate', function () {
    validateArgs();
    return getService("template").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllTemplates', function () {
    validateArgs();
    return getService("template").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishTemplateChanges', function () {
    validateArgs();
    return getService("template").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});

gulp.task('ShowTemplateChanges', function () {
    validateArgs();
    return getService("template").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});


gulp.task('publishRule', function () {
    validateArgs();
    return getService("rule").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllRule', function () {
    validateArgs();
    return getService("rule").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishRuleChanges', function () {
    validateArgs();
    return getService("rule").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});

gulp.task('ShowRuleChanges', function () {
    validateArgs();
    return getService("rule").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});

gulp.task('convertToES5', function () {
    validateArgs();
    return getService("rule").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.convertToES5();
    });
});

gulp.task('publishLookup', function () {
    validateArgs();
    return getService("lookup").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publish(currentFile);
    });
});

gulp.task('publishAllLookup', function () {
    validateArgs();
    return getService("lookup").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishAll();
    });
});

gulp.task('publishLookupChanges', function () {
    validateArgs();
    return getService("lookup").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.publishChanges();
    });
});
gulp.task('ShowLookupChanges', function () {
    validateArgs();
    return getService("lookup").then(function (service) {
        console.log("Service URL " + service.serviceUrl());
        return service.GetAllDiff();
    });
});

gulp.task('initializeTheme', function () {
    validateArgs();
    return getService("theme").then(function (service) {
        console.log("Service URL asset " + service.assetServiceUrl());
        console.log("Service URL theme " + service.themeServiceUrl());
        return service.initializeTheme();
    });

});

gulp.task('publishLessFileChange', function () {
    validateArgs();
    return getService("theme").then(function (service) {
        console.log("Service URL asset " + service.assetServiceUrl());
        console.log("Service URL theme " + service.themeServiceUrl());
        return service.publishLessFileChange();
    });

});

gulp.task('ShowCSSChanges', function () {
    validateArgs();
    return getService("theme").then(function (service) {
        console.log("Service URL asset " + service.assetServiceUrl());
        console.log("Service URL theme " + service.themeServiceUrl());
        return service.diffLessFiles();
    });
});

gulp.task('publishAll', gulp.series('publishAllConfigurations', 'publishAllLookup','publishAllTemplates', 'publishAllRule', 'publishAllWebhookOutbound', 'publishAllWebhookInbound', 'publishAllQuestionnaire','initializeTheme'));
gulp.task('publishAllChanges', gulp.series('publishConfigurationChanges', 'publishLookupChanges','publishTemplateChanges', 'publishRuleChanges', 'publishWebhookOutboundChanges', 'publishWebhookInboundChanges', 'publishQuestionnaireChanges','publishLessFileChange'));