var MongoClient = require('mongodb').MongoClient;
var fetch = require('node-fetch')
var url = 'mongodb://docitt:docSigma@10.100.0.11:27017/assignment?authSource=admin';
const tenant = 'naf';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkb2NpdHQiLCJpYXQiOjE1NzE1NTgzNjMsImV4cCI6MTU3MTU3Mjc2Mywic3ViIjoiYWRtaW5AbmFmLmNvbSIsInRlbmFudCI6Im5hZiIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIiwiYWRtaW4tcG9ydGFsIl0sIklzVmFsaWQiOnRydWV9.e3iUhaq3V191xB0XBuFcP9LvfFZrdHfLqTmYhzb79Fg';
const eventHubUrl="http://docitt.dev.lendfoundry.com:5004";

const limit = 5;
const sleep = 1000;
var self = this;
MongoClient.connect(url, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(function (client) {
	const db = client.db('assignment');

	return db.collection('invites').find({
        'TenantId': tenant,
        'UserName': null, 
        'Role' : 'Borrower'
	}).count().then(function (count) {
		let currentPos = 1;
		//let lastPage = Math.ceil(1000 / limit);
		let lastPage = Math.ceil(count / limit) //uncomment if needed to run for all the records
		console.log("Count is " + count);
		var pages=[];
		for (var i = 1; i <= lastPage; i++) {
			currentPos = i;
			var skip = limit * (currentPos - 1);
			pages.push(skip);
		}

		pages=pages.map(r => () => {
			console.log("Skipping:"+ r);
			return db.collection('invites')
				.find({
                    'TenantId': tenant,
                    'UserName': null, 
                    'Role' : 'Borrower'
				}, {
					TemporaryApplicationNumber: 1
				})
				.sort({
					_id: -1
				})
				.skip(r)
				.limit(limit)
				.toArray().then(function(applications){
					applications = applications.map(r => () => {
						let payload = JSON.stringify({
							"InviteData": {
								"EntityType": r.EntityType,
								"EntityId": r.EntityId,
								"InvitedBy": r.InvitedBy,
								"InviteEmail": r.InviteEmail,
								"InviteFirstName": r.InviteFirstName,
								"InviteLastName": r.InviteLastName,
								"InviteMobileNumber": r.InviteMobileNumber,
								"InvitationDate": r.InvitationDate.DateTime,
								"Role": "Borrower",
								"UserName": r.UserName,
								"UserId": r.UserId,
								"InvitationUrl": r.InvitationUrl,
								"InvitationAcceptedDate": null,
								"InvitationToken": r.InvitationToken,
								"InvitationTokenExpiry": r.InvitationTokenExpiry,
								"Team": r.Team,
								"LastReminder": null,
								"ReferenceApplicationNumbers": r.ReferenceApplicationNumbers,
								"CreditReportJobId": "",
								"PartnerDetails": r.PartnerDetails,
								"Source": "BorrowerApplication",
								"Id": r._id
							}
                          });

						return fetch(eventHubUrl+"/DummyUserInvitationSend", {
								'method': 'POST',
								'headers': {
									'content-type': 'application/json',
									'authorization': 'Bearer ' + token
								},
								'body': payload
							})
							.then(response => {
								console.log('Executed loan -> ' + r.InviteEmail + ' Response Code : ' + response.status);
							})
							.catch(err => console.error(err));
					});

					return applications.reduce((a, c) => {
						return a.then(_ => {
							return c();
						}).catch(reason => {
							//TODO: Check if fail, it will publish others
							console.error('[Error] Publish Event failed: ', reason);
							return c();
						});
					}, new Promise((resolve) => resolve({}))).then(function () {
						
					});
				})
				.catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Mongo: ', reason);
					return c();
				});
		});

		return pages.reduce((a, c) => {
			return a.then(_ => {
				return c();
			}).catch(reason => {
				//TODO: Check if fail, it will publish others
				console.error('[Error] getting data Event failed: ', reason);
				return c();
			});
		}, new Promise((resolve) => resolve({}))).then(function () {
			client.close();
		});

	})
	.catch(reason => {
		//TODO: Check if fail, it will publish others
		console.error('[Error] catch failed: ', reason);
		return c();
	});
}).catch(function(){
	console.log("Error ocurred");
});