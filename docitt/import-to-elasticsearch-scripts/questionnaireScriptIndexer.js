'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
var MongoClient = require('mongodb').MongoClient;
var fetch = require('node-fetch')
var url = 'mongodb://root:MongoProd123@34.217.98.36:30830/questionnaires?authSource=admin';
const tenant = 'naf';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0ZW5hbnQiLCJpYXQiOjE1MzUwODU5MzcsImV4cCI6bnVsbCwic3ViIjpudWxsLCJ0ZW5hbnQiOiJuYWYiLCJzY29wZSI6bnVsbCwiSXNWYWxpZCI6dHJ1ZX0.VMhDhG1KtC3Xwly4jcZmfgnmnN1RctX1v_vQyLlqULQ';
const eventHubUrl="http://docitt.dev.lendfoundry.com:5004";

const limit = 5;
function sleep(ms) {
	return new Promise((resolve) => {
	  setTimeout(resolve, ms);
	});
  }   
MongoClient.connect(url, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(function (client) {
	const db = client.db('questionnaires');

	return db.collection('questionnaire').find({
		'TenantId': tenant
	}).count().then(function (count) {
		let currentPos = 1;
		//let lastPage = Math.ceil(1000 / limit);
		let lastPage = Math.ceil(count / limit) //uncomment if needed to run for all the records
		console.log("Count is " + count);
		var pages=[];
		for (var i = 1; i <= lastPage; i++) {
			currentPos = i;
			var skip = limit * (currentPos - 1);
			pages.push(skip);
		}

		pages=pages.map(r => () => {
			console.log("Skipping:"+ r);
			return db.collection('questionnaire')
				.find({
					'TenantId': tenant
				}, {
					TemporaryApplicationNumber: 1
				})
				.sort({
					_id: -1
				})
				.skip(r)
				.limit(limit)
				.toArray().then(function(applications){
					applications = applications.map(r => () => {
						let payload = JSON.stringify({
							"entityType": "application",
							"EntityId": r.TemporaryApplicationNumber
						});

						return fetch(eventHubUrl+"/DummyQuestionnaireIndexer", {
								'method': 'POST',
								'headers': {
									'content-type': 'application/json',
									'authorization': 'Bearer ' + token
								},
								'body': payload
							})
							.then(response => {
								console.log('Executed loan -> ' + r.TemporaryApplicationNumber + ' Response Code : ' + response.status);
							})
							.then(function(){
								return sleep(3000);
							})
							.catch(err => console.error(err));
					});

					return applications.reduce((a, c) => {
						return a.then(_ => {
							return c();
						}).catch(reason => {
							//TODO: Check if fail, it will publish others
							console.error('[Error] Publish Event failed: ', reason);
							return c();
						});
					}, new Promise((resolve) => resolve({}))).then(function () {
					});
				})
				.catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Mongo: ', reason);
					return c();
				});
		});

		return pages.reduce((a, c) => {
			return a.then(_ => {
				return c();
			}).catch(reason => {
				//TODO: Check if fail, it will publish others
				console.error('[Error] getting data Event failed: ', reason);
				return c();
			});
		}, new Promise((resolve) => resolve({}))).then(function () {
			client.close();
		});

	})
	.catch(reason => {
		//TODO: Check if fail, it will publish others
		console.error('[Error] catch failed: ', reason);
		return c();
	});
}).catch(function(){
	console.log("Error ocurred");
});