var MongoClient = require('mongodb').MongoClient;
var fetch = require('node-fetch')
var url = 'mongodb://docitt:docSigma@10.100.0.11:27017/questionnaires?authSource=admin';
const tenant = 'naf';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkb2NpdHQiLCJpYXQiOjE1NjcxNDMyODIsImV4cCI6MTU2NzE1NzY4Miwic3ViIjoicHJhYmhhdC5zaW5oYStsZW5kZXJuYWZAbGVuZGZvdW5kcnkuY29tIiwidGVuYW50IjoibmFmIiwic2NvcGUiOlsieyphbnl9IiwiYmFjay1vZmZpY2UiXSwiSXNWYWxpZCI6dHJ1ZX0.K8738Kspha4ktgR1AnZ2wXp2T_tb289yB_HeGShUOr4';
const eventHubUrl="http://docitt.dev.lendfoundry.com:5004";
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0 

const limit = 5;
const sleep = 1000;
var self = this;
MongoClient.connect(url, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}).then(function (client) {
	const db = client.db('application-filters');

	return db.collection('application-filters').find({
		'TenantId': tenant,
		"ApplicationId":{$ne:null}
	}).count().then(function (count) {
		let currentPos = 1;
		//let lastPage = Math.ceil(1000 / limit);
		let lastPage = Math.ceil(count / limit) //uncomment if needed to run for all the records
		console.log("Count is " + count);
		var pages=[];
		for (var i = 1; i <= lastPage; i++) {
			currentPos = i;
			var skip = limit * (currentPos - 1);
			pages.push(skip);
		}

		pages=pages.map(r => () => {
			console.log("Skipping:"+ r);
			return db.collection('application-filters')
				.find({
					'TenantId': tenant,
					"ApplicationId":{$ne:null}
				}, {
					ApplicationId: 1
				})
				.sort({
					_id: -1
				})
				.skip(r)
				.limit(limit)
				.toArray().then(function(applications){
					applications = applications.map(r => () => {
						let payload = JSON.stringify({
							"TemporaryApplicationNumber": r.ApplicationId
						});

						return fetch(eventHubUrl+"/DummyApplicationFilter", {
								'method': 'POST',
								'headers': {
									'content-type': 'application/json',
									'authorization': 'Bearer ' + token
								},
								'body': payload
							})
							.then(response => {
								console.log('Executed loan -> ' + r.ApplicationId + 'payload ' + payload + ' Response Code : ' + response.status);
							})
							.catch(err => console.error(err));
					});

					return applications.reduce((a, c) => {
						return a.then(_ => {
							return c();
						}).catch(reason => {
							//TODO: Check if fail, it will publish others
							console.error('[Error] Publish Event failed: ', reason);
							return c();
						});
					}, new Promise((resolve) => resolve({}))).then(function () {
						
					});
				})
				.catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Mongo: ', reason);
					return c();
				});
		});

		return pages.reduce((a, c) => {
			return a.then(_ => {
				return c();
			}).catch(reason => {
				//TODO: Check if fail, it will publish others
				console.error('[Error] getting data Event failed: ', reason);
				return c();
			});
		}, new Promise((resolve) => resolve({}))).then(function () {
			client.close();
		});

	})
	.catch(reason => {
		//TODO: Check if fail, it will publish others
		console.error('[Error] catch failed: ', reason);
		return c();
	});
}).catch(function(){
	console.log("Error ocurred");
});