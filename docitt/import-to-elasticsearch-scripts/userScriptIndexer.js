var MongoClient = require('mongodb').MongoClient;
var fetch = require('node-fetch')
var url = 'mongodb://docitt:docSigma@10.100.0.11:27017/identity?authSource=admin';
const tenant = 'oc';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJkb2NpdHQiLCJpYXQiOjE1ODY0MDUxMjIsImV4cCI6MTU4NjQxOTUyMiwic3ViIjoic3dhcmFqXzAxQHNpZ21haW5mby5uZXQiLCJ0ZW5hbnQiOiJvYyIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIiwiYWRtaW4tcG9ydGFsIl0sIklzVmFsaWQiOnRydWV9.wIf7Qa8njY9c8v-CCOoBdP8my2V6vsgWigWRHsIfUWM';
const eventHubUrl="http://docitt.dev.lendfoundry.com:5004";

const limit = 5;
const sleep = 1000;
var self = this;
MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('identity');
    if(!err) console.log("We are connected");
    
    db.collection('users').find({'TenantId':tenant}).count(function(err, count){
		let currentPos = 1;
		//let lastPage = Math.ceil(1000 / limit); //uncomment to run only for 1000 users
		let lastPage = Math.ceil(count / limit) //uncomment if needed to run for all the records

		for (var i=1; i <= lastPage;i++) {
			currentPos = i;
			var skip = limit * (currentPos - 1);

			db.collection('users')
		    .find({'TenantId':tenant},{Username:1})
			.skip(skip)
			.limit(limit)
		    .toArray(function(err, docs) {
		        docs.forEach(function(doc) {
					//setTimeout(function() {
						console.log("-==->" + doc.Username)
							let payload = JSON.stringify({
								"UserProfile" : {
									"Username" : doc.Username
								 }
							})

							fetch(eventHubUrl+"/dummyUserIndexer", {
								'method': 'POST',
								'headers': {
					            	'content-type': 'application/json',
									'authorization': 'Bearer '+token
								},
								'body': payload
							})
							.then(resposne => {console.log('Resposne Code : '+resposne.status)})
							.catch(err => console.error(err))

					//}, sleep);
		        });
		        client.close();
		    });
		}
	});
});
