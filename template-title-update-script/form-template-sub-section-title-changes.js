var templates =    [
            {
                "templateFieldName": "spouseInfo",
                "templateTitle": "Spouse Information"
            },
            {
                "templateFieldName": "spouseCommunicationInfo",
                "templateTitle": "Spouse Communication Information"
            },
            {
                "templateFieldName": "additionalResidence",
                "templateTitle": "Additional Residence"
            },
            {
                "templateFieldName": "spouseAdditionalResidence",
                "templateTitle": "Spouse Additional Residence"
            },
            {
                "templateFieldName": "coborrowerAdditionalResidence",
                "templateTitle": "CoBorrower Additional Residence"
            },
            {
                "templateFieldName": "coBorrowerSpouseAdditionalResidence",
                "templateTitle": "CoBorrower Spouse Additional Residence"
            },
            {
                "templateFieldName": "coBorrowerInfo",
                "templateTitle": "CoBorrower Information"
            },
            {
                "templateFieldName": "coBorrowerCommunicationInfo",
                "templateTitle": "CoBorrower Communication Information"
            },
            {
                "templateFieldName": "spouseResidenceDetail",
                "templateTitle": "Spouse Residence Detail"
            },
            {
                "templateFieldName": "coborrowerResidenceDetail",
                "templateTitle": "CoBorrower Residence Detail"
            },
            {
                "templateFieldName": "coBorrowerSpouseResidenceDetail",
                "templateTitle": "CoBorrower Spouse Residence Detail"
            },
            {
                "templateFieldName": "borrowerREOInfo",
                "templateTitle": "Borrower REO Information"
            },
            {
                "templateFieldName": "spouseREOAdditionalInfo",
                "templateTitle": "Spouse REO Additional Information"
            },
            {
                "templateFieldName": "spouseREOInfo",
                "templateTitle": "Spouse REO Information"
            },
            {
                "templateFieldName": "coBorrowerREOInfo",
                "templateTitle": "CoBorrower REO Information"
            },
            {
                "templateFieldName": "coboSpouseREOInfo",
                "templateTitle": "CoBorrower Spouse REO Information"
            },
            {
                "templateFieldName": "coboSpouseREOAdditionalInfo",
                "templateTitle": "CoBorrower Spouse REO Additional Information"
            },
            {
                "templateFieldName": "coBorrowerInvitationInfo",
                "templateTitle": "CoBorrower Invitation Information"
            },
            {
                "templateFieldName": "coBorrowerMaritalStatusInfo",
                "templateTitle": "CoBorrower Marital Status Information"
            },
            {
                "templateFieldName": "coBorrowerSpouseCommunicationInfo",
                "templateTitle": "CoBorrower Spouse Communication Information"
            },
            {
                "templateFieldName": "coBorrowerInvitationQuestions",
                "templateTitle": "CoBorrower Invitation Questions"
            },
            {
                "templateFieldName": "coBorrowerAssets",
                "templateTitle": "CoBorrower Assets"
            },
            {
                "templateFieldName": "currentEmployment",
                "templateTitle": "Current Employment"
            },
            {
                "templateFieldName": "employment",
                "templateTitle": "Employment"
            },
            {
                "templateFieldName": "currentEmploymentContact",
                "templateTitle": "Current Employment Contact"
            },
            {
                "templateFieldName": "employmentContact",
                "templateTitle": "Employment Contact"
            },
            {
                "templateFieldName": "alimony",
                "templateTitle": "Alimony"
            },
             {
                "templateFieldName": "alimonySupport",
                "templateTitle": "Alimony Support"
            },
            {
                "templateFieldName": "business",
                "templateTitle": "Business"
            },
            {
                "templateFieldName": "businessAnother",
                "templateTitle": "Additional Business"
            },
            {
                "templateFieldName": "rentalIncome",
                "templateTitle": "Rental Income"
            },
            {
                "templateFieldName": "militaryPay",
                "templateTitle": "Military Pay"
            },
            {
                "templateFieldName": "socialSecurity",
                "templateTitle": "Social Security"
            },
            {
                "templateFieldName": "otherIncome",
                "templateTitle": "Other Income"
            },
            {
                "templateFieldName": "interestDividend",
                "templateTitle": "Interest Dividend"
            },
            {
                "templateFieldName": "incomeSearch",
                "templateTitle": "Search Income"
            },
             {
                "templateFieldName": "coBorrowerIncome",
                "templateTitle": "CoBorrower Income"
            },
             {
                "templateFieldName": "_business",
                "templateTitle": "CoBorrower Business"
            },
            {
                "templateFieldName": "_rentalIncome",
                "templateTitle": "CoBorrower Rental Income"
            },
            {
                "templateFieldName": "_socialSecurity",
                "templateTitle": "CoBorrower Social Security"
            },
            {
                "templateFieldName": "_otherIncome",
                "templateTitle": "CoBorrower Other Income"
            },
            {
                "templateFieldName": "_interestDividend",
                "templateTitle": "CoBorrower Interest Dividend"
            },
            {
                "templateFieldName": "childSupportQuestions",
                "templateTitle": "Child Support Questions"
            },
            {
                "templateFieldName": "declaration",
                "templateTitle": "Declaration"
            },
            {
                "templateFieldName": "summaryPersonalInfo",
                "templateTitle": "Summary Personal Information"
            }
            
        ]            
var cursor = db.getCollection('form-sub-sections')
               .find({ 
                   'TenantId':{$in: ['oc','naf']}     
               })               
               .sort({_id:-1})              
 var requests = [];
 
while(cursor.hasNext()) 
{
       var formTemplate = cursor.next();
     var formTemplateTitleDataUpdate = templates.find(function(p)
    {
        var formTemplateFieldName =  formTemplate.TemplateUniqueKey;   
      
       return p.templateFieldName == formTemplateFieldName;
    });  
    if(formTemplateTitleDataUpdate!=null){
         
            fieldUpdate = "{\"TemplateTitle\":\"" + formTemplateTitleDataUpdate.templateTitle + "\"}" 
        }
    
  
         var obj={ 
                        'updateOne': {
                            'filter': { '_id': formTemplate._id },
                            'update': { '$set': "#" }
                        }
                    }
                    
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                                  
                   requests.push(obj);
                  
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('form-sub-sections').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
              
                     
}    

if(requests.length > 0) {
    var response=db.getCollection('form-sub-sections').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}