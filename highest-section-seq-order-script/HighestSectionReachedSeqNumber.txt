
var cursor = db.getCollection('questionnaire')
               .find({ 
                   'TenantId':{$in: ['naf','cm']}
                    })                                     
               .sort({_id:-1})               
 var requests = [];
while(cursor.hasNext()) 
{
       var questionnaire = cursor.next();
    var userstateIndex = -1
    
    var userStates = [];            
    questionnaire.UserState.forEach(function(userstate)
   {
       userstateIndex = userstateIndex +1;  
       if(!userstate.HighestSectionReachedSeqNumber){
           userstate.HighestSectionReachedSeqNumber = userstate.HighestSectionReached == 6 ? userstate.HighestSectionReached - 1 : userstate.HighestSectionReached
       }      
       userStates.push(userstate); 
   });
   if(!questionnaire.HighestSectionReachedSeqNumber){
      var questionnaireHighestSectionReached= questionnaire.HighestSectionReached == 6 ? questionnaire.HighestSectionReached - 1 : questionnaire.HighestSectionReached
       var fieldUpdate = "{";
   var fieldUpdate = fieldUpdate + "\"HighestSectionReachedSeqNumber\":" + questionnaireHighestSectionReached
    fieldUpdate= fieldUpdate + ",\"UserState\":" + JSON.stringify(userStates)
       fieldUpdate = fieldUpdate + "}";
    print(fieldUpdate);
    
                    var obj={ 
                        'updateOne': {
                            'filter': { '_id': questionnaire._id },
                            'update': { '$set': "#" }
                        }
                    }
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                                       
                    requests.push(obj);
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('questionnaire').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
                }
}    
if(requests.length > 0) {
    var response=db.getCollection('questionnaire').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}
