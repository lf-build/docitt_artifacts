var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('questionnaires');
    if(!err) console.log("We are connected");
    
	db.collection('questionnaire')
		.find({
				'TemporaryApplicationNumber': { $in:  data.applicationNumbers }, 
				'TenantId':'naf',
				'CreatedDate.Time.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
			//console.log( doc);

			var questionnaireApplicationCreated = {
				"TenantId" : "naf",
				"EntityId" :  doc.TemporaryApplicationNumber,
				"Id": doc._id,
				"Name": "QuestionnaireApplicationCreated",
				"Description" : "New Temporary Application number '" + doc.TemporaryApplicationNumber + "' has been created by " + doc.UserName,
				"Title" : "Temporary Application Created",
				"Data":{
					"TemporaryApplicationNumber" : doc.TemporaryApplicationNumber,
					"AssignedLoanOfficerEmailId" : doc.AssignedLoanOfficerUserName,
					"Username" : doc.UserName,
					"Date" : doc.CreatedDate.Time.DateTime
				}
			};

			console.log("Publishing to Hub " + doc.TemporaryApplicationNumber);
			console.log(JSON.stringify(questionnaireApplicationCreated));
			configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(questionnaireApplicationCreated)});
			//hub.publish(doc.TemporaryApplicationNumber, JSON.stringify(questionnaireApplicationCreated));
			
			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
