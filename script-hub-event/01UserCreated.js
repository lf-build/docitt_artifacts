var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;

var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('identity');
	if(!err) console.log("We are connected");

	db.collection('users')
		.find({
				'Username': { $in:  data.users }, 
				'TenantId':'naf',
				'Portal' : 'borrower',
				'CreatedOn.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.sort({'CreatedOn.DateTime': -1})
		.toArray(function(err, docs) {
			docs.forEach(function(doc){
				var UserCreatedData = {
					"TenantId" : "naf",
					"EntityId" :  doc.Username,
					"Id": doc._id,
					"Name": "UserCreated",
					"Description" : "User created with " + doc.Username,
					"Data":{
						"Username" : doc.Username,
						"Role" : doc.Roles[0],
						"Title" : "User Created",
						"Date" : doc.CreatedOn.DateTime
					}
				};
			
				console.log(doc.Username);
				console.log(JSON.stringify(UserCreatedData));
				configToProcess.push({name : doc.Username, data: JSON.stringify(UserCreatedData)});
				//hub.publish(doc.Username, JSON.stringify(UserCreatedData));
			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
