var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {
	useNewUrlParser: true,
	useUnifiedTopology: true
}, function (err, client) {
	const db = client.db('documents');
	const db1 = client.db('questionnaires');
	if (!err) console.log("We are connected");
	var users=[];
	db1.collection("questionnaire").find({
		"TemporaryApplicationNumber": {
			$in: data.applicationNumbers
		}
	}).toArray().then(function (docs) {
		docs.forEach(function (doc) {
			users[doc.TemporaryApplicationNumber]=doc.UserName;
		});
		console.dir(users);
		db.collection('document-manager')
			.find({
				'EntityId': {
					$in: data.applicationNumbers
				},
				'TenantId': 'naf',
				'Timestamp.DateTime': {
					$gt: new Date("2019-08-23T23:00:00Z"),
					$lt: new Date("2019-08-26T21:00:00Z")
				}
			})
			.toArray(function (err, docs) {
				docs.forEach(function (doc) {
					var DocumentAdded = {
						"TenantId": "naf",
						"EntityId": doc.EntityId,
						"Username": users[doc.EntityId],
						"Id": doc._id,
						"Data": {
							"Document": {
								"Id": doc.Id,
								"FileName": doc.FileName,
								"Size": doc.Size,
								"Tags": doc.Tags,
								"Timestamp": doc.Timestamp.DateTime
							},
						},
						"Name": "DocumentAdded",
						"Description": "Document '" + doc.FileName + "' has been added by '" + users[doc.EntityId] + "'"
					};

					console.log(doc.EntityId);
					console.log(JSON.stringify(DocumentAdded));
					configToProcess.push({
						name: doc._id,
						data: JSON.stringify(DocumentAdded)
					});
				});

				client.close();

				configToProcess = configToProcess.map(r => () =>
					hub.publish(r.name, r.data));

				return configToProcess.reduce((a, c) => {
					return a.then(_ => {
						return c();
					}).catch(reason => {
						//TODO: Check if fail, it will publish others
						console.error('[Error] Configuration failed: ', reason);
						return c();
					});
				}, new Promise((resolve) => resolve({})));

				//hub.publish(doc.EntityId, JSON.stringify(DocumentAdded));

			});

	})

});