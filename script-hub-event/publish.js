const data = require('./data');
let fetch = require('node-fetch');

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  
var publish = function(key, bodyData){
    return fetch(data.hub_url, 
        {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: bodyData
        }).then(response => {
            console.log("Completed - " + key);
        }).catch(err => {
            console.log("!!!!FAILED!!!!" + key);
            console.log(err);
        }).then(function(){
            return sleep(1000);
        });
};

exports.publish = publish;


