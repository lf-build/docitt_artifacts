var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('email');
    if(!err) console.log("We are connected");
    
	db.collection('emails')
		.find({
				//'EntityId': { $in:  data.applicationNumbers }, 
                'TenantId':'naf',
                'Subject' :'Docitt Reminder',
				'SentOnDate.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
	
                var ApplicationReminderNotification = {
                    "TenantId" : "naf",
					"Data":{
                        "EntityId" : doc.EntityId,
                        "BorrowerFirstName" : doc.BorrowerFirstName,
                        "BorrowerLastName" : doc.BorrowerLastName,
                        "Email" : doc.Email,
                        "BorrowerPortalUrl" : doc.BorrowerPortalUrl,
                        "LoanOfficerName" : doc.LoanOfficerName,
                        "LoanOfficerNMLS" : doc.LoanOfficerNMLS,
                        "LoanOfficerEmail" : doc.LoanOfficerEmail,
                        "LoanOfficerPhoneNumber" : doc.LoanOfficerPhoneNumber,
                        "InitiatedByName" : doc.InitiatedByName,
                        "InitiatedByPhoneNumber" : doc.InitiatedByPhoneNumber,
                        "InitiatedByEmail" : doc.InitiatedByEmail,
                        "Date" : doc.SentOnDate.DateTime,
						 },
                         "Name": "ApplicationReminderNotificationed",
                    "Description": "ApplicationReminderNotification",
                    "Title": "Application Reminder Notification"

				};

			console.log(doc.TemporaryApplicationNumber);
			console.log(JSON.stringify(ApplicationReminderNotification));
			configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(ApplicationReminderNotification)});
			//hub.publish(doc.EntityId, JSON.stringify(ApplicationReminderNotification));
			

			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
