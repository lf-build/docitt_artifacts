var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('assignment');
    if(!err) console.log("We are connected");
    
	db.collection('assignments')
		.find({
				'EntityId': { $in:  data.applicationNumbers }, 
				'EntityType': 'application',
				'TenantId':'naf',
				'AssignedOn.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
			console.log(doc.EntityId);

			var applicationAssignmentAssigned = {
				"TenantId" : "naf",
				"EntityId" :  doc.EntityId,
				"EntityType": "application",
				"Id": doc._id,
				"Name": "ApplicationAssignmentAssigned",
				"Description" : "Application Assignment Assigned",
				"Title" : "Application Assignment Assigned",
				"Data":{
					"Assignment" : {
						"Assignee": doc.Assignee,
						"Role": doc.Role,
						"AssignedOn": doc.AssignedOn.DateTime,
						"Team": doc.Team,
						"Date": doc.AssignedOn.DateTime
					}
				}
			};

			console.log("Publishing to Hub" + doc.EntityId);
			console.log(JSON.stringify(applicationAssignmentAssigned));
			configToProcess.push({name : doc.EntityId, data: JSON.stringify(applicationAssignmentAssigned)});
			//hub.publish(doc.EntityId, JSON.stringify(applicationAssignmentAssigned));
			

			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
