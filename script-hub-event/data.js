const usersData = require('./usersData');
const applicationData = require('./applicationData');
const conditionData = require('./conditionData');

//var connectionString = 'mongodb://docitt:docSigma@10.100.0.11:27017/admin?authSource=admin';
var connectionString = 'mongodb://root:MongoProd123@34.219.40.247:30830/admin?authSource=admin';
//var applicationNumbers = ["DOC023135", "DOC023134", "DOC023129","DOC023138"];
//var users = ["ritesh.pk@lendfoundry.com", "ritesh.pk+0826.01@lendfoundry.com"];

//var hub_url = "https://hub.docitt.net/api/v1/notify";
var hub_url = "https://encdsj7asqo0u.x.pipedream.net/";


var sleep = function (ms) {
    console.log("..waiting");
    return new Promise(resolve => setTimeout(resolve, ms));
  };

exports.applicationNumbers = applicationData.applicationNumbers;
exports.users = usersData.users;
exports.conditions = conditionData.conditions;
exports.connectionString = connectionString;
exports.hub_url = hub_url;
exports.sleep = sleep;