var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('assignment');
    if(!err) console.log("We are connected");
    
	db.collection('invites')
		.find({
				//'EntityId': { $in:  data.applicationNumbers }, 
				'TenantId':'naf',
				'InvitationDate.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
	
                var InvitationSend = {
					"TenantId" : "naf",
					"EntityId" :  doc.Username,
					"Id": doc._id,
					"Data":{
                        "Invitation": {
                            "InvitedBy": doc.InvitedBy,
                            "InviteEmail": doc.InviteEmail,
                            "InviteFirstName": doc.InviteFirstName,
                            "InviteLastName": doc.InviteLastName,
                            "InviteMobileNumber": doc.InviteMobileNumber,
                            "InvitationDate": doc.InvitationDate.DateTime,
                            "Role": doc.Roles[0],
                            "Team": doc.Team,
                            "InvitationUrl": doc.InvitationUrl,
                            "InvitationToken": doc.InvitationToken,
                            "InvitationTokenExpiry": doc.InvitationTokenExpiry
                        }
						
                    },
                    "Name": "InvitationSend",
                    "Description": "Invite generated for '"+doc.InviteEmail+"' by '"+doc.InvitedBy+"'",
                    "Title": "Invitation Created"

				};

			console.log(doc.TemporaryApplicationNumber);
			console.log(JSON.stringify(InvitationSend));
			configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(InvitationSend)});
			//hub.publish(doc.EntityId, JSON.stringify(InvitationSend));
			

			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
