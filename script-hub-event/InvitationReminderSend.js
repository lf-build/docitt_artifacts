var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('eventmanagement');
    if(!err) console.log("We are connected");
    
	db.collection('eventmanagement')
		.find({
				
				'TenantId':'naf',
				'CreatedDate.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
	
                var InvitationReminderSend = {
					"TenantId" : "naf",
					"EntityType" : "application",
                    "EntityId": doc.TemporaryApplicationNumber,
                    "UserName" : doc.UserName,
					"Data":{
						"Date" : doc.CreatedDate.DateTime
                    },
                    "Name": "InvitationReminderSend",
                    "Description": "Invitation Send to '"+doc.UserName+"'",

				};

			console.log(doc.TemporaryApplicationNumber);
			console.log(JSON.stringify(InvitationReminderSend));
			configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(InvitationReminderSend)});
			//hub.publish(doc.EntityId, JSON.stringify(InvitationReminderSend));
			

			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
