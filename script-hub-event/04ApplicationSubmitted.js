var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('questionnaires');
    if(!err) console.log("We are connected");
    
	db.collection('questionnaire')
		.find({
				'TemporaryApplicationNumber': { $in:  data.applicationNumbers }, 
				'Status' : { $ne: 1 },
				'TenantId':'naf'
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
			//console.log( doc);

				var users = doc.UserState;

				users.forEach(function(user){
					//console.log(users);

					if(user.Status != 1 && user.UserName == user.UserEmailId && user.ApplicantType == "Borrower")
					{
						var questionnaireApplicationCreated = {
							"TenantId" : "naf",
							"EntityId" :  doc.TemporaryApplicationNumber,
							"Id": doc._id,
							"Name": "ApplicationSubmitted",
							"Description" : "Application Number '" + doc.TemporaryApplicationNumber + "' is submitted by " + user.UserName,
							"Title" : "Application Submitted",
							"Data":{
								"TemporaryApplicationNumber" : doc.TemporaryApplicationNumber,
								"Username" : user.UserName,
								"Date" : doc.CreatedDate.Time.DateTime
							}
						};
			
						console.log("Publishing to Hub " + doc.TemporaryApplicationNumber);
						console.log(JSON.stringify(questionnaireApplicationCreated));
						configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(questionnaireApplicationCreated)});
						//hub.publish(doc.TemporaryApplicationNumber, JSON.stringify(questionnaireApplicationCreated));
					}
				});
			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
