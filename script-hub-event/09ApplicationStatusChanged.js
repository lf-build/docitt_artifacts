var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('event-store');
    if(!err) console.log("We are connected");
    
	db.collection('event-store')
		.find({
				'EntityId': { $in:  data.applicationNumbers }, 
				'TenantId':'naf',
				'ActiveOn.Time': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
	
                var ApplicationStatusChanged = {
					"TenantId" : "naf",
					"EntityId" :  doc.TemporaryApplicationNumber,
					"Id": doc._id,
					"Name": "ApplicationStatusChanged",
					"Description" : "Application  '" + doc.TemporaryApplicationNumber + "' status is updated to '"+ doc.NewStatusName +"'",
					"Data":{
						"OldStatus" : doc.OldStatus,
                        "NewStatusName" : doc.NewStatusName,
                        "Reason": null,
						"Date" : doc.ActiveOn.Time
					}
				};

			console.log("Publishing to Hub" + doc.TemporaryApplicationNumber);
			console.log(JSON.stringify(ApplicationStatusChanged));
			configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(ApplicationStatusChanged)});
			//hub.publish(doc.EntityId, JSON.stringify(ApplicationStatusChanged));
			

			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
