var MongoClient = require('mongodb').MongoClient;
const data = require('./data');
const hub = require('./publish');
var connectionString = data.connectionString;
var configToProcess = [];

MongoClient.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, client) {
    const  db = client.db('identity');
    if(!err) console.log("We are connected");
    
	db.collection('users')
		.find({
				
				'TenantId':'naf',
				'LastPasswordModifyOn.DateTime': {$gt: new Date("2019-08-23T23:00:00Z") , $lt: new Date("2019-08-26T21:00:00Z")}
		})
		.toArray(function(err, docs) {
			docs.forEach(function(doc) {
	
                var UserPasswordResetRequested = {
					"TenantId" : "naf",
                    "EntityId": doc.UserName,
					"Data":{
                        "UserName" : doc.UserName,
                        "Token" : doc.Token,
                        "ResetUrl" : doc.ResetUrl,
                        "Date" : doc.LastPasswordModifyOn.DateTime
                    },
                    "Name": "UserPasswordResetRequested",
                    "Description": "Password reset token generated for '"+doc.UserName+"'",
                    "Title": "User Password Reset Requested"
				};

			console.log(doc.TemporaryApplicationNumber);
			console.log(JSON.stringify(UserPasswordResetRequested));
			configToProcess.push({name : doc.TemporaryApplicationNumber, data: JSON.stringify(UserPasswordResetRequested)});
			//hub.publish(doc.EntityId, JSON.stringify(UserPasswordResetRequested));
			

			});
			client.close();

			configToProcess = configToProcess.map(r => () => 
			         hub.publish(r.name, r.data));

			return configToProcess.reduce((a, c) => {
				return a.then(_ => {
					return c();
				}).catch(reason => {
					//TODO: Check if fail, it will publish others
					console.error('[Error] Configuration failed: ', reason);
					return c();
				});
			}, new Promise((resolve) => resolve({})));
		});
}); 
