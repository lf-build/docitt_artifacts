var questionFieldName = "LoanMaturityPeriodCount";

var cursor = db.getCollection('question-sections')
               .find({
                   'TenantId':{$in: ['cm']},
                   "QuestionList": {$elemMatch: {"QuestionFieldName": questionFieldName }}
                   })                  
               .sort({_id:-1});
 var requests = [];
while(cursor.hasNext())
{
       var questionSection = cursor.next();
    var questionIndex = -1
    questionSection.QuestionList.forEach(function(question)
   {
       questionIndex = questionIndex +1;
       if(question.QuestionFieldName == questionFieldName){
           var isAlreadyAdded = false;
           var newlyAddedEnums =[];
newlyAddedEnums.push({
                    "Answer" : "60",
                    "Icon" : "60",
                    "SeqNo" :NumberInt(5),
                    "EnumFieldName" : 'Enum' + questionFieldName + '60',
                    "Label" : "7-Year",
                    "Value" : "60"       
                 });
newlyAddedEnums.push({
                    "Answer" : "30",
                    "Icon" : "30",
                    "SeqNo" :NumberInt(6),
                    "EnumFieldName" : 'Enum' + questionFieldName + '30',
                    "Label" : "5-Year",
                    "Value" : "30"       
                 });

             var availableAnsData = [];
             var AvailableAnsListData =[];           
                 question.AvailableAns.forEach(function(availableAns){
                     if(availableAns.EnumFieldName == 'Enum' + questionFieldName + '60')
                     {
                         isAlreadyAdded = true;
                     }
                     if(availableAns.EnumFieldName == 'Enum' + questionFieldName + '30')
                     {
                         isAlreadyAdded = true;
                     }                     
                    
                     var availableAnsSeqNo = availableAns.SeqNo ;
                    
                    availableAnsData.push({
         "_t" : "Docitt.Questionnaire.AvailableAns, Docitt.Questionnaire.Abstractions",
                    "Answer" : availableAns.Answer,
                    "Icon" : availableAns.Icon,
                    "SeqNo" :  NumberInt(availableAnsSeqNo),
                    "EnumFieldName" : availableAns.EnumFieldName,
                    "Label" : availableAns.Label,
                    "Value" : availableAns.Value,
                    "Key" : null,
                    "Type" : null,
                    "Childs" : null
                 });
                 AvailableAnsListData.push("\"" + availableAns.Answer.toString() + "\"");
                     });
                     if(!isAlreadyAdded && newlyAddedEnums.length>0){
                  newlyAddedEnums.forEach(function(newlyAddedEnum){
                      availableAnsData.push({
                  "_t" : "Docitt.Questionnaire.AvailableAns, Docitt.Questionnaire.Abstractions",
                    "Answer" : newlyAddedEnum.Answer,
                    "Icon" : newlyAddedEnum.Icon,
                    "SeqNo" :newlyAddedEnum.SeqNo,
                    "EnumFieldName" : newlyAddedEnum.EnumFieldName,
                    "Label" : newlyAddedEnum.Label,
                    "Value" : newlyAddedEnum.Value,
                    "Key" : null,
                    "Type" : null,
                    "Childs" : null
                 });
                 AvailableAnsListData.push("\"" + newlyAddedEnum.Answer.toString() + "\"");
                  });
             }
           
var AvailableAnsDataJson = JSON.stringify(availableAnsData);
var fieldUpdate="{\"QuestionList." + questionIndex +".AvailableAns\":" + AvailableAnsDataJson + ",\"QuestionList." + questionIndex + ".AvailableAnsList\":[" + AvailableAnsListData + "]}"
//                     var fieldUpdate="{\"QuestionList." + questionIndex +".AvailableAns\":" + AvailableAnsDataJson + "}"
                    
                 // print(AvailableAnsListData);
                    var obj={
                        'updateOne': {
                            'filter': { '_id': questionSection._id },
                            'update': { '$set': "#" }
                        }
                    }
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                    requests.push(obj);
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('question-sections').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
                 }
   });
}
          if(requests.length > 0) {
    var response=db.getCollection('question-sections').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}