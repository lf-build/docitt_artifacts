var questionFieldName = "NoteRatePercent";

var cursor = db.getCollection('question-sections')
               .find({ 
                   'TenantId':{$in: ['naf','cm']},                        
                   "QuestionList": {$elemMatch: {"QuestionFieldName": questionFieldName }}
                   })          
               .sort({_id:-1});  
 var requests = []; 
             
while(cursor.hasNext()) 
{    
       var questionSection = cursor.next();
    var questionIndex = -1
    questionSection.QuestionList.forEach(function(question)
   {
       questionIndex = questionIndex +1;     
       if(question.QuestionFieldName == questionFieldName){ 
           
             var questionType = 32
             var availableAnsData = null;
             var AvailableAnsListData = null;

                  
var fieldUpdate="{\"QuestionList." + questionIndex +".AvailableAns\":" + availableAnsData + ",\"QuestionList." + questionIndex + ".AvailableAnsList\":" + AvailableAnsListData + ",\"QuestionList." + questionIndex + ".QuestionType\":" + questionType + "}"
              
                    var obj={ 
                        'updateOne': {
                            'filter': { '_id': questionSection._id },
                            'update': { '$set': "#" }
                        }
                    }
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                                       
                    requests.push(obj);
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('question-sections').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
                 }
   });
}    

           if(requests.length > 0) {
    var response=db.getCollection('question-sections').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}
