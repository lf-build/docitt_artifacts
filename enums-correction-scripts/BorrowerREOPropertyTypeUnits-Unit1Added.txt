var cursor = db.getCollection('question-sections')
               .find({ 
                   'TenantId':{$nin: ['oc']},
                   'SectionId':1, 
                   "QuestionSectionName":  "property type",
                   "QuestionList.1.QuestionFieldName": "BorrowerREOPropertyTypeUnits" })                   
               .sort({_id:-1})
var isUnitsOneIsExists = false;
 var requests = [];
while(cursor.hasNext()) 
{
       var questionSection = cursor.next();
    var questionIndex = -1
    questionSection.QuestionList.forEach(function(question)
   {
       questionIndex = questionIndex +1;
       if(question.QuestionFieldName == "BorrowerREOPropertyTypeUnits"){
             var availableAnsData = [];
             var AvailableAnsListData =[];
             availableAnsData.push({
                  "_t" : "Docitt.Questionnaire.AvailableAns, Docitt.Questionnaire.Abstractions",
                    "Answer" : "1",
                    "Icon" : "1",
                    "SeqNo" :NumberInt(1),
                    "EnumFieldName" : 'Enum' + question.QuestionFieldName + '1',
                    "Label" : "1",
                    "Value" : "1",
                    "Key" : null,
                    "Type" : null,
                    "Childs" : null                
                 });
                
                 AvailableAnsListData.push("\"1\"");
                 question.AvailableAns.forEach(function(availableAns){
                     if(availableAns.EnumFieldName == 'Enum' + question.QuestionFieldName + '1')
                     {
                         availableAnsData.pop(availableAns.EnumFieldName);
                         AvailableAnsListData.pop(0);
                         isUnitsOneIsExists = true;
                     }
                  var availableAnsSeqNo = (isUnitsOneIsExists)? availableAns.SeqNo :availableAns.SeqNo + 1;
                    availableAnsData.push({ 
         "_t" : "Docitt.Questionnaire.AvailableAns, Docitt.Questionnaire.Abstractions",                
                    "Answer" : availableAns.Answer,
                    "Icon" : availableAns.Icon,
                    "SeqNo" :  NumberInt(availableAnsSeqNo),
                    "EnumFieldName" : availableAns.EnumFieldName,
                    "Label" : availableAns.Label,
                    "Value" : availableAns.Value,
                    "Key" : null,
                    "Type" : null,
                    "Childs" : null                
                 });                 
                 AvailableAnsListData.push("\"" + availableAns.Answer.toString() + "\"");
                     });
                    
var AvailableAnsDataJson = JSON.stringify(availableAnsData);
                                 
var fieldUpdate="{\"QuestionList." + questionIndex +".AvailableAns\":" + AvailableAnsDataJson + ",\"QuestionList." + questionIndex + ".AvailableAnsList\":[" + AvailableAnsListData + "]}"
                     
//                     var fieldUpdate="{\"QuestionList." + questionIndex +".AvailableAns\":" + AvailableAnsDataJson + "}"
                    print(fieldUpdate);
                 // print(AvailableAnsListData);
                   
                    var obj={ 
                        'updateOne': {
                            'filter': { '_id': questionSection._id },
                            'update': { '$set': "#" }
                        }
                    }
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                                       
                    requests.push(obj);
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('question-sections').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
                 }
   });
}    
           if(requests.length > 0) {
    var response=db.getCollection('question-sections').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}
