var keys=[];
var listOfFields= {
    "EnumPropertyUsePrimary": "EnumPropertyUsePrimary",
    "EnumPropertyUseSecondary":   "EnumPropertyUseSecondHome",
    "EnumPropertyUseInvestment" : "EnumPropertyUseInvestmentProperty",
    "EnumPropertyUseFHASecondaryResidence" : "EnumPropertyUseFHASecondaryResidence"
}

for(var index in listOfFields) {
        keys.push(index);
}

var cursor = db.getCollection('question-sections')
               .find({ 
                   'SectionId':1, 
                   "QuestionSectionName": "Property Use",
                   "QuestionList.0.QuestionFieldName": "BorrowerPropertyUse" })
               .sort({_id:-1})

var requests = [];
while(cursor.hasNext()) 
{
   var questionSection = cursor.next();
   for(var questionIndex in questionSection.QuestionList) 
   {
        for(var availableAnsIndex in questionSection.QuestionList[questionIndex].AvailableAns)
        {
                var existingEnumField=questionSection.QuestionList[questionIndex].AvailableAns[availableAnsIndex].EnumFieldName
                var newEnumField=listOfFields[existingEnumField]
                if(keys.includes(existingEnumField))
                {
                    print("ApplicationId : " + questionSection.ApplicationNumber);
                    //print(questionSection.QuestionList[questionIndex].AvailableAns[availableAnsIndex])                    
                    var fieldUpdate="{\"QuestionList."+questionIndex+".AvailableAns."+availableAnsIndex+".EnumFieldName\":\""+newEnumField + "\"}"
                    print(fieldUpdate);
                    var obj={ 
                        'updateOne': {
                            'filter': { '_id': questionSection._id },
                            'update': { '$set': "#" }
                        }
                    }
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                                       
                    requests.push(obj);
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('question-sections').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
                }
        }
   }
}
if(requests.length > 0) {
    var response=db.getCollection('question-sections').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}
