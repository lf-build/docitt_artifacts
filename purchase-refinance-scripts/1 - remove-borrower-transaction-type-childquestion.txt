var cursor = db.getCollection('question-sections')
               .find({ 
                   'TenantId':{$nin: ['oc']},
                   'SectionId':1, 
                   "QuestionSectionName":  "purpose",
                   "QuestionList.0.QuestionFieldName": "BorrowerTransactionType" })                                     
               .sort({_id:-1})
 var requests = [];
while(cursor.hasNext()) 
{
       var questionSection = cursor.next();
    var questionIndex = -1
    questionSection.QuestionList.forEach(function(question)
   {
       questionIndex = questionIndex +1;
       if(question.QuestionFieldName == "BorrowerTransactionType"){
             var childQuestions = [];            
            
                 question.ChildQuestions.forEach(function(childQuestion){                    
                    if((childQuestion.QuestionId!="38" )  )
                        {                                                              
                    childQuestions.push({                         
                    "Answer" : childQuestion.Answer,
                    "QuestionId" : childQuestion.QuestionId,
                    "SubSectionId" :  childQuestion.SubSectionId,
                    "SectionId" : childQuestion.SectionId,
                    "QuestionSectionSeqNo" : childQuestion.QuestionSectionSeqNo
                     });                
                 }     
                 });                
                 if(childQuestions.length == 0)
                     return;
var childQuestionsJson = JSON.stringify(childQuestions);
                                 
var fieldUpdate="{\"QuestionList." + questionIndex +".ChildQuestions\":" + childQuestionsJson + "}"

                    var obj={ 
                        'updateOne': {
                            'filter': { '_id': questionSection._id },
                            'update': { '$set': "#" }
                        }
                    }
                    obj['updateOne']['update']['$set']= JSON.parse(fieldUpdate);
                                       
                    requests.push(obj);
                    if (requests.length === 500) {
                        //Execute per 500 operations and re-init
                        try   {
                            var response=db.getCollection('question-sections').bulkWrite(requests);
                            print(requests)
                            print(response)
                            requests = [];
                        }
                        catch(e){
                            print(e)
                        }
                    }
                 }
   });
}    
           if(requests.length > 0) {
    var response=db.getCollection('question-sections').bulkWrite(requests);
    print(requests)
    print(response)
    requests=[]
}
