FROM node:12.16.1-alpine

WORKDIR /app
ADD /docitt/package.json /app/package.json

RUN npm install --only=production

RUN npm config set unsafe-perm true
RUN npm install -g pm2

ADD /docitt/ /app

ENTRYPOINT pm2-docker --raw start deployment-scripts/app.js